\changetocdepth {5}
\setlength {\cftchapterindent }{0em}
\contentsline {chapter}{List of Tables}{xi}{section*.4}
\contentsline {chapter}{List of Figures}{xii}{section*.5}
\setlength {\cftchapterindent }{\levelindentincrement }
\contentsline {part}{Chapter}{xvi}{section*.7}
\markboth {CHAPTER}{Page}
\contentsline {chapter}{\chapternumberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Context}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Previous Work}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Approach}{8}{section.1.4}
\contentsline {section}{\numberline {1.5}Contributions}{8}{section.1.5}
\contentsline {chapter}{\chapternumberline {2}History of Musical Robots}{10}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{10}{section.2.1}
\contentsline {section}{\numberline {2.2}Greek Antiquity and the Islamic Golden Age}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Archimedes}{12}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Hero of Alexandria}{14}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Apollonius}{14}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Banu (Sons of) Musa}{15}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Ismail ibn al-Razzaz al-Jazari}{17}{subsection.2.2.5}
\contentsline {section}{\numberline {2.3}17th to 19th Century Europe}{21}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Vaucanson}{23}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Keyboard Automata}{25}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Miscellaneous}{25}{subsection.2.3.3}
\contentsline {subsubsection}{\numberline {2.3.3.1}Marreppe's Violinist}{25}{subsubsection.2.3.3.1}
\contentsline {subsubsection}{\numberline {2.3.3.2}Friedrich Kaufmann}{26}{subsubsection.2.3.3.2}
\contentsline {subsection}{\numberline {2.3.4}Manzetti}{26}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Conclusion}{28}{section.2.4}
\contentsline {chapter}{\chapternumberline {3}Timbre Production}{30}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction and Previous Work}{30}{section.3.1}
\contentsline {section}{\numberline {3.2}Striking Mechanisms}{31}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Djembe Strokes}{31}{subsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.1.1}Bass}{32}{subsubsection.3.2.1.1}
\contentsline {subsubsection}{\numberline {3.2.1.2}Tone}{33}{subsubsection.3.2.1.2}
\contentsline {subsubsection}{\numberline {3.2.1.3}Slap}{33}{subsubsection.3.2.1.3}
\contentsline {subsection}{\numberline {3.2.2}Factors Influencing Timbre}{34}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}Hand Rigidity}{34}{subsubsection.3.2.2.1}
\contentsline {subsubsection}{\numberline {3.2.2.2}Hand Morphology}{34}{subsubsection.3.2.2.2}
\contentsline {subsection}{\numberline {3.2.3}Additional Considerations for Slap}{37}{subsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.3.1}Hand size}{37}{subsubsection.3.2.3.1}
\contentsline {subsubsection}{\numberline {3.2.3.2}Open Fingers}{37}{subsubsection.3.2.3.2}
\contentsline {subsubsection}{\numberline {3.2.3.3}Sticks in Sabar}{38}{subsubsection.3.2.3.3}
\contentsline {subsection}{\numberline {3.2.4}Timbral Evaluation}{39}{subsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.4.1}Methodology}{39}{subsubsection.3.2.4.1}
\contentsline {subsubsection}{\numberline {3.2.4.2}Materials Used}{40}{subsubsection.3.2.4.2}
\contentsline {subsubsection}{\numberline {3.2.4.3}Centroid}{40}{subsubsection.3.2.4.3}
\contentsline {subsubsection}{\numberline {3.2.4.4}Comparisons}{42}{subsubsection.3.2.4.4}
\contentsline {subsubsection}{\numberline {3.2.4.5}Discussion}{44}{subsubsection.3.2.4.5}
\contentsline {subsection}{\numberline {3.2.5}Kiki's Hand}{45}{subsection.3.2.5}
\contentsline {section}{\numberline {3.3}Arm}{47}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Dynamics}{47}{subsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.1.1}Human Dynamics}{47}{subsubsection.3.3.1.1}
\contentsline {subsubsection}{\numberline {3.3.1.2}Servo Dynamics}{49}{subsubsection.3.3.1.2}
\contentsline {subsection}{\numberline {3.3.2}Three Segment Arm}{52}{subsection.3.3.2}
\contentsline {paragraph}{\numberline {3.3.2.0.1}Impact Angle}{53}{paragraph.3.3.2.0.1}
\contentsline {subsection}{\numberline {3.3.3}Inverse Kinematics}{54}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Segment Lengths}{57}{subsection.3.3.4}
\contentsline {section}{\numberline {3.4}Future Work}{62}{section.3.4}
\contentsline {chapter}{\chapternumberline {4}Realtime Transcription of Hand-Drum Timbres}{63}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{63}{section.4.1}
\contentsline {section}{\numberline {4.2}Previous Work}{66}{section.4.2}
\contentsline {section}{\numberline {4.3}Implementation}{67}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Onset Detection}{67}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Feature Selection}{68}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Realtime Classification}{69}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Evaluation}{71}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}The Ecological Case}{71}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Loudness}{72}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Extended Techniques}{73}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Different Instruments}{75}{subsection.4.4.4}
\contentsline {subsubsection}{\numberline {4.4.4.1}Cajon}{75}{subsubsection.4.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.4.2}Darbuka}{76}{subsubsection.4.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.4.3}Frame Drum}{76}{subsubsection.4.4.4.3}
\contentsline {subsubsection}{\numberline {4.4.4.4}Bongos}{76}{subsubsection.4.4.4.4}
\contentsline {section}{\numberline {4.5}Conclusion and Future Work}{77}{section.4.5}
\contentsline {section}{\numberline {4.6}Acknowledgements}{77}{section.4.6}
\contentsline {section}{\numberline {4.7}Proposal}{78}{section.4.7}
\contentsline {chapter}{\chapternumberline {5}Autoevaluation of Timbre}{79}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduction}{79}{section.5.1}
\contentsline {section}{\numberline {5.2}Previous Work}{80}{section.5.2}
\contentsline {section}{\numberline {5.3}Implementation}{80}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Warped Timbre Space}{80}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Control Space}{81}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Optimization}{81}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}Evaluation}{82}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Computational}{83}{subsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.1.1}Expected Distance}{83}{subsubsection.5.4.1.1}
\contentsline {subsubsection}{\numberline {5.4.1.2}Observed Distances}{85}{subsubsection.5.4.1.2}
\contentsline {subsection}{\numberline {5.4.2}Perceptual}{85}{subsection.5.4.2}
\contentsline {section}{\numberline {5.5}Discussion}{86}{section.5.5}
\contentsline {section}{\numberline {5.6}Conclusion}{86}{section.5.6}
\contentsline {chapter}{\chapternumberline {6}Interactive Rhythm Learning for Percussion Robots}{87}{chapter.6}
\contentsline {section}{\numberline {6.1}Previous Work}{88}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Algorithmic Composition}{88}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Musical Robots}{92}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Machine Learning}{93}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Rhythmic Models}{96}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Components of Rhythm}{96}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Representations of Rhythm}{97}{subsection.6.2.2}
\contentsline {section}{\numberline {6.3}Implementation}{99}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Basic Approach}{100}{subsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.1.1}Prediction}{100}{subsubsection.6.3.1.1}
\contentsline {subsubsection}{\numberline {6.3.1.2}Learning}{101}{subsubsection.6.3.1.2}
\contentsline {subsection}{\numberline {6.3.2}Improvements}{102}{subsection.6.3.2}
\contentsline {subsubsection}{\numberline {6.3.2.1}Timbre}{103}{subsubsection.6.3.2.1}
\contentsline {subsubsection}{\numberline {6.3.2.2}Activation Function}{106}{subsubsection.6.3.2.2}
\contentsline {subsubsection}{\numberline {6.3.2.3}Timing}{107}{subsubsection.6.3.2.3}
\contentsline {subsubsection}{\numberline {6.3.2.4}Long-term Dependencies}{111}{subsubsection.6.3.2.4}
\contentsline {section}{\numberline {6.4}Evaluation}{114}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Network Topology}{115}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Learning Specific Rhythms}{115}{subsection.6.4.2}
\contentsline {subsubsection}{\numberline {6.4.2.1}Individual Rhythms}{117}{subsubsection.6.4.2.1}
\contentsline {subsubsection}{\numberline {6.4.2.2}Corpus of Rhythms}{118}{subsubsection.6.4.2.2}
\contentsline {subsubsection}{\numberline {6.4.2.3}Signal Rhythms}{119}{subsubsection.6.4.2.3}
\contentsline {subsection}{\numberline {6.4.3}Learning Improvisation}{121}{subsection.6.4.3}
\contentsline {subsubsection}{\numberline {6.4.3.1}Note Density}{122}{subsubsection.6.4.3.1}
\contentsline {subsubsection}{\numberline {6.4.3.2}Meter}{123}{subsubsection.6.4.3.2}
\contentsline {subsubsection}{\numberline {6.4.3.3}Syncopicity}{124}{subsubsection.6.4.3.3}
\contentsline {section}{\numberline {6.5}Future Work}{125}{section.6.5}
\contentsline {chapter}{Notes}{126}{chapter*.9}
\contentsline {chapter}{Bibliography}{127}{section*.11}
\setcounter {tocdepth}{0}
\contentsline {part}{Appendix}{136}{section*.12}
\contentsline {appendix}{\chapternumberline {A}Pseudocode for Inverse Kinematics of a Three Segment Arm}{137}{appendix.A}
\contentsline {appendix}{\chapternumberline {B}Audio Buffers Don't Do What You Think They Do!}{139}{appendix.B}
