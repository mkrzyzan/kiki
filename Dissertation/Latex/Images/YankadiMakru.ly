%{
Welcome to LilyPond
===================

Congratulations, LilyPond has been installed successfully.

Now to take it for the first test run.

  1. Save this file

  2. Select

       Compile > Typeset file

  from the menu.

  The file is processed, and

  3.  The PDF viewer will pop up. Click one of the noteheads.


That's it.  For more information, visit http://lilypond.org .

%}

\header{

}

\layout {
  indent = #0
}

\absolute{
  \override Staff.StaffSymbol.line-count = #3
  \clef percussion
  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1)
  \override Staff.Clef.stencil = ##f
  \override Staff.TimeSignature.stencil = ##f

  \repeat volta 2 {a8 e'16 e'16 a16 c'16 c'8 a8 e'8 a8 c'16 c'16} |
  c'8 c'16 c'16~ c'16 c'8 c'16 c'8 e'16 e'16 e'4 |
  \repeat volta 2 {a16 c'8 c'16 a8 e'8 a8 c'8 a8 e'8} |
  c'8 c'16 c'16~ c'16 c'8 c'16 c'8 e'16 e'16 e'4_"D.C." \bar "|."
}

\version "2.18.2"  % necessary for upgrading to future LilyPond versions.
