#include <math.h>
#include <stdio.h>

#define NUM_FEATURES 5
#define NUM_SAMPLES  25
#define NUM_COMPARISONS ((NUM_SAMPLES-1) * (NUM_SAMPLES/2.0))

double basses[NUM_SAMPLES][NUM_FEATURES] = 
{
{1.019759, -1.021615, -1.241188, 0.744605, 1.775642},
{1.054282, -1.020208, -1.271465, 0.775979, 1.542580},
{1.042499, -0.990574, -1.140498, 0.915585, 1.950439},
{1.127588, -0.925812, -1.099961, 0.917725, 1.833908},
{1.000021, -1.054298, -1.246697, 0.902281, 1.600846},
{1.058922, -0.963455, -1.080120, 0.972123, 1.659111},
{1.017830, -1.031059, -1.262622, 0.796911, 1.775642},
{1.056864, -1.053105, -1.305675, 0.941122, 1.600846},
{0.994448, -1.002098, -1.167614, 0.813702, 1.775642},
{1.064988, -0.916278, -1.029079, 0.902654, 1.775642},
{1.008949, -1.029655, -1.182261, 0.918870, 1.659111},
{1.012737, -1.057777, -1.239259, 0.990271, 1.484314},
{1.133965, -0.843771, -0.911789, 0.927665, 1.251252},
{1.049444, -1.076574, -1.287973, 1.028114, 1.542580},
{1.051779, -0.945291, -1.060972, 0.916664, 1.659111},
{1.069297, -0.895505, -1.013088, 0.802461, 1.775642},
{1.114702, -1.074889, -1.369330, 0.981668, 1.892173},
{1.060365, -1.107760, -1.338324, 1.097061, 1.659111},
{1.021942, -0.989358, -1.180083, 0.762832, 2.008704},
{1.011306, -1.013926, -1.157053, 0.906451, 1.659111},
{1.011519, -1.060286, -1.202570, 1.036247, 1.659111},
{1.018125, -0.999223, -1.135627, 0.952129, 1.775642},
{1.013330, -1.120078, -1.354155, 1.009362, 1.309518},
{1.027157, -1.020844, -1.173019, 0.921929, 1.542580},
{1.053155, -1.091822, -1.317476, 1.041683, 1.717377}
};

/*-------------------------------------------------*/
double distance(double* sample_a, double* sample_b)
{
  double sum=0, temp;
  int i;
  
  for(i=0; i<NUM_FEATURES; i++)
    {
      temp = sample_a[0] - sample_b[0];
      temp *= temp;
      sum += temp;
    }

  return sqrt(sum / (double)NUM_FEATURES);
}

/*-------------------------------------------------*/
double distance_mean(double samples[NUM_SAMPLES][NUM_FEATURES])
{
  int i, j;
  double mean=0;
  double scale = 1.0 / NUM_COMPARISONS;

  for(i=0; i<NUM_SAMPLES-1; i++)
    for(j=i+1; j<NUM_SAMPLES; j++)
       mean += distance(samples[i], samples[j]) * scale;

  return mean;
}

/*-------------------------------------------------*/
double distance_variance(double samples[NUM_SAMPLES][NUM_FEATURES], double mean)
{
  int i, j;
  double variance=0, temp;
  double scale = 1.0 / NUM_COMPARISONS;

  for(i=0; i<NUM_SAMPLES-1; i++)
    for(j=i+1; j<NUM_SAMPLES; j++)
       {
         temp= distance(samples[i], samples[j]);
         temp -= mean;
         variance += temp * temp * scale;
       }
  return variance;  
}

/*-------------------------------------------------*/
int main(void)
{
  double m, v;
  m = distance_mean(basses);
  v = distance_variance(basses, m);

  fprintf(stderr, "distance mean: %lf, distance variance: %lf\r\n", m, v);
  fprintf(stderr, "num_comparisons: %i\r\n", (int)NUM_COMPARISONS);
}

 