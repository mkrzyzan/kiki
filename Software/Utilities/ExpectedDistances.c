#include <libc.h>
#include <math.h>

#define NUM_SAMPLES    1000000
#define NUM_DIMENSIONS 5

/*---------------------------------------------*/
long double random_0_1()
{
  return ((long double)random()) / ((long double)RAND_MAX);
}

/*---------------------------------------------*/
long double random_normal(long double mean, long double std_dev)
{
  long double u = 2 * random_0_1() - 1;
  long double v = 2 * random_0_1() - 1;
  long double r = u*u + v*v;
  /*if outside interval [0,1] start over*/
  if(r == 0 || r > 1) return random_normal(mean, std_dev);

  long double c = sqrtl(-2 * logl(r) / r);
  return mean + u * c * std_dev;
}

/*---------------------------------------------*/
long double random_distance(int num_dimensions)
{
  int i;
  long double a, b, sum=0;

  for(i=0; i<num_dimensions; i++)
    {
      a =  random_normal(0, 1);
      a -= random_normal(0, 1);
      sum += a*a;
    }
  return sqrtl(sum);
  return sum;
}

/*---------------------------------------------*/
int main(void)
{
  long double mean=0, variance=0, dist;
  int i;
  
  srandom(time(NULL));

  for(i=0; i<NUM_SAMPLES; i++)
    {
      dist = random_distance(NUM_DIMENSIONS);
      mean += dist;
    }
  mean /= NUM_SAMPLES;

  for(i=0; i<NUM_SAMPLES; i++)
    {
      dist = random_distance(NUM_DIMENSIONS);
      dist -= mean; 
      variance += dist * dist;
    }
  variance /= NUM_SAMPLES;

  fprintf(stderr, "mean: %lf\tvariance: %lf\r\n", (double)mean, (double) variance);
}

