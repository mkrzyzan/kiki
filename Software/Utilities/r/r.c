#include "stdio.h"
#include "math.h"
#include "string.h"

#define DIMENSION 2

int main(int argc, const char* argv[])
{
  if(argc < 2) 
   {fprintf(stderr, "Which file would you like to find the Pearson correlation coefficient in?\r\n"); return -1;}

  while(--argc > 0)
    {
      ++argv;
      FILE* f = fopen(*argv, "r");
      if(f != NULL)
        {
          long double avg     [DIMENSION];
          long double variance[DIMENSION];
          long double std_dev [DIMENSION];
          long double next    [DIMENSION];
          long double product     = 1;
          long double numerator   = 0;
          long double r           = 1;
          long double m, b, t=0;
          
          unsigned d=0;
          unsigned N=0, N1=0;
          int ret;

          memset(avg,      0, DIMENSION * sizeof(*avg     ));
          memset(variance, 0, DIMENSION * sizeof(*variance));
          memset(std_dev,  0, DIMENSION * sizeof(*std_dev ));
          memset(next,     0, DIMENSION * sizeof(*next    ));
          
          //average
          while((ret = fscanf(f, "%Lf\n", next+d)) != EOF)
            {
              if(ret <= 0)
                {fprintf(stderr, "garbage in input\r\n"); break;}
              avg[d] += next[d];
              ++N; d = N % DIMENSION;
            }
          if(N == 0)
            {fprintf(stderr, "no floating point numbers in file\r\n"); continue;}
          if(d != 0)
            {fprintf(stderr, "different length of data\r\n"); continue;}
          
          N /= DIMENSION;
          for(d=0; d<DIMENSION; d++)
            avg[d] /= N;
          
          d=0;
          rewind(f);

          while((ret = fscanf(f, "%Lf", next+d)) != EOF)
            {
              if(ret <= 0)
                {fprintf(stderr, "garbage in input\r\n"); break;}
              next[d] -= avg[d];
              variance[d] += next[d] * next[d];
              product *= next[d];
              
              ++N1; d = N1 % DIMENSION;
              if(d == 0)
                {
                  numerator += product;
                  product = 1.0;
                }
            }

          if(N1 != (N * DIMENSION))
            {fprintf(stderr, "file changed while reading a second time...\r\n"); continue;}
            
          for(d=0; d<DIMENSION; d++)
            {
              r         *= sqrtl(variance[d]);
              variance[d] /= N;
              std_dev [d] = sqrtl(variance[d]);
            }
            
          r = numerator / r;
          m = r * (std_dev[1] / std_dev[0]); //how to  calculate for multivariate data?
          b = avg[1] - m * avg[0];
          if(N > 2)
            {
              t = sqrtl((1-(r*r)) / (N-2));
              t = r / t;
            }          
          fprintf(stderr, "%s, N=%u\tr=%Lf\tslope=%Lf\ty-intercept=%Lf\tt stat=%Lf with %i dof\r\n", *argv, N, r, m, b, t, N-2);
        }
      else
        fprintf(stderr, "unable to open %s\r\n", *argv);
    }
}