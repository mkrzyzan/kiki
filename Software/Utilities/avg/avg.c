#include "stdio.h"
#include "math.h"

int main(int argc, const char* argv[])
{
  if(argc < 2) 
   {fprintf(stderr, "which file would you like to find averages in?\r\n"); return -1;}

  while(--argc > 0)
    {
      ++argv;
      FILE* f = fopen(*argv, "r");
      if(f != NULL)
        {
          long double avg=0;
          long double variance=0;
          long double std_dev=0;
          long double next;
          unsigned N=0, N1=0;
          int ret;

          while((ret = fscanf(f, "%Lf\n", &next)) != EOF)
            {
              if(ret <= 0)
                {fprintf(stderr, "garbage in input\r\n"); break;}
              ++N;
              avg += next;
            }
          if(N == 0)
            {fprintf(stderr, "no floating point numbers in file\r\n"); continue;}
          avg /= N;
        
          rewind(f);

          while((ret = fscanf(f, "%Lf", &next)) != EOF)
            {
              if(ret <= 0)
                {fprintf(stderr, "garbage in input\r\n"); break;}
              ++N1;
              next -= avg;
              variance += next * next;
            }
          if(N1 != N)
            {fprintf(stderr, "file changed while reading a second time...\r\n"); continue;}
          variance /= N;
          std_dev = sqrtl(variance);

          fprintf(stderr, "%s N=%u\tavg=%Lf\tvariance=%Lf\tstd_dev=%Lf\r\n", *argv, N, avg, variance, std_dev);
        }
      else
        fprintf(stderr, "unable to open %s\r\n", *argv);
    }
}