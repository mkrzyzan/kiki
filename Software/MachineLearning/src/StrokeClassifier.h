#ifndef __MK_STROKE_CLASSIFIER__
#define __MK_STROKE_CLASSIFIER__


#if defined(__cplusplus)
extern "C"{
#endif   //(__cplusplus)

#include "MKAiff.h"
#include "List.h"
#include "Timestamp.h"

/*----------------------------------------------------------*/
typedef enum classifier_features_enum
{
  //indices for features array
  CLASSIFIER_SPECTRAL_CENTROID = 0,
  CLASSIFIER_SPECTRAL_SPREAD,
  CLASSIFIER_SPECTRAL_SKEWNESS,
  CLASSIFIER_SPECTRAL_KURTOSIS,
  CLASSIFIER_RMS_AMPLITUDE,
  CLASSIFIER_ZERO_CROSSING_RATE,
  CLASSIFIER_NUM_FEATURES,
}classifier_features_t;

/*----------------------------------------------------------*/
typedef struct opaque_classifier_example_strcut 
{
  MKAiff* audio_aiff;
  MKAiff* spectral_aiff;
  char*   name;
  
  double  raw_features[CLASSIFIER_NUM_FEATURES];
  double  normalized_features[CLASSIFIER_NUM_FEATURES];
  int     category; //will be -1 if uncategorized
}ClassifierExample;

/*----------------------------------------------------------*/
typedef void (*classifier_onset_callback_t)(void* callback_arg, ClassifierExample* e, timestamp_microsecs_t onset_timestamp);

/*----------------------------------------------------------*/
typedef struct opaque_stroke_classifier_strcut StrokeClassifier;

/*----------------------------------------------------------*/
StrokeClassifier* classifier_new                        (classifier_onset_callback_t callback, void* callback_arg, const char* training_example_path);
StrokeClassifier* classifier_destroy                    (StrokeClassifier* self);

//               must be mono
void             classifier_process_audio               (StrokeClassifier* self, float* buffer, int numSamples, timestamp_microsecs_t buffer_start_timestamp);
void             classifier_classify                    (StrokeClassifier* self, MKAiff* aiff_in, ClassifierExample** returned_e, double* returned_distance);
void             classifier_clear                       (StrokeClassifier* self);

void             classifier_example_copy_features       (ClassifierExample* src, ClassifierExample* dst); //not aiffs, name, or category

void             classifier_enter_training_mode         (StrokeClassifier* self, int  is_training /*1 to enter 0 to exit*/, int  category);
int              classifier_get_is_training             (StrokeClassifier* self);
int              classifier_get_training_category       (StrokeClassifier* self);

void             classifier_delete_all_training_examples(StrokeClassifier* self);
void             classifier_delete_last_training_example(StrokeClassifier* self);
List*            classifier_get_all_training_examples   (StrokeClassifier* self);
double           classifier_get_euclidian_distance      (StrokeClassifier* self, ClassifierExample* a, ClassifierExample* b);

void             classifier_set_k                       (StrokeClassifier* self, unsigned k);
unsigned         classifier_get_k                       (StrokeClassifier* self);

/* set the mask to 0 to turn a feature off */
int              classifier_get_feature_mask_for_feature(StrokeClassifier* self, classifier_features_t feature);
void             classifier_set_feature_mask_for_feature(StrokeClassifier* self, classifier_features_t feature, int mask);

/* returns accuracy on leave-one-out cross-validation across training set */
double           classifier_validate_training_examples  (StrokeClassifier* self, unsigned* returned_n);

void             classifier_set_onset_threshold         (StrokeClassifier* self, double threshold);
double           classifier_get_onset_threshold         (StrokeClassifier* self);
void             classifier_set_onset_callback          (StrokeClassifier* self, classifier_onset_callback_t callback, void* arg);
classifier_onset_callback_t   classifier_get_onset_callback  (StrokeClassifier* self, void** returned_arg);

void             classifier_print_normalized_examples(StrokeClassifier* self);

#if defined(__cplusplus)
extern "C"{
#endif   //(__cplusplus)

#endif //__MK_STROKE_CLASSIFIER__