/*
 *  Synth.h
 *  Make weird noises
 *
 *  Made by Michael Krzyzaniak at Arizona State University's
 *  School of Arts, Media + Engineering in Spring of 2013
 *  mkrzyzan@asu.edu
 */

#include "Microphone.h"

const char* MIC_CHAN_0_STROKE_CLASSIFIER_EXAMPLES_PATH = "ClassifierExamplesChan0/";
const char* MIC_CHAN_1_STROKE_CLASSIFIER_EXAMPLES_PATH = "ClassifierExamplesChan1/";
const char* MIC_CHAN_N_STROKE_CLASSIFIER_EXAMPLES_PATH = "ClassifierExamplesChanN/";


/*OpaqueMicrophoneStruct----------------------------------*/
struct OpaqueMicrophoneStruct
{
  AUDIO_GUTS                  ;
  StrokeClassifier** classifier; //one for each channel
  
  //for simulated mic
  MKAiff*                      audioFile ;
  timestamp_microsecs_t        start_time;
  timestamp_microsecs_t        frame_count;
  int                          not_first_time;
};

int micSimulatorAudioCallback(void* SELF, auSample_t* buffer, int numFrames, int numChannels);
int micAudioCallback         (void* SELF, auSample_t* buffer, int numFrames, int numChannels);

Microphone* micDestroy       (Microphone* self);

/*syNew---------------------------------------------------*/
int micInitClassifiers(Microphone* self, unsigned numChannels)
{
  int i;
  self->destroy = (Audio* (*)(Audio*))micDestroy;
  
  self->classifier = (StrokeClassifier**)calloc(numChannels, sizeof(self->classifier[0]));
  if(self->classifier == NULL) return 0;

  if(self->numChannels > 0)
    {
      self->classifier[0] = classifier_new(NULL, NULL, MIC_CHAN_0_STROKE_CLASSIFIER_EXAMPLES_PATH);
      if(self->classifier[0] == NULL) return 0;
    }
  if(self->numChannels > 1)
    {
      self->classifier[1] = classifier_new(NULL, NULL, MIC_CHAN_1_STROKE_CLASSIFIER_EXAMPLES_PATH);
      if(self->classifier[1] == NULL) return 0;
    }
  for(i=2; i<self->numChannels; i++)
    {
      self->classifier[i] = classifier_new(NULL, NULL, MIC_CHAN_N_STROKE_CLASSIFIER_EXAMPLES_PATH);
      if(self->classifier[i] == NULL) return 0;
    }
  return 1;
}

/*syNew---------------------------------------------------*/
Microphone* micNew(unsigned numChannels)
{
  Microphone* self = (Microphone*) auAlloc(sizeof(*self), micAudioCallback, NO, numChannels);
  
  if(self != NULL)
    {
      if(micInitClassifiers(self, numChannels) == 0)
        return micDestroy(self);
    }
  return self;
}

/*syNew---------------------------------------------------*/
Microphone* micSimulatorNew(char* filename)
{
  MKAiff* aiff = aiffWithContentsOfFile(filename);
  if(aiff == NULL) return NULL;
  
  Microphone* self = (Microphone*) auAlloc(sizeof(*self), micSimulatorAudioCallback, YES, aiffNumChannels(aiff));
  if(self != NULL)
    {
      self->buffer_timestamp_is_supported = 1;
      self->audioFile = aiff;
      aiffNormalize(self->audioFile, aiffYes);
      aiffResample(self->audioFile, AU_SAMPLE_RATE, aiffInterpLinear);
      if(micInitClassifiers(self, self->numChannels) == 0)
        return micDestroy(self);      
    }
  return self;
}


/*syDestroy-----------------------------------------------*/
Microphone* micDestroy(Microphone* self)
{
  if(self != NULL)
    {
      auPause((Audio*)self);
      
      if(self->classifier != NULL)
        {
          int i;
          for(i=0; i<self->numChannels; i++)
            classifier_destroy(self->classifier[i]);
          free(self->classifier);
        }

      aiffDestroy(self->audioFile);
      auDestroy((Audio*)self);
    }
    
  return (Microphone*) NULL;
}

/*---------------------------------------------------------*/
StrokeClassifier* micGetClassifier     (Microphone* self, unsigned channel)
{
  StrokeClassifier* result = NULL;
  if(channel < self->numChannels)
    result = self->classifier[channel];

  return result;
}

/*---------------------------------------------------------*/
int micSimulatorAudioCallback(void* SELF, auSample_t* buffer, int numFrames, int numChannels)
{

  Microphone* self = (Microphone*)SELF;
  int resultFrames = 0;

  if(!self->not_first_time)
    {
      self->not_first_time = 1;
      self->start_time = timestamp_get_current_time() + 50000;
    }
      
  resultFrames = aiffReadFloatingPointSamplesAtPlayhead (self->audioFile, buffer, numFrames * numChannels);
  resultFrames /= numChannels;
  self->current_buffer_timestamp = self->start_time + (timestamp_microsecs_t)(0.5 + 1000000 * ((long double)self->frame_count / (long double)AU_SAMPLE_RATE));
  self->frame_count += numFrames;
  
  micAudioCallback(SELF, buffer, resultFrames, numChannels);
  aiffAdvancePlayheadByFrames  (self->audioFile, -resultFrames);
  
  //buffer got deinterleaved in the process...
  resultFrames = aiffReadFloatingPointSamplesAtPlayhead (self->audioFile, buffer, numFrames * numChannels);
  resultFrames /= numChannels;
  
  return resultFrames;
}

/*micAudioCallback-----------------------------------------*/
int micAudioCallback(void* SELF, auSample_t* buffer, int numFrames, int numChannels)
{
  Microphone* self = (Microphone*)SELF;
  
  if(self->buffer_timestamp_is_supported)
    {
      int i;
      auDeinterleave(buffer, numFrames, numChannels);
      if(numChannels > self->numChannels) numChannels = self->numChannels;
      
      for(i=0; i<numChannels; i++)
        {
          classifier_process_audio(self->classifier[i], (float*)buffer, numFrames, self->current_buffer_timestamp);
          buffer += numFrames;
        }
    }
  else 
    fprintf(stderr, "your audio system's timing is not precise enough for this software!\r\n");
  
  return numFrames;
}

