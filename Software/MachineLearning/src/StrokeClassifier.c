#include "StrokeClassifier.h"
#include "AudioSuperclass.h"
#include "FFT.h"
#include "List.h"
#include "Filter.h"

#include <glob.h>
#include <sys/time.h>
#include <pthread.h>

#define EXAMPLE_DURATION 0.05  //seconds
#define EXAMPLE_DURATION_FRAMES (EXAMPLE_DURATION * AU_SAMPLE_RATE)
#define CLASSIFIER_FFT_N 1024
#define CLASSIFIER_FFT_OVERLAP (3.0 / 4.0)
#define CLASSIFIER_FFT_HOP_SIZE_FRAMES ((1-CLASSIFIER_FFT_OVERLAP) * CLASSIFIER_FFT_N)
#define ONSET_FFT_N AU_BUFFER_NUM_FRAMES
#define ONSET_FFT_OVERLAP (7.0 / 8.0)
#define ONSET_FFT_HOP_SIZE_FRAMES  ((1-CLASSIFIER_FFT_OVERLAP) * CLASSIFIER_FFT_N)
#define ONSET_FFT_HOP_SIZE_SECONDS ((ONSET_FFT_N * (1-(ONSET_FFT_OVERLAP))) / ((double)AU_SAMPLE_RATE))
#define STATE_LISTENING 0
#define STATE_RECORDING 1
#define CLASSIFIER_DEFAULT_K    5
#define CLASSIFIER_OSS_FILTER_ORDER 4 //must be even
#define CLASSIFIER_OSS_FILTER_LATENCY (((CLASSIFIER_OSS_FILTER_ORDER >> 1) * ONSET_FFT_HOP_SIZE_SECONDS) * 1000000)
#define CLASSIFIER_FILTER_SAMPLE_RATE (AU_SAMPLE_RATE / (double)ONSET_FFT_HOP_SIZE_FRAMES) //check this!

#define CATEGORY_MARKER_ID 1

/*----------------------------------------------------------*/
struct opaque_stroke_classifier_strcut
{
  List*                 training_examples;
  const char*           training_example_path;
  
  double                standard_deviation[CLASSIFIER_NUM_FEATURES];
  double                mean              [CLASSIFIER_NUM_FEATURES];
  int                   feature_mask      [CLASSIFIER_NUM_FEATURES];
  
  unsigned              k;  //for kNN classification
  
  FFT*                  classifier_fft;
  MKAiff*               audio_aiff;
  int                   is_training;
  int                   training_category;
  Filter*               onset_filter;
  FFT*                  onset_fft;  
  int                   onset_state;
  float*                onset_prev_spectrum;
  double                onset_threshold;
  float                 onset_prev_signal;
  float                 onset_derivative;
  BOOL                  onset_is_past_threshold;
  classifier_onset_callback_t onset_callback;
  void*                 onset_callback_arg;
  
  double*               spectral_weights;
  float*                scratch_analysis_buffer;
  timestamp_microsecs_t onset_time;
  
  pthread_mutex_t       mutex;
};

ClassifierExample* classifier_example_new(StrokeClassifier* self, char* name, MKAiff* aiff);
ClassifierExample* classifier_example_destroy(ClassifierExample* self);
void               classifier_example_init_features(StrokeClassifier* self, ClassifierExample* example);
void               classifier_calculate_spectrum(StrokeClassifier* self, ClassifierExample* example);
void               classifier_calculate_spectral_centroid(StrokeClassifier* self, ClassifierExample* example);
void               classifier_calculate_spectral_spread_skewness_kurtosis(StrokeClassifier* self, ClassifierExample* example);
void               classifier_calculate_rms_and_zcr(StrokeClassifier* self, ClassifierExample* example);
void               classifier_glob_training_examples(StrokeClassifier* self, const char* path);
void               classifier_clear(StrokeClassifier* self);
void               classifier_category_from_neighbors(StrokeClassifier* self, ClassifierExample* example, unsigned k, ClassifierExample* e[], double dist[]);
void               classifier_k_nearest_neighbors(StrokeClassifier* self, ClassifierExample* e, unsigned k, ClassifierExample* result_e[], double result_dist[]);
void               classifier_example_classify(StrokeClassifier* self, ClassifierExample* e);
void               classifier_example_add_to_training_examples(StrokeClassifier* self, ClassifierExample* e);
void               classifier_quick_classify(StrokeClassifier* self, ClassifierExample* e);

/*----------------------------------------------------------*/
StrokeClassifier* classifier_new(classifier_onset_callback_t callback, void* callback_arg, const char* training_example_path)
{
  StrokeClassifier* self = (StrokeClassifier*)calloc(1, sizeof(*self));
  if(self != NULL)
    {
      int i;
      self->onset_threshold       = 0.7;
      self->onset_callback        = callback;
      self->onset_callback_arg    = callback_arg;
      self->training_example_path = training_example_path;
      
      
      if(pthread_mutex_init(&self->mutex, NULL) != 0)
        return classifier_destroy(self);
        
      self->training_examples = listNew();
      if(self->training_examples == NULL)
        return classifier_destroy(self);

      self->onset_fft = fftNew(ONSET_FFT_N);
      if(self->onset_fft == NULL)
        return classifier_destroy(self);
        
      self->classifier_fft = fftNew(CLASSIFIER_FFT_N);
      if(self->classifier_fft == NULL)
        return classifier_destroy(self);
        
      self->scratch_analysis_buffer = calloc(/*CLASSIFIER_FFT_HOP_SIZE_FRAMES*/ CLASSIFIER_FFT_N, sizeof(*self->scratch_analysis_buffer));
      if(self->scratch_analysis_buffer == NULL)
        return classifier_destroy(self);
      
      self->audio_aiff = aiffWithDurationInSeconds(1, AU_SAMPLE_RATE, 16, 1);
      if(self->audio_aiff == NULL)
        return classifier_destroy(self);
        
      self->onset_filter = filter_new(FILTER_LOW_PASS, 12, CLASSIFIER_OSS_FILTER_ORDER); //12 HZ is sextuplets at 120 bpm...
      if(self->onset_filter == NULL)
        return classifier_destroy(self);
      filter_set_sample_rate(self->onset_filter, CLASSIFIER_FILTER_SAMPLE_RATE);
        
      self->onset_prev_spectrum = calloc(ONSET_FFT_N/2, sizeof(*self->onset_prev_spectrum));
      if(self->onset_prev_spectrum == NULL)
        return classifier_destroy(self);

      self->spectral_weights = calloc(CLASSIFIER_FFT_N/2, sizeof(*self->spectral_weights));
      if(self->spectral_weights == NULL)
        return classifier_destroy(self);
      
      for(i=1; i<CLASSIFIER_FFT_N/2; i++)
        self->spectral_weights[i] = AU_CPS2MIDI(fftFrequencyOfBin(self->classifier_fft, i, AU_SAMPLE_RATE));
      
      for(i=0; i<CLASSIFIER_NUM_FEATURES; i++)
        self->feature_mask[i] = 1;
      
      self->k = CLASSIFIER_DEFAULT_K;
      
      classifier_clear(self);
        
      classifier_glob_training_examples(self, self->training_example_path);
    }
  return self;
}

/*----------------------------------------------------------*/
StrokeClassifier* classifier_destroy(StrokeClassifier* self)
{
  if(self != NULL)
    {
      //synchronize with audio thread...
      //but what if the thread exits before the lock is unlocked?
      //pthread_mutex_lock(&self->mutex);
      //pthread_mutex_unlock(&self->mutex);
      pthread_mutex_destroy(&self->mutex);

      listDestroy(self->training_examples, YES);
      fftDestroy(self->onset_fft);
      fftDestroy(self->classifier_fft);
      aiffDestroy(self->audio_aiff);
      filter_destroy(self->onset_filter);

      if(self->onset_prev_spectrum != NULL)
        free(self->onset_prev_spectrum);
      if(self->scratch_analysis_buffer != NULL)
        free(self->scratch_analysis_buffer);

      free(self);
    }
    
  return (StrokeClassifier*) NULL;
}

/*----------------------------------------------------------*/
ClassifierExample* classifier_example_new(StrokeClassifier* self, char* name, MKAiff* aiff)
{
  ClassifierExample* example;
  example = calloc(1, sizeof(*example));
  if(example != NULL)
    {
      example->category = -1;
      example->audio_aiff = aiffNewMono(aiff);
      if(example->audio_aiff == NULL)
        return classifier_example_destroy(example);
      
      unsigned num_frames  = aiffDurationInFrames(example->audio_aiff);
      num_frames *= 1.0 / (1 - CLASSIFIER_FFT_OVERLAP);
      example->spectral_aiff = aiffWithDurationInFrames(1, AU_SAMPLE_RATE, 16, num_frames);
      if(example->spectral_aiff == NULL)
        return classifier_example_destroy(example);      
      
      unsigned n = strlen(name);
      example->name = calloc(n+1, sizeof(*(example->name)));
      if(example->name == NULL)
        return classifier_example_destroy(example);
      memcpy(example->name, name, n);
      
      classifier_example_init_features(self, example);
    }
  return example;
}

/*----------------------------------------------------------*/
ClassifierExample* classifier_example_destroy(ClassifierExample* self)
{
  if(self != NULL)
    {
      if(self->audio_aiff != NULL)
        aiffDestroy(self->audio_aiff);
      if(self->spectral_aiff != NULL)
        aiffDestroy(self->spectral_aiff);
      if(self->name != NULL)
        free(self->name);        
      free(self);
    }
  return (ClassifierExample*)NULL;
}

/*----------------------------------------------------------*/
void classifier_example_copy_features (ClassifierExample* src, ClassifierExample* dst)
{  
  int i;
  for(i=0; i<CLASSIFIER_NUM_FEATURES; i++)
    {
      dst->raw_features[i] = src->raw_features[i];
      dst->normalized_features[i] = src->normalized_features[i];
    }
}

/*----------------------------------------------------------*/
void classifier_example_save(ClassifierExample* self)
{
  //save the category as a string in the aiff;
  char str[32];
  snprintf(str, 32, "%i", self->category);
  aiffRemoveAllMarkers(self->audio_aiff);
  aiffAddMarkerWithPositionInSamples(self->audio_aiff, str, CATEGORY_MARKER_ID, 0);
  aiffSaveWithFilename(self->audio_aiff, self->name);
}

/*----------------------------------------------------------*/
void classifier_example_delete_file(ClassifierExample* self)
{
  remove(self->name);
}

/*----------------------------------------------------------*/
void classifier_example_init_features(StrokeClassifier* self, ClassifierExample* example)
{
  int i;
  
  classifier_calculate_spectrum(self, example);
  classifier_calculate_spectral_centroid(self, example);
  classifier_calculate_spectral_spread_skewness_kurtosis(self, example);
  classifier_calculate_rms_and_zcr(self, example);
  
  for(i=0; i<CLASSIFIER_NUM_FEATURES; i++)
    {
      example->normalized_features[i] = example->raw_features[i] - self->mean[i];
      if(self->standard_deviation[i] != 0)
        example->normalized_features[i] /= self->standard_deviation[i];
    }
}

/*----------------------------------------------------------*/
void classifier_calculate_spectrum(StrokeClassifier* self, ClassifierExample* example)
{
  MKAiff* spectrum = example->spectral_aiff;
  float* scratch = self->scratch_analysis_buffer;
  int overlap = CLASSIFIER_FFT_N * CLASSIFIER_FFT_OVERLAP;
  unsigned num_samples_read;
  int i;
  
  aiffRewindPlayheadToBeginning(example->audio_aiff);
  fftFlush(self->classifier_fft);
  do{
      num_samples_read = aiffReadFloatingPointSamplesAtPlayhead(example->audio_aiff, scratch, CLASSIFIER_FFT_N);
      for(i=num_samples_read; i<CLASSIFIER_FFT_N; scratch[i++]=0);
      fftTransform(self->classifier_fft, scratch, NULL, CLASSIFIER_FFT_N, &scratch, NULL, NO, NO, YES);
      aiffAppendFloatingPointSamples(spectrum, scratch, CLASSIFIER_FFT_N, aiffFloatSampleType);
      aiffAdvancePlayheadByFrames(example->audio_aiff, -overlap);
    }while(num_samples_read == CLASSIFIER_FFT_N);      
}

/*----------------------------------------------------------*/
void classifier_calculate_spectral_centroid(StrokeClassifier* self, ClassifierExample* example)
{
  double centroid    = 0;
  double centroid_numerator, centroid_denominator;
  MKAiff* spectrum = example->spectral_aiff;
  float* scratch   = self->scratch_analysis_buffer;
  int i, num_ffts=0, num_samples_read = 0;
  
  aiffRewindPlayheadToBeginning(spectrum);
  
  do{
      ++num_ffts;
      num_samples_read = aiffReadFloatingPointSamplesAtPlayhead(spectrum, scratch, CLASSIFIER_FFT_N);
      centroid_numerator = centroid_denominator = 0;
        for(i=1; i<CLASSIFIER_FFT_N/2; i++)
          {
            centroid_numerator   += self->spectral_weights[i] * scratch[i];
            centroid_denominator += scratch[i];
          }
        if(centroid_denominator != 0)
          centroid += centroid_numerator / centroid_denominator;
    }while(num_samples_read == CLASSIFIER_FFT_N);
  
  centroid /= (double)num_ffts;
  example->raw_features[CLASSIFIER_SPECTRAL_CENTROID] = centroid;
}

/*----------------------------------------------------------*/
void classifier_calculate_spectral_spread_skewness_kurtosis(StrokeClassifier* self, ClassifierExample* example)
{
  double dist    , dist_exponent;
  double spread=0  , spread_numerator;
  double skewness=0, skewness_numerator;
  double kurtosis=0, kurtosis_numerator;
  double denominator, sigma;
  MKAiff* spectrum = example->spectral_aiff;
  float* scratch   = self->scratch_analysis_buffer;
  int i, num_ffts=0, num_samples_read = 0;
  
  aiffRewindPlayheadToBeginning(spectrum);
  
  do{
      ++num_ffts;
      num_samples_read = aiffReadFloatingPointSamplesAtPlayhead(spectrum, scratch, CLASSIFIER_FFT_N);
      spread_numerator = skewness_numerator = kurtosis_numerator = denominator = 0;
      for(i=1; i<CLASSIFIER_FFT_N/2; i++)
        {
          dist = (self->spectral_weights[i] - example->raw_features[CLASSIFIER_SPECTRAL_CENTROID]);
          
          dist_exponent =  dist * dist;
          spread_numerator   += dist_exponent * scratch[i];
          
          dist_exponent *= dist;
          skewness_numerator += dist_exponent * scratch[i];
          
          dist_exponent *= dist;
          kurtosis_numerator += dist_exponent * scratch[i];
          
          denominator += scratch[i];
        }
      if(denominator != 0)
        {
          spread   += spread_numerator   / denominator;
          skewness += skewness_numerator / denominator;
          kurtosis += kurtosis_numerator / denominator;
        }
  }while(num_samples_read == CLASSIFIER_FFT_N);
  
  spread   /= (double)num_ffts;
  skewness /= (double)num_ffts;
  kurtosis /= (double)num_ffts;
  
  if(spread != 0)
    {
      kurtosis /= spread * spread; //sigma^4
      spread    = sqrt(spread);
      skewness /= spread * spread * spread; //sigma^3
    }
  example->raw_features[CLASSIFIER_SPECTRAL_SPREAD]   = spread;
  example->raw_features[CLASSIFIER_SPECTRAL_SKEWNESS] = skewness;
  example->raw_features[CLASSIFIER_SPECTRAL_KURTOSIS] = kurtosis;
}

/*----------------------------------------------------------*/
void               classifier_calculate_rms_and_zcr(StrokeClassifier* self, ClassifierExample* example)
{
  //do it in chunks of 512 samples;
  int    N = 512, i;
  double RMS = 0;
  double ZCR = 0;
  float  buffer[N];
  int    num_samples_read;
  double duration;
  BOOL   was_positive = 0; //uh, boundary condition is almost 50% likely to cause small error.
  
  aiffRewindPlayheadToBeginning(example->audio_aiff);
  
  do{
      num_samples_read = aiffReadFloatingPointSamplesAtPlayhead(example->audio_aiff, buffer, N);
      for(i=0; i<num_samples_read; i++)
        {
          RMS += buffer[i] * buffer[i];
          if((buffer[i] > 0) != was_positive)
            {
              was_positive = !was_positive;
              ++ZCR;
            }
        }
    }while(num_samples_read == N);

  duration = aiffDurationInSamples(example->audio_aiff);
  if(duration > 1)
    {
      RMS /= duration;
      ZCR /= duration;
    }
    
  RMS = sqrt(RMS);
  
  example->raw_features[CLASSIFIER_RMS_AMPLITUDE] = RMS;
  example->raw_features[CLASSIFIER_ZERO_CROSSING_RATE] = ZCR;
}

/*----------------------------------------------------------*/
//this is usually done atomically with something else so locking occurs elswhere
void classifier_normalize_training_features(StrokeClassifier* self)
{
  ClassifierExample* e;
  double N = listCount(self->training_examples);
  int i;
  
  for(i=0; i<CLASSIFIER_NUM_FEATURES; i++)
    {
      self->standard_deviation[i] = 0;
      self->mean[i] = 0;
      if(listResetIterator(self->training_examples))
        {
          do{
              e = listCurrentData(self->training_examples);
              self->mean[i] += e->raw_features[i];
            }while(listAdvanceIterator(self->training_examples));
          self->mean[i] /= N;
        }
    }

  for(i=0; i<CLASSIFIER_NUM_FEATURES; i++)
    if(listResetIterator(self->training_examples))
      {
        do{
            e = listCurrentData(self->training_examples);
            e->normalized_features[i] = e->raw_features[i] - self->mean[i];
            self->standard_deviation[i] += e->normalized_features[i] * e->normalized_features[i];
          }while(listAdvanceIterator(self->training_examples));
        //there are N-1 independent observarions aside from the mean
        self->standard_deviation[i] /= N;
        self->standard_deviation[i] = sqrt(self->standard_deviation[i]);
      }

  for(i=0; i<CLASSIFIER_NUM_FEATURES; i++)
    if(self->standard_deviation[i] != 0)
      if(listResetIterator(self->training_examples))
        do{
            e = listCurrentData(self->training_examples);
            e->normalized_features[i] /= self->standard_deviation[i];
          }while(listAdvanceIterator(self->training_examples));
}

/*----------------------------------------------------------*/
void classifier_print_normalized_examples(StrokeClassifier* self)
{
  ClassifierExample* e;
  int i;
  
  if(listResetIterator(self->training_examples))
    {
      do{
        e = listCurrentData(self->training_examples);
        fprintf(stderr, "category: %i \tfeatures:", e->category);
        for(i=0; i<CLASSIFIER_NUM_FEATURES; i++)
          fprintf(stderr, "%lf ", e->normalized_features[i]);
        fprintf(stderr, "\r\n");
        }while(listAdvanceIterator(self->training_examples));
    }  
}

/*----------------------------------------------------------*/
//this is only called during initialization so is not locked
void classifier_glob_training_examples(StrokeClassifier* self, const char* path)
{
  glob_t g;

  int n = strlen(path);
  char glob_pattern[n + 7];
  memcpy(glob_pattern  , path, n);
  memcpy(glob_pattern+n, "*.aiff", 7); //include \0 at end
    
  if(glob(glob_pattern, 0, NULL, &g) == 0)
    {
      int i;
      for(i=0; i<g.gl_pathc; i++)
        {
          //fprintf(stderr, "example[%i]: %s\r\n", i, g.gl_pathv[i]);
          MKAiff* aiff = aiffWithContentsOfFile(g.gl_pathv[i]);
          if(aiff != NULL)
            {
              ClassifierExample* example = classifier_example_new(self, g.gl_pathv[i], aiff);
              if(example != NULL)
                {
                  char* marker_name;
                  if(aiffNameOfMarkerWithID(aiff, CATEGORY_MARKER_ID, &marker_name) == aiffYes)
                    example->category = strtol(marker_name, NULL, 10);
                  listAppendData(self->training_examples, example, (listDataDeallocator_t)classifier_example_destroy);
                }
              aiffDestroy(aiff); // b/c classifier_example_new copies it (does not copy markers)
            }
        }
      classifier_normalize_training_features(self);
      globfree(&g);
    }
}

/*----------------------------------------------------------*/
BOOL classifier_onset_detected(StrokeClassifier* self, MKAiff* aiff_in, ClassifierExample** returned_e)
{
  BOOL should_free = YES;
  struct timeval t;
  gettimeofday(&t, NULL);
  char str[255];
  snprintf(str, 255, "%s%u%07u.aiff", self->training_example_path, (unsigned)t.tv_sec, (unsigned)t.tv_usec);

  ClassifierExample* e = classifier_example_new(self, str, aiff_in);
  
  if(e != NULL)
    {
      pthread_mutex_lock(&self->mutex);
      if(self->is_training)
        {
          classifier_example_add_to_training_examples(self, e);
          should_free = NO;
        }
      else
        classifier_example_classify(self, e);
      pthread_mutex_unlock(&self->mutex);
    }
  
  if(returned_e != NULL)
    *returned_e = e;
    
  return should_free;
}

/*----------------------------------------------------------*/
//locked by calling function
void classifier_example_add_to_training_examples(StrokeClassifier* self, ClassifierExample* e)
{
  //example will be freed later
  e->category = self->training_category;
  classifier_example_save(e);
  
  listInsertDataAtIndex(self->training_examples, e, 0, (listDataDeallocator_t)classifier_example_destroy);
}

/*----------------------------------------------------------*/
//locked by calling function
void classifier_example_classify(StrokeClassifier* self, ClassifierExample* e)
{
  int num_training_examples = listCount(self->training_examples);
  unsigned k = (num_training_examples < self->k) ? num_training_examples : self->k;
  
  if(k>0)
    {
      ClassifierExample* result_e[k];
      double result_dist[k];
      
      classifier_k_nearest_neighbors(self, e, k, result_e, result_dist);
      classifier_category_from_neighbors(self, e, k, result_e, result_dist);
    }
}

/*----------------------------------------------------------*/
double classifier_get_euclidian_distance(StrokeClassifier* self, ClassifierExample* a, ClassifierExample* b)
{
  int i;
  double temp, dist=0;
          
  for(i=0; i<CLASSIFIER_NUM_FEATURES; i++)
    {
      if(!self->feature_mask[i]) 
        continue;
      temp = a->normalized_features[i] - b->normalized_features[i];
      temp *= temp;
      dist += temp;
    }

  return sqrt(dist);
}

/*----------------------------------------------------------*/
void classifier_k_nearest_neighbors(StrokeClassifier* self, ClassifierExample* e, unsigned k, ClassifierExample* result_e[], double result_dist[])
{
  ClassifierExample* current_e;
  double dist;
  int    i, max_index;
  
  for(i=0; i<k; i++)
    {
      result_e[i]    = NULL;
      result_dist[i] = 1000000000;
    }

  if(listResetIterator(self->training_examples))
    {
      do{
          current_e   = listCurrentData(self->training_examples);
          dist = classifier_get_euclidian_distance(self, e, current_e);

          max_index = 0;
          
          //k is small so O(nk) complexity here should be fine
          for(i=1; i<k; i++)
            if(result_dist[i] > result_dist[max_index])
              max_index = i;

          if(dist < result_dist[max_index])
            {
              result_e[max_index] = current_e;
              result_dist[max_index] = dist;
            }
        }while(listAdvanceIterator(self->training_examples));
    }
}

/*----------------------------------------------------------*/
void classifier_category_from_neighbors(StrokeClassifier* self, ClassifierExample* example, unsigned k, ClassifierExample* e[], double dist[])
{
  //find first mode of all e->category
  //to do -- should weight by distance?
  
  int i, j, max_index=0;
  int count[k];
  
  for(i=0; i<k; i++)
    count[i] = 0;
  
  //Yikes! again, this depends upon small k!
  for(i=0; i<k; i++)
    for(j=(i+1); j<k; j++)
      if((e[i])->category == (e[j])->category)
        ++count[i];
  
  for(i=1; i<k; i++)
    if(count[i] > count[max_index])
      max_index = i;
  
  example->category = (e[max_index])->category;
}

/*----------------------------------------------------------*/
void             classifier_enter_training_mode         (StrokeClassifier* self, int  is_training /*1 to enter 0 to exit*/, int  category)
{
  self->is_training = is_training;
  self->training_category = category;
  
  if(!is_training)
    {
      pthread_mutex_lock(&self->mutex);
      classifier_normalize_training_features(self);
      pthread_mutex_unlock(&self->mutex);
    }
}

/*----------------------------------------------------------*/
int             classifier_get_is_training             (StrokeClassifier* self)
{
  return self->is_training;
}

/*----------------------------------------------------------*/
int              classifier_get_training_category       (StrokeClassifier* self)
{
  return self->training_category;
}
/*----------------------------------------------------------*/
void             classifier_delete_all_training_examples(StrokeClassifier* self)
{
  while(listCount(self->training_examples) > 0)
    classifier_delete_last_training_example(self);  
}

/*----------------------------------------------------------*/
void             classifier_delete_last_training_example(StrokeClassifier* self)
{
  pthread_mutex_lock(&self->mutex);
  if(listCount(self->training_examples) > 0)
    {
      classifier_example_delete_file(listDataAtIndex(self->training_examples, 0));
      listRemoveDataAtIndex(self->training_examples, 0, YES);
  
      //don't worry about it if you are already in training mode
      //because data will be normalized when training mode exits
      if(!classifier_get_is_training(self))
        classifier_normalize_training_features(self);
    }
  pthread_mutex_unlock(&self->mutex);
}

/*----------------------------------------------------------*/
List*            classifier_get_all_training_examples   (StrokeClassifier* self)
{
  return self->training_examples;
}

/*----------------------------------------------------------*/
void             classifier_set_k                       (StrokeClassifier* self, unsigned k)
{
  //if k > the number of training examples, half of the number of training examples will be
  //used as k. That is checked in classifier_classify;
  if(k>1000) k = 1000;
  if(k<1)  k = 1;
  
  self->k = k;
}

/*----------------------------------------------------------*/
unsigned         classifier_get_k                       (StrokeClassifier* self)
{
  return self->k;
}

/*----------------------------------------------------------*/
int              classifier_get_feature_mask_for_feature(StrokeClassifier* self, classifier_features_t feature)
{
  int result = 0;
  if(feature < CLASSIFIER_NUM_FEATURES)
    result = self->feature_mask[feature];
    
  return result;
}

/*----------------------------------------------------------*/
void             classifier_set_feature_mask_for_feature(StrokeClassifier* self, classifier_features_t feature, int mask)
{
  if(mask != 0) mask = 1;
  if(feature < CLASSIFIER_NUM_FEATURES)
    self->feature_mask[feature] = mask;
}

/*----------------------------------------------------------*/
double           classifier_validate_training_examples   (StrokeClassifier* self, unsigned* returned_n)
{
  unsigned i, n;
  double   accuracy = 0;
  ClassifierExample* excluded_e;
  int correct_category;
    
  pthread_mutex_lock(&self->mutex);
  
  n = listCount(self->training_examples);
  for(i=0; i<n; i++)
    {
      excluded_e = listDataAtIndex(self->training_examples, 0);
      listRemoveDataAtIndex(self->training_examples, 0, NO);
      correct_category = excluded_e->category;
      classifier_example_classify(self, excluded_e);
      if(excluded_e->category == correct_category)
        ++accuracy;
      //fprintf(stderr, "correct: %i\tclassified:%i\r\n", correct_category, excluded_e->category);
      
      //in the case of only 1 example, e will not be modified and will evaluate true, ie 100% accurate;
      excluded_e->category = correct_category;
      listAppendData(self->training_examples, excluded_e, (listDataDeallocator_t)classifier_example_destroy);
    }

  pthread_mutex_unlock(&self->mutex);
  
  if(returned_n != NULL)
    *returned_n = n;
    
  if(n > 0) 
    accuracy /= n;
  
  return 100.0 * accuracy;
}

/*----------------------------------------------------------*/
void             classifier_set_onset_threshold        (StrokeClassifier* self, double threshold)
{
  if(threshold < 0) threshold = 0;
  self->onset_threshold = threshold;
}

/*----------------------------------------------------------*/
double           classifier_get_onset_threshold        (StrokeClassifier* self)
{
  return self->onset_threshold;
}

/*----------------------------------------------------------*/
void             classifier_set_onset_callback          (StrokeClassifier* self, classifier_onset_callback_t callback, void* arg)
{
  self->onset_callback = callback;
  self->onset_callback_arg = arg;
}

/*----------------------------------------------------------*/
classifier_onset_callback_t   classifier_get_onset_callback  (StrokeClassifier* self, void** arg)
{
  return self->onset_callback;
  *arg = self->onset_callback_arg;
}

/*----------------------------------------------------------*/
void classifier_clear(StrokeClassifier* self)
{
  int i;
  aiffRewindPlayheadToBeginning(self->audio_aiff);
  aiffRemoveSamplesAtPlayhead(self->audio_aiff, aiffDurationInSamples(self->audio_aiff));
  filter_clear(self->onset_filter);
  fftFlush    (self->onset_fft); 
  self->onset_state = STATE_LISTENING;
  self->onset_prev_signal = 0;
  self->onset_derivative = -1;
  for(i=0; i>ONSET_FFT_N/2; i++)
    self->onset_prev_spectrum = 0;
}

/*----------------------------------------------------------*/
void classifier_process_audio(StrokeClassifier* self, float* buffer, int num_frames, timestamp_microsecs_t buffer_start_timestamp)
{  
  int num_FFTs = 1.0 / (1-ONSET_FFT_OVERLAP);
  int i, j, buffer_bytes = num_frames / num_FFTs;
  float *spectrum, *b=buffer, *prev_spectrum;
  float onset_signal, onset_new_derivative;
  float* buffer_recording_start = buffer; //onset might start in the middle of buffer
  
  for(i=0; i<num_FFTs; i++)
    {
      onset_signal = 0;
      prev_spectrum = self->onset_prev_spectrum;
      
      //in case numFrames is not an exact multiple of numOverlaps
      if(i == (num_FFTs-1)) buffer_bytes = num_frames - (b-buffer);
      fftTransform (self->onset_fft, b, NULL, buffer_bytes, &spectrum, NULL, NO, NO, YES);
      
      //fft is padded to length N;
      for(j=1; j<ONSET_FFT_N/2; j++)
        {
          //*spectrum = logf(1 + 1000 * *spectrum);
          if(*spectrum > *prev_spectrum)
            onset_signal += *spectrum - *prev_spectrum;
          *prev_spectrum++ = *spectrum++;
        }
      
      onset_signal *= 0.1;
      filter_process_data(self->onset_filter, &onset_signal, 1);
      
      //onset will always lag by at least filter order / 2, and probably a little more.
      onset_new_derivative = onset_signal - self->onset_prev_signal;
      if((self->onset_derivative > 0) && (onset_new_derivative <= 0)) //local maxium
        if((onset_signal >= self->onset_threshold) && (self->onset_state == STATE_LISTENING) && (self->onset_is_past_threshold == NO))
          {
            self->onset_state = STATE_RECORDING;
            buffer_recording_start = b;
            self->onset_is_past_threshold = YES;
            self->onset_time  = buffer_start_timestamp + 1000000.0 * (i * AU_BUFFER_NUM_FRAMES / (num_FFTs * AU_SAMPLE_RATE));
            self->onset_time -= CLASSIFIER_OSS_FILTER_LATENCY;
            
            //self->onset_callback(self->onset_callback_arg, NULL, self->onset_time);
          }
          
      if(onset_signal < self->onset_threshold)
        self->onset_is_past_threshold = NO;

      self->onset_derivative = onset_new_derivative;
      self->onset_prev_signal = onset_signal;

      b += buffer_bytes;
    }
  
  if(self->onset_state == STATE_RECORDING)
    {
      int duration = aiffDurationInFrames(self->audio_aiff);
      int num_frames_to_record = (num_frames - (buffer_recording_start - buffer));
      int lacking_duration = EXAMPLE_DURATION_FRAMES - duration;
      num_frames_to_record = (lacking_duration < num_frames_to_record) ? lacking_duration : num_frames_to_record;
      
      if(num_frames_to_record > 0)
        aiffAppendFloatingPointSamples(self->audio_aiff, buffer_recording_start, num_frames_to_record, aiffFloatSampleType);
      
      if(aiffDurationInFrames(self->audio_aiff) >= EXAMPLE_DURATION_FRAMES)
        {
          ClassifierExample* e = NULL;          
          BOOL should_destroy = classifier_onset_detected(self, self->audio_aiff, &e);
      
          if(e != NULL)
            {
              if(self->onset_callback != NULL)
                self->onset_callback(self->onset_callback_arg, e, self->onset_time);
              if(should_destroy) 
                classifier_example_destroy(e);
            }

          aiffRewindPlayheadToBeginning(self->audio_aiff);
          aiffRemoveSamplesAtPlayhead(self->audio_aiff, aiffDurationInSamples(self->audio_aiff));
          self->onset_state = STATE_LISTENING;
        }
    }
}











/*
Code Graveyard

//----------------------------------------------------------
BOOL classifier_sort_by_distance_callback(ClassifierExample* a, ClassifierExample* b)
{
  return (a->reserved_distance > b->reserved_distance);
}

//----------------------------------------------------------
BOOL classifier_sort_by_category_callback(ClassifierExample* a, ClassifierExample* b)
{
  return (a->category > b->category);
}

//----------------------------------------------------------
//actually slower despite lower time complexity... 
//Sorts training examples by distance and
//then sorts the first k examples by category, and returns
//the category comprising the longest subsequence
//not used by this module

void classifier_example_quick_classify(StrokeClassifier* self, ClassifierExample* e)
{
  ClassifierExample* current_e;
  double temp, dist;
  int    i, current_category=-1, max_category_count=0, category_count=0, result_category=-1;
  
  //sorting will screw up cross-validation... copying prevents that.
  List* examples = self->training_examples;//listShallowCopy(self->training_examples);
  if(examples == NULL) return;
  
  if(listResetIterator(examples))
    {
      do{
          dist = 0;
          current_e   = listCurrentData(examples);
          
          for(i=0; i<CLASSIFIER_NUM_FEATURES; i++)
            {
              if(!self->feature_mask[i]) 
                continue;
              temp = e->normalized_features[i] - current_e->normalized_features[i];
              temp *= temp;
              dist += temp;
            }
            
          //is it really necessary to take the square root to find nearest?
          dist = sqrt(dist);
          current_e->reserved_distance = dist;
        }while(listAdvanceIterator(examples));
    }
  
  listSort     (examples, (listSortCallback_t)classifier_sort_by_distance_callback);
  listSortRange(examples, 0, self->k, (listSortCallback_t)classifier_sort_by_category_callback);
  
  i=0;
  if(listResetIterator(examples))
    {
      current_e       = listCurrentData(examples);
      result_category = current_e->category; //this will break ties;
      
      do{
          current_e   = listCurrentData(examples);
          //printf("{%i %lf} ", current_e->category, current_e->reserved_distance);
          if(current_e->category == current_category)
            {
              if((++category_count) > max_category_count)
                {
                  result_category = current_category;
                  max_category_count = category_count;
                }
            }
          else
            {
              category_count = 0;
              current_category = current_e->category;
            }
        }while(listAdvanceIterator(examples) && (++i < self->k));
        //printf("... -> %i\r\n", result_category);
    }
  //listDestroy(examples, NO);
  e->category = result_category;
}
*/

