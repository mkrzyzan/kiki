#include "RhythmGenerator.h"
#include "NeuralNetwork.h"
#include "Util.h"
#include <math.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> //usleep

#define RHYTHM_BEATS_SILENCE_BERFORE_STOP      4
#define RHYTHM_MAX_NUM_STROKE_CATEGORIES       64
#define RHYTHM_MAX_BEAT_DURATION               1500000 //microseconds
#define RHYTHM_MIN_BEAT_DURATION               350000  //microseconds (must be more than half of prediction duration)
#define RHYTHM_MIN_STROKE_DURATION             83333   //microseconds, 12 Hz
#define RHYTHM_BRAIN_PATH                      "RhythmicModels/brain.nn"
#define RHYTHM_BRAIN_NUM_LAYERS                3       //must be at least 3
#define RHYTHM_BRAIN_NUM_HIDDEN_NEURONS        9       //equal in all hidden layers
#define RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS     6
#define RHYTHM_BRAIN_INPUT_LENGTH_IN_SUBDIVISIONS (RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS * (self->subdivisions_per_beat))
#define PENDING_STROKES_LENGTH_IN_BEATS        (12 + RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS) //plenty of space
#define PENDING_STROKES_LENGTH_IN_SUBDIVISIONS    (self->subdivisions_per_beat * PENDING_STROKES_LENGTH_IN_BEATS) //plenty of space
#define SIGMOID_THRESHHOLD                      -4

typedef void (*rhythm_stike_funct_t)(RhythmGenerator*, timestamp_microsecs_t, int, double);
void rhythm_strike_imitate(RhythmGenerator* self, timestamp_microsecs_t input_timestamp, int category, double loudness);
void rhythm_strike_delay  (RhythmGenerator* self, timestamp_microsecs_t input_timestamp, int category, double loudness);
//void rhythm_strike_learn  (RhythmGenerator* self, timestamp_microsecs_t input_timestamp, int category, double loudness);
NeuralNetwork* rhythm_init_brain(int num_layers, nnNeuronCount_t num_neurons_in_layer[]);

/*----------------------------------------------------------*/
typedef struct rhythm_stroke_struct
{
  timestamp_microsecs_t deadline;
  int                   category;
  float                 loudness;
}Rhythm_Stroke;

/*----------------------------------------------------------*/
struct opaque_rhythm_generator_strcut
{
  rhythm_metronome_callback_t metronome_callback;
  rhythm_strike_callback_t    strike_callback;
  void*                       metronome_callback_arg;
  void*                       strike_callback_arg;
  unsigned                    num_stroke_categories;
  
  pthread_t                   rhythm_generator_thread;
  pthread_mutex_t             mutex;
  bool                        thread_is_running;
  timestamp_microsecs_t       thread_start_delay;

  timestamp_microsecs_t       prediction_duration;
  int                         countdown_to_silence;
  timestamp_microsecs_t       prev_input_timestamp, prev_output_timestamp, next_output_timestamp, first_output_timestamp;
  timestamp_microsecs_t       subdivision_duration, beat_duration;
  unsigned                    subdivisions_per_beat;//, subdivisions_delay;
  uint64_t                    current_subdivision;
  
  rhythm_stike_funct_t        strike;
  rhythm_algorithm_t          current_algorithm;
  
  Rhythm_Stroke*              pending_strokes;
  unsigned                    pending_strokes_size;
  
  NeuralNetwork*              brain;
  nnNeuronData_t*             input_neurons;
  nnNeuronData_t*             output_neurons;
  nnNeuronData_t*             target_output_neurons;
  nnNeuronCount_t             num_input_neurons; //for faster access
  nnNeuronCount_t             num_output_neurons;
};

/***********************************************************************************/
void rhythm_activation_rectified_sigmoid(nnNeuronData_t* value)
{
  if(*value < SIGMOID_THRESHHOLD)
    *value = 0;
  else
    *value = (1.0 / (1.0 + exp(-*value)));
}

/***********************************************************************************/
nnNeuronData_t rhythm_activation_deriv_rectified_sigmoid(nnNeuronData_t value)
{
  nnNeuronData_t derivative = 0;
  
  if(value >= SIGMOID_THRESHHOLD)
    {
 	    derivative =  (1.0 / (1.0 + exp(-value)));
	    derivative = derivative * (1-derivative);
    }
  return derivative; 
}

/*----------------------------------------------------------*/
void* rhythm_generator_thread_run_loop(void* SELF);

/*----------------------------------------------------------*/
RhythmGenerator* rhythm_new(unsigned subdivisions_per_beat, unsigned num_stroke_categories, rhythm_metronome_callback_t metronome_callback, 
                            void* metronome_callback_arg, rhythm_strike_callback_t strike_callback, void* strike_callback_arg, timestamp_microsecs_t prediction_duration)
{                            
  RhythmGenerator* self = (RhythmGenerator*)calloc(1, sizeof(*self));
  if(self != NULL)
    {
      unsigned n;   
      self->subdivisions_per_beat  = subdivisions_per_beat;
      self->metronome_callback     = metronome_callback;
      self->strike_callback        = strike_callback;
      self->metronome_callback_arg = metronome_callback_arg;
      self->strike_callback_arg    = strike_callback_arg;
      self->prediction_duration    = prediction_duration;
      rhythm_set_algorithm(self, RHYTHM_LEARN_ALGORITHM);
      
      CONSTRAIN(num_stroke_categories, 1, RHYTHM_MAX_NUM_STROKE_CATEGORIES);
      self->num_stroke_categories = num_stroke_categories;

      self->pending_strokes_size = PENDING_STROKES_LENGTH_IN_SUBDIVISIONS;

      self->pending_strokes = calloc(self->pending_strokes_size, sizeof(self->pending_strokes[0]));
      if(self->pending_strokes == NULL) return rhythm_destroy(self);
      
      for(n=0; n<self->pending_strokes_size; n++)
        self->pending_strokes[n].category = -1;
      n = num_stroke_categories * subdivisions_per_beat;
      nnNeuronCount_t num_neurons[RHYTHM_BRAIN_NUM_LAYERS];
      num_neurons[0] = n*RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS;
      num_neurons[RHYTHM_BRAIN_NUM_LAYERS-1] = n;
      for(n=1; n<RHYTHM_BRAIN_NUM_LAYERS-1; n++)
        num_neurons[n] = RHYTHM_BRAIN_NUM_HIDDEN_NEURONS;
      self->brain = rhythm_init_brain(RHYTHM_BRAIN_NUM_LAYERS, num_neurons);
      if(self->brain == NULL)
        return rhythm_destroy(self);

      nnSetHiddenLayersActivationFunction(self->brain, NN_ACTIVATION_SOFTPLUS);
      nnSetOutputLayerActivationFunction (self->brain, rhythm_activation_rectified_sigmoid, rhythm_activation_deriv_rectified_sigmoid);

      self->output_neurons = nnOutputBuffer(self->brain, &self->num_output_neurons);
      if(self->num_output_neurons != num_stroke_categories * subdivisions_per_beat)
        return rhythm_destroy(self);
        
      self->input_neurons = nnInputBuffer(self->brain, &self->num_input_neurons);
      if(self->num_input_neurons != self->num_output_neurons * RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS)
        return rhythm_destroy(self);
        
      self->target_output_neurons = calloc(self->num_output_neurons, sizeof(self->target_output_neurons[0]));
      if(self->target_output_neurons == NULL)
        return rhythm_destroy(self);
      
      nnSetAnnealingRate(self->brain, 1.0);
      
      if(pthread_mutex_init(&self->mutex, NULL) != 0)
        return rhythm_destroy(self);
    }
  return self;
}

/*----------------------------------------------------------*/
NeuralNetwork* rhythm_init_brain(int num_layers, nnNeuronCount_t num_neurons_in_layer[])
{
  int i;
  NeuralNetwork* brain = nnOpen(RHYTHM_BRAIN_PATH);
  if(brain != NULL)
    {
      if(num_layers != nnNumLayers(brain))
        brain = nnDestroy(brain);
      else
        for(i=0; i<num_layers; i++)
          {
            if(num_neurons_in_layer[i] != nnNumNeuronsInLayer(brain, i))
              {
                brain = nnDestroy(brain);
                break;
              }
          }
    }
  //don't make this 'else'
  if(brain == NULL)
  {
    fprintf(stderr, "unable to open brain!\r\n");
    brain = nnNewA(num_layers, num_neurons_in_layer);
    //errors checked in calling funciton
  }
  return brain;
}

/*----------------------------------------------------------*/
RhythmGenerator* rhythm_destroy(RhythmGenerator* self)
{
  if(self != NULL)
    {
      pthread_mutex_destroy(&self->mutex);
      if(self->pending_strokes != NULL)
        free(self->pending_strokes);
      if(self->brain != NULL)
        {
          nnSave(self->brain, RHYTHM_BRAIN_PATH);
          nnDestroy(self->brain);
        }
        
      free(self);
    }
  return (RhythmGenerator*)NULL;
}

/*----------------------------------------------------------*/
int rhythm_start_thread(RhythmGenerator* self)
{
  int error = 0;
  if(!self->thread_is_running)
    {
      self->thread_is_running = 1; //must do this first...
      self->current_subdivision = 0;
      error = pthread_create(&self->rhythm_generator_thread, NULL, rhythm_generator_thread_run_loop, self);
      if(error != 0) //failure
        self->thread_is_running = 0; //okay put it back
    }
  return !error;
}

/*----------------------------------------------------------*/
int rhythm_stop_thread(RhythmGenerator* self)
{
  self->thread_is_running = 0;
  pthread_join(self->rhythm_generator_thread, NULL);
  return 1;
}

/*----------------------------------------------------------*/
void rhythm_set_algorithm(RhythmGenerator* self, rhythm_algorithm_t algo)
{
  rhythm_stop_thread(self);
  //it could technically start again causing problems... Should look into that...
  //also should mutex self->strike;
  
  switch(algo)
    {
      case RHYTHM_IMITATE_STROKE_ALGORITHM:
        self->strike = rhythm_strike_imitate;
        self->current_algorithm = algo;
        break;

      case RHYTHM_DELAY_ALGORITHM:
        self->strike = rhythm_strike_delay;
        self->current_algorithm = algo;
        break;
        
      case RHYTHM_LEARN_ALGORITHM:
        self->strike = rhythm_strike_delay; //rhythm_strike_learn;
        self->current_algorithm = algo;
        break;
    
      default: break;
    }
}

/*----------------------------------------------------------*/
rhythm_algorithm_t rhythm_get_algorithm(RhythmGenerator* self)
{
  return self->current_algorithm;
}

/*----------------------------------------------------------*/
void               rhythm_set_learning_rate(RhythmGenerator* self, double rate)
{
  nnSetLearningRate(self->brain, rate);
}

/*----------------------------------------------------------*/
double             rhythm_get_learning_rate(RhythmGenerator* self)
{
  return nnLearningRate(self->brain);
}

/*----------------------------------------------------------*/
void rhythm_strike_imitate(RhythmGenerator* self, timestamp_microsecs_t input_timestamp, int category, double loudness)
{
  timestamp_microsecs_t onset_duration = input_timestamp - self->prev_input_timestamp;
  self->prev_input_timestamp           = input_timestamp;
  category                            %= self->num_stroke_categories;
  
  if(onset_duration > RHYTHM_MIN_BEAT_DURATION)
    self->strike_callback(self->strike_callback_arg, category, loudness);
}

/*----------------------------------------------------------*/
void rhythm_strike_delay(RhythmGenerator* self, timestamp_microsecs_t input_timestamp, int category, double loudness) 
{
  timestamp_microsecs_t onset_duration = input_timestamp - self->prev_input_timestamp;
  self->prev_input_timestamp           = input_timestamp;
  category                            %= self->num_stroke_categories;
  self->countdown_to_silence           = RHYTHM_BEATS_SILENCE_BERFORE_STOP * self->subdivisions_per_beat;
  
  if(!self->thread_is_running)
    { 
      if((onset_duration < RHYTHM_MAX_BEAT_DURATION) && (onset_duration > RHYTHM_MIN_BEAT_DURATION))
        {
          //self->beat_duration          =  480000;//onset_duration;
          self->beat_duration          =  onset_duration;
          if((2 * self->beat_duration) < self->prediction_duration) return; //this should not be possible
          self->thread_start_delay     =  (2 * self->beat_duration) - self->prediction_duration;
          self->prev_output_timestamp  =  input_timestamp + self->thread_start_delay - self->beat_duration; //beat duration gets modulo-ed out later for fast tempi
          self->subdivision_duration   =  self->beat_duration / self->subdivisions_per_beat;

          self->next_output_timestamp  = self->first_output_timestamp = input_timestamp + self->thread_start_delay;

          Rhythm_Stroke* stroke = &(self->pending_strokes[0]);
          stroke->loudness = loudness;
          stroke->category = category;
          stroke->deadline = input_timestamp + self->thread_start_delay; //unused
          rhythm_start_thread(self);
        }
    }
  else
    {
      //(self->current_subdivision > 0) handles the case in which a stroke arrives before the
      //rhythm thread is started (ie during thread_start_delay), because we don't know what time it is
      //unless it is running. there might be  better way of handling this (i.e. calculating what time it is).
      if((onset_duration > RHYTHM_MIN_STROKE_DURATION) && (self->current_subdivision > 0))
        {
          int index;
          double temp_index;
          
          pthread_mutex_lock(&self->mutex);
          
          //first term gets moduloed out later but corrects negative numbers when self->current_subdivision is small
          temp_index =  self->pending_strokes_size + self->current_subdivision - 1;
          temp_index += (((int64_t)input_timestamp)     - ((int64_t)self->prev_output_timestamp)
                        +((int64_t)self->thread_start_delay)) / (double)self->subdivision_duration;
          pthread_mutex_unlock(&self->mutex);
          
          index  = round(temp_index);
          index %= (self->pending_strokes_size);
          
          Rhythm_Stroke* stroke = &(self->pending_strokes[index]);
          stroke->loudness = loudness;
          stroke->category = category;
          stroke->deadline = input_timestamp + self->thread_start_delay;  //unused
        }
    }
}

/*----------------------------------------------------------*/
void rhythm_strike_learn(RhythmGenerator* self, timestamp_microsecs_t input_timestamp, int category, double loudness) 
{

}

/*----------------------------------------------------------*/
void               rhythm_forget_everything(RhythmGenerator* self)
{
  nnForgetTraining(self->brain);
}

/*----------------------------------------------------------*/
void rhythm_strike(RhythmGenerator* self, timestamp_microsecs_t input_timestamp, int category, double loudness)
{
  self->strike(self, input_timestamp, category, loudness);
}

/*----------------------------------------------------------*/
void test_print_neuron_buffer(RhythmGenerator* self, nnNeuronData_t* p, int n)
{
  int j, k;
  
  for(j=0; j<n; j+=self->num_stroke_categories)
    {
      if(j%(self->subdivisions_per_beat*self->num_stroke_categories) == 0)
        fprintf(stderr, "---------------------------\r\n");
      
      for(k=0; k<self->num_stroke_categories; k++)
        fprintf(stderr, "%f ", (float)*p++);
      fprintf(stderr, "\r\n");
        
    }
  fprintf(stderr, "\r\n\n");
}

/*----------------------------------------------------------*/
void* rhythm_generator_thread_run_loop(void* SELF)
{
  RhythmGenerator* self = (RhythmGenerator*)SELF;
  int countdown;
  timestamp_microsecs_t start;
  
  while(self->thread_is_running)
    {
      start = timestamp_get_current_time();
      if(start < (self->next_output_timestamp - 300))
        {
          usleep(500);  
          continue;
        }
      
      self->next_output_timestamp += self->subdivision_duration;
      pthread_mutex_lock(&self->mutex);
      
      self->prev_output_timestamp = start;
      if((self->current_subdivision % self->subdivisions_per_beat) == 0)
        self->metronome_callback(self->metronome_callback_arg);
        
 //---------------------------------------------------------------------------
      if(self->current_algorithm == RHYTHM_DELAY_ALGORITHM)
        {
          Rhythm_Stroke* current_stroke = &(self->pending_strokes[self->current_subdivision % self->pending_strokes_size]);
          if(current_stroke->category != -1)
            {
              self->strike_callback(self->strike_callback_arg, current_stroke->category, current_stroke->loudness);
              current_stroke->category = -1;
            }
        }
//---------------------------------------------------------------------------
      else if(self->current_algorithm == RHYTHM_LEARN_ALGORITHM)
        {
          int subdivision = self->current_subdivision % self->subdivisions_per_beat;
          int subdivision_offset = subdivision * self->num_stroke_categories;
          int i, index, category=0;
          nnNeuronData_t sum=0;
          Rhythm_Stroke* current_stroke;
          double prob = 0;
      
          if(subdivision == 0)
            {
              Rhythm_Stroke* curent_stroke;
              int training_input_index    = (self->pending_strokes_size + self->current_subdivision - (self->subdivisions_per_beat * (3+RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS) ));
              int prediction_input_index  = (self->pending_strokes_size + self->current_subdivision - (self->subdivisions_per_beat * (0+RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS) ));
              int target_output_index     = (self->pending_strokes_size + self->current_subdivision - (self->subdivisions_per_beat)) % self->pending_strokes_size;
            
              for(i=0; i<self->num_input_neurons; i++) self->input_neurons[i] = 0;
              for(i=0; i<RHYTHM_BRAIN_INPUT_LENGTH_IN_SUBDIVISIONS; i++)
                {
                  current_stroke = &(self->pending_strokes[(training_input_index + i) % self->pending_strokes_size]);
                  if(current_stroke->category != -1)
                    {
                      self->input_neurons[i * self->num_stroke_categories + current_stroke->category] = 1.0;
                      if(i < self->subdivisions_per_beat)  //clear out the first beat of the input buffer
                        current_stroke->category = -1;
                    }
                }

              for(i=0; i<self->num_output_neurons; self->target_output_neurons[i++] = 0);
              for(i=0; i<self->subdivisions_per_beat; i++)
                {                    
                  current_stroke = &(self->pending_strokes[(target_output_index + i) % self->pending_strokes_size]);
                  if(current_stroke->category != -1)
                    self->target_output_neurons[i * self->num_stroke_categories + current_stroke->category] = 1.0;
                }
                
              nnCalculateOutput(self->brain);

              //used in signal test in dissertation. Keep for reference
              //fprintf(stderr, "prob: %lf\r\n", rhythm_probability_of_playing_target_output(self));

              nnTrainOnce(self->brain, self->target_output_neurons, self->num_output_neurons);

              for(i=0; i<self->num_input_neurons; i++)
                self->input_neurons[i] = 0;
              for(i=0; i<RHYTHM_BRAIN_INPUT_LENGTH_IN_SUBDIVISIONS; i++)
                {
                  current_stroke = &(self->pending_strokes[(prediction_input_index + i) % self->pending_strokes_size]);
                  if(current_stroke->category != -1)
                    self->input_neurons[i * self->num_stroke_categories + current_stroke->category] = 1.0;
                }
              nnCalculateOutput(self->brain);
            }         

          prob = (double)random() / (double)RAND_MAX;
          for(i=0; i<self->num_stroke_categories; i++)
            {
              sum += self->output_neurons[subdivision_offset + i];
              if(sum > prob)
                {
                  self->strike_callback(self->strike_callback_arg, i, 1);
                  break;
                }
            }
        }
//---------------------------------------------------------------------------
      self->current_subdivision++;
      countdown = self->countdown_to_silence--;
      pthread_mutex_unlock(&self->mutex);
     
      if(countdown <= 0)
        break;
    }
    
  self->thread_is_running = 0;
  
  fprintf(stderr, "rhythm thread exit\r\n");
  return NULL;
}


/*---------------_-------------_---_-------------------------- 
  _____   ____ _| |_   _  __ _| |_(_) ___  _ __  
 / _ \ \ / / _` | | | | |/ _` | __| |/ _ \| '_ \ 
|  __/\ V / (_| | | |_| | (_| | |_| | (_) | | | |
 \___| \_/ \__,_|_|\__,_|\__,_|\__|_|\___/|_| |_|
 
 Below are *most* of the tests in chapter 6 of the
 dissertation, although some simple variants were
 hand-coded in and removed later. 
 ------------------------------------------------------------*/
 #include "RhythmLibrary.h" //keita rhythms
 
 typedef enum
 {
   RHYTHM_PRINT_NOTHING,
   RHYTHM_PRINT_PROBABILITY_OF_PLAYING_TARGET,
   RHYTHM_PRINT_VALUE_OF_LEAST_ACCURATE_CLUSTER,
   RHYTHM_PRINT_DENSITY_TEST,
   RHYTHM_PRINT_METER_TEST,
   RHYTHM_PRINT_SYNCOPATION_TEST,
 }rhythm_what_to_print_t;
 
 /*----------------------------------------------------------*/
//compares self->output_neurons to self->target_output_neurons
double           rhythm_probability_of_playing_target_output(RhythmGenerator* self)
{
  int i, j, contains_onset;
  double probability=1, temp;
  
  for(i=0; i<self->num_output_neurons; i+=self->num_stroke_categories)
    {
      contains_onset = 0;
      for(j=0; j<self->num_stroke_categories; j++)
        if(self->target_output_neurons[i+j] == 1)
          break;
      
      if(j==self->num_stroke_categories) //no onset in this timepoint
        {
          temp=0;
          for(j=0; j<self->num_stroke_categories; j++)
            temp += self->output_neurons[i+j];
          if(temp > 1) temp = 1;
              temp = 1.0-temp; 
        }
      else
        {
          temp = self->output_neurons[i+j];
          if(temp > 1) temp = 1;
        }
      probability *= temp;
    }
  
  return probability;
}

/*----------------------------------------------------------*/
//compares self->output_neurons to self->target_output_neurons
double           rhythm_probability_of_least_accurate_neuron(RhythmGenerator* self)
{
  int i, j, contains_onset;
  double min=100, temp;
  
  
  for(i=0; i<self->num_output_neurons; i++)
    {
      if(self->target_output_neurons[i] == 1)
        temp = self->output_neurons[i];
      else
        temp = 1 - self->output_neurons[i];
      if(temp < min) min = temp;
    }
  
  return min;
}

/*----------------------------------------------------------*/
//used to test density and syncopation
double rhythm_num_onsets(RhythmGenerator* self, double* neurons, int num_beats, int grid_spacing)
{
  int    i, j;
  int    num_timepoints = num_beats * self->subdivisions_per_beat;
  int    num_neurons = num_timepoints * self->num_stroke_categories;
  double observed_in_density=0;
  double sum, num_onsets=0;
  
  for(i=0; i<num_neurons; i+=self->num_stroke_categories * grid_spacing)
    {
      sum = 0;
      for(j=0; j<self->num_stroke_categories; j++)
        sum += neurons[i+j];
      if(sum > 1) sum = 1;
      num_onsets += sum;
    }

  return num_onsets;
}

/*----------------------------------------------------------*/
double rhythm_test_meter(RhythmGenerator* self, double* neurons, int num_beats, int grid_spacing, double* off_grid_probability)
{
  int    i, j;
  int    num_timepoints = num_beats * self->subdivisions_per_beat;
  int    num_neurons = num_timepoints * self->num_stroke_categories;
  double observed_in_density=0;
  double sum, correct=1, incorrect=1;
  
  for(i=0; i<num_neurons; i+=self->num_stroke_categories)
    {
      sum = 0;
      for(j=0; j<self->num_stroke_categories; j++)
        sum += neurons[i+j];
      if(sum > 1) sum = 1;
      sum = 1 - sum;
      
      if(((i/self->num_stroke_categories) % grid_spacing) == 0)
        correct *= sum;
      else 
        incorrect *= sum;
    }
  
  correct   = 1 -   correct;
  incorrect = 1 - incorrect;
  
  if(off_grid_probability != NULL)
    *off_grid_probability = incorrect;
  return correct;
}

/*----------------------------------------------------------*/
double rhythm_train_offline_once(RhythmGenerator* self, const int rhythm[], int num_beats, int grid_spacing, rhythm_what_to_print_t what_to_print)
{
  //not individual neurons -- uses the format in RhythmLibrary.c
  int i, category, beat, rhythm_index;
  long double probability=1;
  double min=100, temp;
  double observed_in_density=0, observed_out_density=0;
  double correct_n=0, incorrect_n=0, temp_incorrect_n;
  double sync_all_onsets_in =0, sync_downbeats_in =0;
  double sync_all_onsets_out=0, sync_downbeats_out=0;

  for(beat=0; beat<num_beats; beat++)
    {
      //training input
      for(i=0; i<self->num_input_neurons; self->input_neurons[i++]=0);
      rhythm_index = beat * self->subdivisions_per_beat;
      for(i=0; i<self->num_input_neurons; i+=self->num_stroke_categories)
        {
          category = rhythm[rhythm_index];
          if(category != -1)
            self->input_neurons[i + category] = 1.0;
          
          ++rhythm_index;
          rhythm_index %= num_beats * self->subdivisions_per_beat;
        }
      
      nnCalculateOutput(self->brain);
      
      //training target output
      for(i=0; i<self->num_output_neurons; self->target_output_neurons[i++]=0);
      rhythm_index = (beat + RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS - 1 + 3) * self->subdivisions_per_beat;
      rhythm_index %= num_beats * self->subdivisions_per_beat;
      for(i=0; i<self->num_output_neurons; i+=self->num_stroke_categories)
        {
          category = rhythm[rhythm_index];
          if(category != -1)
            self->target_output_neurons[i + category] = 1.0;
          ++rhythm_index;
          rhythm_index %= num_beats * self->subdivisions_per_beat;
        }
      
      nnTrainOnce(self->brain, self->target_output_neurons, self->num_output_neurons);
      
      //prediction input
      for(i=0; i<self->num_input_neurons; self->input_neurons[i++]=0);
      rhythm_index = ((beat+2) % num_beats) * self->subdivisions_per_beat;
      for(i=0; i<self->num_input_neurons; i+=self->num_stroke_categories)
        {
          category = rhythm[rhythm_index];
          if(category != -1)
            self->input_neurons[i + category] = 1.0;
          
          ++rhythm_index;
          rhythm_index %= num_beats * self->subdivisions_per_beat;
        }
      nnCalculateOutput(self->brain);
     
      //prediction target output
      for(i=0; i<self->num_output_neurons; self->target_output_neurons[i++]=0);
      rhythm_index = (((beat+2) % num_beats) + RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS - 1 + 3) * self->subdivisions_per_beat;
      rhythm_index %= num_beats * self->subdivisions_per_beat;
      for(i=0; i<self->num_output_neurons; i+=self->num_stroke_categories)
        {
          category = rhythm[rhythm_index];
          if(category != -1)
            self->target_output_neurons[i + category] = 1.0;
          ++rhythm_index;
          rhythm_index %= num_beats * self->subdivisions_per_beat;
        }
               
      observed_in_density   = rhythm_num_onsets(self, self->input_neurons,  RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS, 1);
      observed_out_density  = rhythm_num_onsets(self, self->output_neurons, 1, 1);
      observed_in_density  /= 6 * self->subdivisions_per_beat;
      observed_out_density /= self->subdivisions_per_beat;  
      correct_n             = rhythm_test_meter(self, self->output_neurons, 1, grid_spacing, &temp_incorrect_n);
      incorrect_n           = temp_incorrect_n;
      sync_all_onsets_out   = rhythm_num_onsets(self, self->output_neurons, 1, 3);
      sync_downbeats_out    = rhythm_num_onsets(self, self->output_neurons, 1, 6);
      sync_all_onsets_in    = rhythm_num_onsets(self, self->input_neurons , RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS, 3);
      sync_downbeats_in     = rhythm_num_onsets(self, self->input_neurons , RHYTHM_BRAIN_INPUT_LENGTH_IN_BEATS, 6);
      
      probability *= rhythm_probability_of_playing_target_output(self);
      temp = rhythm_probability_of_least_accurate_neuron(self);
      if(temp < min) min = temp; 
     
      if(what_to_print == RHYTHM_PRINT_DENSITY_TEST)
        fprintf(stderr, "%lf\t%lf\r\n", observed_in_density, observed_out_density);
        
      if(what_to_print == RHYTHM_PRINT_METER_TEST)
        fprintf(stderr, "grid_spacing: %i\tcorrect = %lf\tincorrect = %lf\r\n", grid_spacing, correct_n, incorrect_n);
    
      if(what_to_print == RHYTHM_PRINT_SYNCOPATION_TEST)
        fprintf(stderr, "%lf %lf\r\n", 1 - (sync_downbeats_in / sync_all_onsets_in), 1 - (sync_downbeats_out / sync_all_onsets_out));
    }
  
  if (what_to_print == RHYTHM_PRINT_PROBABILITY_OF_PLAYING_TARGET)
    fprintf(stderr, "prob: %Lf\r\n", probability);
  
  else if (what_to_print == RHYTHM_PRINT_VALUE_OF_LEAST_ACCURATE_CLUSTER)
    fprintf(stderr, "prob: %lf\r\n", min);
    
  return (what_to_print == RHYTHM_PRINT_VALUE_OF_LEAST_ACCURATE_CLUSTER) ? min : probability;
}

/*----------------------------------------------------------*/
void rhythm_train_keita_one_at_a_time(RhythmGenerator* self, double tolerance)
{
  int generation, part;
  double probability;
  double average=0;
  
  for(part=0; part<rhythm_library_num_unique_parts; part++)
    {
      nnForgetTraining(self->brain);
      //rhythm_train_zeros(self, tolerance);
      for(generation=0;generation<500; generation++)
        {
          fprintf(stderr, "part:%i\t%i\t", part, generation);
          probability = rhythm_train_offline_once(self, rhythm_library_unique_parts[part]->rhythm, rhythm_library_unique_parts[part]->len, 1, RHYTHM_PRINT_PROBABILITY_OF_PLAYING_TARGET);
          if(probability > (1-tolerance))
            break;
        }
      average += generation;
   }

  average /= (double)rhythm_library_num_unique_parts;
  fprintf(stderr, "avg num generations: %lf\r\n", average);
}

/*----------------------------------------------------------*/
void rhythm_train_keita_all_at_once(RhythmGenerator* self, double tolerance)
{
  int part, i, j;
  double prob, min_prob;
  tolerance = 1 - tolerance;
  for(i=0; i<800; i++)
    {
      fprintf(stderr, "KEITA TRAINING CYCLE %i\r\n", i);
      min_prob=1;
      for(part=0; part<rhythm_library_num_unique_parts; part++)
        {
          fprintf(stderr, "rhythm_index: %i\t", part);
          prob = rhythm_train_offline_once(self, rhythm_library_unique_parts[part]->rhythm, rhythm_library_unique_parts[part]->len, 1, RHYTHM_PRINT_PROBABILITY_OF_PLAYING_TARGET);
          if(prob < min_prob)
            min_prob = prob;
        }
      if(min_prob >= tolerance) break;
    }
  fprintf(stderr, "num generations: %i\r\n", i);
}

/*----------------------------------------------------------*/
void rhythm_train_signal(RhythmGenerator* self, double tolerance)
{
  int generation;
  double probability;
  
  for(generation=0;generation<2000;generation++)
    {
      fprintf(stderr, "%i\t", generation);
      probability  = rhythm_train_offline_once(self, yankadi_signal_macru_signal_part.rhythm, yankadi_signal_macru_signal_part.len, 1, RHYTHM_PRINT_VALUE_OF_LEAST_ACCURATE_CLUSTER);
      if(probability > (1-tolerance))
        break;
    }
}

/*----------------------------------------------------------*/
void rhythm_make_improvised_rhythm(RhythmGenerator* self, int* rhythm, int num_beats, double density, int grid_spacing)
{
  //not individual neurons -- uses the format in RhythmLibrary.c, taken by rhythm_train_once()
  int    i;
  int    num_timepoints = num_beats * self->subdivisions_per_beat;
  int    num_bytes      = num_timepoints * sizeof(*rhythm);
  double r;
 
  memset(rhythm, -1, num_bytes);
   
  for(i=0; i<num_timepoints; i+=grid_spacing)
    {
      r = random() / (long double)RAND_MAX;
      if(r < density)
        rhythm[i] = random() % self->num_stroke_categories;
    }
}

/*----------------------------------------------------------*/
void rhythm_train_density(RhythmGenerator* self)
{
  int    i;
  int*   rhythm;
  int    beats_per_density = 120;
  int    num_densities     = 50;
  int    num_beats = beats_per_density * num_densities;
  int    num_timepoints = num_beats * self->subdivisions_per_beat;
  int    num_bytes = num_timepoints * sizeof(*rhythm);
  double density;
  
  rhythm = malloc(num_bytes);
  if(rhythm == NULL) {fprintf(stderr, "stack overflow\r\n"); return;}
  
  for(i=0; i<num_beats; i+=beats_per_density)
    {
      density = random() / (long double)RAND_MAX;
      rhythm_make_improvised_rhythm(self, rhythm+(i*self->subdivisions_per_beat), beats_per_density, density, 1);
    }
  
  rhythm_train_offline_once(self, rhythm, num_beats, 1, RHYTHM_PRINT_DENSITY_TEST);
  
  free(rhythm);
}

/*----------------------------------------------------------*/
void rhythm_train_meter(RhythmGenerator* self)
{
  int    i;
  int    num_generations = 100;
  int*   rhythm;
  int    num_beats = 6;
  int    num_timepoints = num_beats * self->subdivisions_per_beat;
  int    num_bytes = num_timepoints * sizeof(*rhythm);
  double density = 0.4;
  
  rhythm = malloc(num_bytes);
  if(rhythm == NULL) {fprintf(stderr, "stack overflow\r\n"); return;}
  
  for(i=0; i<num_generations; i++)
    {
      rhythm_make_improvised_rhythm(self, rhythm, num_beats, 1.0/3.0, 4); //triplets
      rhythm_train_offline_once(self, rhythm, num_beats, 4, RHYTHM_PRINT_METER_TEST);
      rhythm_make_improvised_rhythm(self, rhythm, num_beats, 1.0/4.0, 3); //sixteenths (assuming 12 subdivisions per beat)
      rhythm_train_offline_once(self, rhythm, num_beats, 3, RHYTHM_PRINT_METER_TEST);
    }
  free(rhythm);
}

/*----------------------------------------------------------*/
void rhythm_make_syncopated_rhythm(RhythmGenerator* self, int* rhythm, int num_beats, double density, double syncopation_ratio)
{
  //not individual neurons -- uses the format in RhythmLibrary.c, taken by rhythm_train_once()
  int    i;
  int    num_timepoints = num_beats * self->subdivisions_per_beat;
  int    num_bytes      = num_timepoints * sizeof(*rhythm);
  double r;
  double test;
  int    grid_spacing = 3;
 
  double all = 0, sync = 0;
 
  memset(rhythm, -1, num_bytes);
   
  for(i=0; i<num_timepoints; i+=grid_spacing)
    {
      r = random() / (long double)RAND_MAX;
      test = density;
      if(((i%self->subdivisions_per_beat) == 3) || ((i%self->subdivisions_per_beat) == 9)) //syncopated part of beat
        test *= syncopation_ratio;
      else
        test *= 1-syncopation_ratio;
      if(r < test)
        {
          rhythm[i] = random() % self->num_stroke_categories;
          ++all;
          if(((i%self->subdivisions_per_beat) == 3) || ((i%self->subdivisions_per_beat) == 9)) //syncopated part of beat
            ++sync;
        }
    }
}

/*----------------------------------------------------------*/
void rhythm_train_syncopation(RhythmGenerator* self)
{
  int    i;
  double density = 1; //observed density will be half of this
  int*   rhythm;
  int    beats_per_syncopation_ratio = 120;
  int    num_syncopation_ratios      = 50;
  int    num_beats = beats_per_syncopation_ratio * num_syncopation_ratios;
  int    num_timepoints = num_beats * self->subdivisions_per_beat;
  int    num_bytes = num_timepoints * sizeof(*rhythm);
  double syncopation_ratio;
  
  rhythm = malloc(num_bytes);
  if(rhythm == NULL) {fprintf(stderr, "stack overflow\r\n"); return;}
  
  for(i=0; i<num_beats; i+=beats_per_syncopation_ratio)
    {
      syncopation_ratio = random() / (long double)RAND_MAX;
      rhythm_make_syncopated_rhythm(self, rhythm+(i*self->subdivisions_per_beat), beats_per_syncopation_ratio, density, syncopation_ratio);
    }
  
  rhythm_train_offline_once(self, rhythm, num_beats, 1, RHYTHM_PRINT_SYNCOPATION_TEST);
  
  free(rhythm);
}

/*----------------------------------------------------------*/
void rhythm_train_zeros(RhythmGenerator* self, double tolerance)
{
  int i, j, generation=0;
  double density, p;
  
  for(i=0; i<self->num_output_neurons; self->target_output_neurons[i++]=0);
  
  for(generation=0;generation<1000;generation++)
    {
      for(i=0; i<self->num_input_neurons; self->input_neurons[i++]=0);
      //density = random() / (long double)RAND_MAX;
      
      density = 0.0;
      for(i=0; i<self->num_input_neurons; i+=self->num_stroke_categories)
        if((random() / (long double)RAND_MAX) < density)
          self->input_neurons [i + (random() % self->num_stroke_categories)] = 1;
          
      nnCalculateOutput(self->brain);
      
      p=rhythm_probability_of_playing_target_output(self);
      if(p >= (1-tolerance)) break;

      nnTrainOnce(self->brain, self->target_output_neurons, self->num_output_neurons);
    }
    
    fprintf(stderr, "zeroed: generation: %i\tprob: %lf\r\n", (int)generation, (double)p);
}



