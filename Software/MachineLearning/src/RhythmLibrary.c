//learn rhythms of different, non-multiple lengths...
//const interactive learning of timbral context
//not signals or entrances (repeated)
//learning another part via feedback (network output->input)


#include "RhythmLibrary.h"

#define RHYTHM_LIBRARY_ARRAY_LENGTH(array_name) sizeof((array_name)) / sizeof(*(array_name))
#define RHYTHM_LIBRARY_ARRAY_NUM_TIMEPOINTS(array_name) RHYTHM_LIBRARY_ARRAY_LENGTH((array_name))
#define RHYTHM_LIBRARY_ARRAY_NUM_BEATS(array_name) RHYTHM_LIBRARY_ARRAY_NUM_TIMEPOINTS(array_name) / RHYTHM_LIBRARY_TIMEPOINTS_PER_BEAT
#define RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(new_array_name, old_array_name) const rhythm_library_part_t new_array_name ## _part = {old_array_name, RHYTHM_LIBRARY_ARRAY_NUM_BEATS(old_array_name)}
#define RHYTHM_LIBRARY_MAKE_PART(array_name) RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(array_name, array_name)

/*----------------------------------------------------*/
const int soli_lent_djembe_1[] = 
{
// *           *           *           *
   0, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1,  1, -1, -1,
   1, -1, -1, -1, -1, -1,  0, -1, -1,  2, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(soli_lent_djembe_1);

/*----------------------------------------------------*/
const int soli_lent_djembe_2[] = 
{
// *           *           *           *
   2, -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
   2, -1, -1, -1, -1, -1,  0, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(soli_lent_djembe_2);

/*----------------------------------------------------*/
const int soli_rapide_djembe_1[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(soli_rapide_djembe_1);

/*----------------------------------------------------*/
const int soli_rapide_djembe_2[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   2, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
};
RHYTHM_LIBRARY_MAKE_PART(soli_rapide_djembe_2);

/*----------------------------------------------------*/
const int soli_manian_djembe_1[] = 
{
// *               *               *            
   0, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(soli_manian_djembe_1);

/*----------------------------------------------------*/
const int soli_manian_djembe_2[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1,
   1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(soli_manian_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(soko_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
const int soko_djembe_2[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   2, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(soko_djembe_2);

/*----------------------------------------------------*/
const int balakulandjan_djembe_1[] = 
{
// *           *           *           *
   2, -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(balakulandjan_djembe_1);

/*----------------------------------------------------*/
const int balakulandjan_djembe_2[] = 
{
// *           *           *           *
   0, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
};
RHYTHM_LIBRARY_MAKE_PART(balakulandjan_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(saa_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(saa_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
const int toro_djembe_1[] = 
{
// *           *           *           *
   1, -1, -1,  1, -1, -1, -1, -1, -1,  0, -1, -1,
   2, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1,  0, -1, -1,
   2, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(toro_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(toro_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
const int bao_djembe_1[] = 
{
// *               *               *            
   1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(bao_djembe_1);

/*----------------------------------------------------*/
const int bao_djembe_2[] = 
{
// *               *               *            
   1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   0, -1, -1, -1,  1, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(bao_djembe_2);

/*----------------------------------------------------*/
const int ngoron_djembe_1[] = 
{
// *           *           *           *
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1,  0, -1, -1,  2, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1,  0, -1, -1,  2, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   2, -1, -1,  2, -1, -1,  2, -1, -1, -1, -1, -1,
   2, -1, -1,  2, -1, -1,  2, -1, -1, -1, -1, -1,
};
RHYTHM_LIBRARY_MAKE_PART(ngoron_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(ngoron_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(donaba_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(donaba_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(dunungbe_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(dunungbe_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(konowulen_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(konowulen_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kurabadon_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kurabadon_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(nantalomba_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(nantalomba_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(mendiani_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(mendiani_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
const int moribayassa_djembe_1[] = 
{
// *           *           *           *
   0, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(moribayassa_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(moribayassa_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
const int djabarra_djembe_1[] = 
{
// *               *               *            
   1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(djabarra_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(djabarra_djembe_2, soli_manian_djembe_2);

/*----------------------------------------------------*/
const int kakilambe_djembe_1[] = 
{
// *               *               *            
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kakilambe_djembe_1);

/*----------------------------------------------------*/
const int kakilambe_djembe_2[] = 
{
// *               *               *            
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  2, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kakilambe_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(konden_1_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(konden_1_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(konden_2_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(konden_2_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(sorsonet_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(sorsonet_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
const int mamaya_djembe_1[] = 
{
// *               *               *            
   1, -1, -1, -1,  1, -1, -1, -1,  2, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  2, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
};
RHYTHM_LIBRARY_MAKE_PART(mamaya_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(mamaya_djembe_2, soli_rapide_djembe_1);

/*----------------------------------------------------*/
const int garangedon_djembe_1[] = 
{
// *               *               *            
   0, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   0, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  2, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
};
RHYTHM_LIBRARY_MAKE_PART(garangedon_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(garangedon_djembe_2, soli_rapide_djembe_1);

/*----------------------------------------------------*/
const int kotedjuga_djembe_1[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   0, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kotedjuga_djembe_1);

/*----------------------------------------------------*/
const int kotedjuga_djembe_2[] = 
{
// *               *               *            
   1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kotedjuga_djembe_2);

/*----------------------------------------------------*/
const int kassa_djembe_1[] = 
{
// *           *           *           *
   1, -1, -1,  1, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   1, -1, -1,  1, -1, -1,  2, -1, -1,  2, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
};
RHYTHM_LIBRARY_MAKE_PART(kassa_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kassa_djembe_2, soli_lent_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kassa_soro_djembe_1, kassa_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kassa_soro_djembe_2, soli_lent_djembe_2);

/*----------------------------------------------------*/
const int konkoba_1_djembe_1[] = 
{
// *               *               *            
   1, -1, -1, -1,  1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1,  0, -1, -1, -1, -1,  2, -1, -1,
  -1, -1, -1, -1,  0, -1, -1, -1, -1,  2, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(konkoba_1_djembe_1);

/*----------------------------------------------------*/
const int konkoba_1_djembe_2[] = 
{
// *               *               *            
   1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(konkoba_1_djembe_2);

/*----------------------------------------------------*/
const int konkoba_2_djembe_1[] = 
{
// *               *               *            
   1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(konkoba_2_djembe_1);

/*----------------------------------------------------*/
const int konkoba_2_djembe_2[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1,
   1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
};
RHYTHM_LIBRARY_MAKE_PART(konkoba_2_djembe_2);

/*----------------------------------------------------*/
const int senefoli_djembe_1[] = 
{
// *           *           *           *
   2, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
   2, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(senefoli_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(senefoli_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(namani_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(namani_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kawa_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kawa_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
const int woima_djembe_1[] = 
{
// *           *           *           *
  -1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(woima_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(woima_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
const int soliwoulen_djembe_1[] = 
{
// *           *           *           *
   2, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1,  2, -1, -1,  1, -1, -1,  1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(soliwoulen_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(soliwoulen_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
const int kontemuru_djembe_1[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1,
   2, -1, -1, -1,  0, -1, -1, -1,  2, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kontemuru_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kontemuru_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
const int soboninkun_djembe_1[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(soboninkun_djembe_1);

/*----------------------------------------------------*/
const int soboninkun_djembe_2[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   2, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(soboninkun_djembe_2);

/*----------------------------------------------------*/
const int tiriba_djembe_1[] = 
{
// *               *               *            
   0, -1, -1, -1,  2, -1, -1, -1,  1, -1, -1, -1,
  -1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(tiriba_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(tiriba_djembe_2, kakilambe_djembe_1);

/*----------------------------------------------------*/
const int tiriba_djembe_3[] = 
{
// *               *               *            
   0, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   0, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(tiriba_djembe_3);

/*----------------------------------------------------*/
const int zaouli_a_djembe_1[] = 
{
// *           *           *           *
   2, -1, -1, -1, -1, -1, -1, -1, -1,  0, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1,  0, -1, -1,
   1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(zaouli_a_djembe_1);

/*----------------------------------------------------*/
const int zaouli_a_djembe_2[] = 
{
// *           *           *           *
  -1, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(zaouli_a_djembe_2);

/*----------------------------------------------------*/
const int zaouli_b_djembe_1[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(zaouli_b_djembe_1);

/*----------------------------------------------------*/
const int zaouli_b_djembe_2[] = 
{
// *               *               *            
   2, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
  -1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   2, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
   2, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(zaouli_b_djembe_2);

/*----------------------------------------------------*/
const int abondan_djembe_1[] = 
{
// *               *               *            
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  2, -1, -1, -1,
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(abondan_djembe_1);

/*----------------------------------------------------*/
const int abondan_djembe_2[] = 
{
// *               *               *            
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1,
   0, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1,
   1, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(abondan_djembe_2);

/*----------------------------------------------------*/
const int sofa_djembe_1[] = 
{
// *           *           *           *
   1, -1, -1,  1, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   1, -1, -1,  1, -1, -1, -1, -1, -1,  2, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(sofa_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(sofa_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
const int fankani_djembe_1[] = 
{
// *           *           *           *
   2, -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1,  0, -1, -1, -1, -1, -1,
   2, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
   2, -1, -1, -1, -1, -1,  0, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(fankani_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(fankani_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
const int denadon_djembe_1[] = 
{
// *           *           *           *
   2, -1, -1,  2, -1, -1, -1, -1, -1,  0, -1, -1,
   1, -1, -1, -1, -1, -1,  1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(denadon_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(denadon_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(djagbe_djembe_1, soli_lent_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(djagbe_djembe_2, balakulandjan_djembe_2);

/*----------------------------------------------------*/
const int djagbewara_djembe_1[] = 
{
// *               *               *            
  -0, -1, -1, -1,  2, -1, -1, -1,  1, -1, -1, -1,
   1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(djagbewara_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(djagbewara_djembe_2, soli_rapide_djembe_1);

/*----------------------------------------------------*/
const int marakadon_djembe_1[] = 
{
// *               *               *            
   1, -1, -1, -1,  2, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(marakadon_djembe_1);

/*----------------------------------------------------*/
const int marakadon_djembe_2[] = 
{
// *               *               *            
   2, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1,  0, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(marakadon_djembe_2);

/*----------------------------------------------------*/
const int sunun_djembe_1[] = 
{
// *           *           *           *
   1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1,  1, -1, -1,
   1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1,  1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(sunun_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(sunun_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
const int djansa_djembe_1[] = 
{
// *           *           *           *
   2, -1, -1,  2, -1, -1, -1, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
   2, -1, -1, -1, -1, -1,  0, -1, -1, -1, -1, -1,
   2, -1, -1,  0, -1, -1,  1, -1, -1,  1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(djansa_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(djansa_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(gidamba_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(gidamba_djembe_2, soko_djembe_2);

/*----------------------------------------------------*/
const int djole_djembe_1[] = 
{
// *           *           *           *
   0, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(djole_djembe_1);

/*----------------------------------------------------*/
const int djole_djembe_2[] = 
{
// *           *           *           *
   0, -1, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1,
   1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1,  1, -1, -1,  1, -1, -1,  1, -1, -1,
   1, -1, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1,
   1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(djole_djembe_2);

/*----------------------------------------------------*/
const int wassolonka_djembe_1[] = 
{
// *               *               *            
   2, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
   2, -1, -1, -1,  2, -1, -1, -1,  1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(wassolonka_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(wassolonka_djembe_2, soli_rapide_djembe_1);

/*----------------------------------------------------*/
const int yankadi_signal[] = 
{
// *           *           *           *
   1, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1,  1, -1, -1, -1, -1, -1,  1, -1, -1,
   1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(yankadi_signal);

/*----------------------------------------------------*/
const int yankadi_signal_macru_signal[] = 
{
// *           *           *           *
 //Yankadi Djembe 1
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   0, -1, -1,  1, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   0, -1, -1,  1, -1, -1,  1, -1, -1, -1, -1, -1,
 //Yankadi Djembe 1 
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   0, -1, -1,  1, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   0, -1, -1,  1, -1, -1,  1, -1, -1, -1, -1, -1,
 //Yankadi Djembe 1 
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   0, -1, -1,  1, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   0, -1, -1,  1, -1, -1,  1, -1, -1, -1, -1, -1,
 //Signal  
   1, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1,  1, -1, -1, -1, -1, -1,  1, -1, -1,
   1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
 //Makru Djembe 3   
   0, -1, -1,  1, -1, -1, -1, -1, -1,  1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
 //Makru Djembe 3   
   0, -1, -1,  1, -1, -1, -1, -1, -1,  1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
 //Makru Djembe 3   
   0, -1, -1,  1, -1, -1, -1, -1, -1,  1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
 //Signal   
   1, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1,  1, -1, -1, -1, -1, -1,  1, -1, -1,
   1, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(yankadi_signal_macru_signal);

/*----------------------------------------------------*/
const int yankadi_djembe_1[] = 
{
// *           *           *           *
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   0, -1, -1,  1, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   0, -1, -1,  1, -1, -1,  1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(yankadi_djembe_1);

/*----------------------------------------------------*/
const int yankadi_djembe_2[] = 
{
// *           *           *           *
   2, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   1, -1, -1, -1, -1, -1,  1, -1, -1, -1, -1, -1,
   2, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(yankadi_djembe_2);

/*----------------------------------------------------*/
const int makru_djembe_1[] = 
{
// *           *           *           *
   0, -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1,
  -1, -1, -1,  1, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
  -1, -1, -1,  1, -1, -1,  1, -1, -1, -1, -1, -1

};
RHYTHM_LIBRARY_MAKE_PART(makru_djembe_1);

/*----------------------------------------------------*/
const int makru_djembe_2[] = 
{
// *           *           *           *
   1, -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1,
   1, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(makru_djembe_2);

/*----------------------------------------------------*/
const int makru_djembe_3[] = 
{
// *           *           *           *
   0, -1, -1,  1, -1, -1, -1, -1, -1,  1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(makru_djembe_3);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(djaa_1_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(djaa_1_djembe_2, soko_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(djaa_2_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(djaa_2_djembe_2, soko_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(dallah_djembe_1, zaouli_a_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(dallah_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
const int kuku_djembe_1[] = 
{
// *           *           *           *
   0, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kuku_djembe_1);

/*----------------------------------------------------*/
const int kuku_djembe_2[] = 
{
// *           *           *           *
   1, -1, -1,  1, -1, -1, -1, -1, -1,  2, -1, -1,
   1, -1, -1,  1, -1, -1,  2, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kuku_djembe_2);

/*----------------------------------------------------*/
const int kuku_djembe_3[] = 
{
// *           *           *           *
   1, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
   0, -1, -1,  2, -1, -1,  2, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kuku_djembe_3);

/*----------------------------------------------------*/
const int kuku_djembe_4[] = 
{
// *           *           *           *
   1, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   2, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  1, -1, -1,  1, -1, -1,
  -1, -1, -1, -1, -1, -1,  1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kuku_djembe_4);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kanin_djembe_1, kuku_djembe_4);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kanin_djembe_2, balakulandjan_djembe_1);

/*----------------------------------------------------*/
const int kono_djembe_1[] = 
{
// *           *           *           *
   0, -1, -1,  1, -1, -1,  1, -1, -1,  1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kono_djembe_1);

/*----------------------------------------------------*/
const int kono_djembe_2[] = 
{
// *           *           *           *
   0, -1, -1,  1, -1, -1,  1, -1, -1,  1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
   0, -1, -1,  1, -1, -1,  1, -1, -1,  1, -1, -1,
   2, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(kono_djembe_2);

/*----------------------------------------------------*/
const int liberte_1_djembe_1[] = 
{
// *               *               *            
   0, -1, -1, -1,  2, -1, -1, -1,  2, -1, -1, -1,
  -1, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
  -1, -1, -1, -1,  2, -1, -1, -1,  2, -1, -1, -1,
   2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(liberte_1_djembe_1);

/*----------------------------------------------------*/
const int liberte_1_djembe_2[] = 
{
// *               *               *            
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
   0, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1,
   0, -1, -1, -1,  1, -1, -1, -1,  1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(liberte_1_djembe_2);

/*----------------------------------------------------*/
const int liberte_2_djembe_1[] = 
{
// *           *           *           *
   2, -1, -1,  2, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1,  0, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1,  0, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1,  0, -1, -1,  1, -1, -1, -1, -1, -1,
   0, -1, -1, -1, -1, -1,  2, -1, -1,  2, -1, -1,
  -1, -1, -1,  0, -1, -1,  1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(liberte_2_djembe_1);

/*----------------------------------------------------*/
const int liberte_2_djembe_2[] = 
{
// *           *           *           *
   2, -1, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1,
   1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};
RHYTHM_LIBRARY_MAKE_PART(liberte_2_djembe_2);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kele_m_se_djembe_1, soli_rapide_djembe_1);

/*----------------------------------------------------*/
RHYTHM_LIBRARY_MAKE_DUPLICATE_PART(kele_m_se_djembe_2, soli_rapide_djembe_2);

/*----------------------------------------------------*/
const rhythm_library_part_t *rhythm_library_parts[] =
{
  &soli_lent_djembe_1_part,
  &soli_lent_djembe_2_part,
  &soli_rapide_djembe_1_part,
  &soli_rapide_djembe_2_part,
  &soli_manian_djembe_1_part,
  &soli_manian_djembe_2_part,
  &soko_djembe_1_part,
  &soko_djembe_2_part,
  &balakulandjan_djembe_1_part,
  &balakulandjan_djembe_2_part,
  &saa_djembe_1_part,
  &saa_djembe_2_part,
  &toro_djembe_1_part,
  &toro_djembe_2_part,
  &bao_djembe_1_part,
  &bao_djembe_2_part,
  &ngoron_djembe_1_part,
  &ngoron_djembe_2_part,
  &donaba_djembe_1_part,
  &donaba_djembe_2_part,
  &dunungbe_djembe_1_part,
  &dunungbe_djembe_2_part,
  &konowulen_djembe_1_part,
  &konowulen_djembe_2_part,
  &kurabadon_djembe_1_part,
  &kurabadon_djembe_2_part,
  &nantalomba_djembe_1_part,
  &nantalomba_djembe_2_part,
  &mendiani_djembe_1_part,
  &mendiani_djembe_2_part,
  &moribayassa_djembe_1_part,
  &moribayassa_djembe_2_part,
  &djabarra_djembe_1_part,
  &djabarra_djembe_2_part,
  &kakilambe_djembe_1_part,
  &kakilambe_djembe_2_part,
  &konden_1_djembe_1_part,
  &konden_1_djembe_2_part,
  &konden_2_djembe_1_part,
  &konden_2_djembe_2_part,
  &sorsonet_djembe_1_part,
  &sorsonet_djembe_2_part,
  &mamaya_djembe_1_part,
  &mamaya_djembe_2_part,
  &garangedon_djembe_1_part,
  &garangedon_djembe_2_part,
  &kotedjuga_djembe_1_part,
  &kotedjuga_djembe_2_part,
  &kassa_djembe_1_part,
  &kassa_djembe_2_part,
  &kassa_soro_djembe_1_part,
  &kassa_soro_djembe_2_part,
  &konkoba_1_djembe_1_part,
  &konkoba_1_djembe_2_part,
  &konkoba_2_djembe_1_part,
  &konkoba_2_djembe_2_part,
  &senefoli_djembe_1_part,
  &senefoli_djembe_2_part,
  &namani_djembe_1_part,
  &namani_djembe_2_part,
  &kawa_djembe_1_part,
  &kawa_djembe_2_part,
  &woima_djembe_1_part,
  &woima_djembe_2_part,
  &soliwoulen_djembe_1_part,
  &soliwoulen_djembe_2_part,
  &kontemuru_djembe_1_part,
  &kontemuru_djembe_2_part,
  &soboninkun_djembe_1_part,
  &soboninkun_djembe_2_part,
  &tiriba_djembe_1_part,
  &tiriba_djembe_2_part,
  &tiriba_djembe_3_part,
  &zaouli_a_djembe_1_part,
  &zaouli_a_djembe_2_part,
  &zaouli_b_djembe_1_part,
  &zaouli_b_djembe_2_part,
  &abondan_djembe_1_part,
  &abondan_djembe_2_part,
  &sofa_djembe_1_part,
  &sofa_djembe_2_part,
  &fankani_djembe_1_part,
  &fankani_djembe_2_part,
  &denadon_djembe_1_part,
  &denadon_djembe_2_part,
  &djagbe_djembe_1_part,
  &djagbe_djembe_2_part,
  &djagbewara_djembe_1_part,
  &djagbewara_djembe_2_part,
  &marakadon_djembe_1_part,
  &marakadon_djembe_2_part,
  &sunun_djembe_1_part,
  &sunun_djembe_2_part,
  &djansa_djembe_1_part,
  &djansa_djembe_2_part,
  &gidamba_djembe_1_part,
  &gidamba_djembe_2_part,
  &djole_djembe_1_part,
  &djole_djembe_2_part,
  &wassolonka_djembe_1_part,
  &wassolonka_djembe_2_part,
  &yankadi_djembe_1_part,
  &yankadi_djembe_2_part,
  &makru_djembe_1_part,
  &makru_djembe_2_part,
  &makru_djembe_3_part,
  &djaa_1_djembe_1_part,
  &djaa_1_djembe_2_part,
  &djaa_2_djembe_1_part,
  &djaa_2_djembe_2_part,
  &dallah_djembe_1_part,
  &dallah_djembe_2_part,
  &kuku_djembe_1_part,
  &kuku_djembe_2_part,
  &kuku_djembe_3_part,
  &kuku_djembe_4_part,
  &kanin_djembe_1_part,
  &kanin_djembe_2_part,
  &kono_djembe_1_part,
  &kono_djembe_2_part,
  &liberte_1_djembe_1_part,
  &liberte_1_djembe_2_part,
  &liberte_2_djembe_1_part,
  &liberte_2_djembe_2_part
};
const int rhythm_library_num_parts = RHYTHM_LIBRARY_ARRAY_LENGTH(rhythm_library_parts);

/*----------------------------------------------------*/
const rhythm_library_part_t *rhythm_library_unique_parts[] =
{
  &soli_lent_djembe_1_part,
  &soli_lent_djembe_2_part,
  &soli_rapide_djembe_1_part,
  &soli_rapide_djembe_2_part,
  &soli_manian_djembe_1_part,
  &soli_manian_djembe_2_part,
  &soko_djembe_2_part,
  &balakulandjan_djembe_1_part,
  &balakulandjan_djembe_2_part,
  &toro_djembe_1_part,
  &bao_djembe_1_part,
  &bao_djembe_2_part,
  &ngoron_djembe_1_part,
  &moribayassa_djembe_1_part,
  &djabarra_djembe_1_part,
  &kakilambe_djembe_1_part,
  &kakilambe_djembe_2_part,
  &mamaya_djembe_1_part,
  &garangedon_djembe_1_part,
  &kotedjuga_djembe_1_part,
  &kotedjuga_djembe_2_part,
  &kassa_djembe_1_part,
  &konkoba_1_djembe_1_part,
  &konkoba_1_djembe_2_part,
  &konkoba_2_djembe_1_part,
  &konkoba_2_djembe_2_part,
  &senefoli_djembe_1_part,
  &woima_djembe_1_part,
  &soliwoulen_djembe_1_part,
  &kontemuru_djembe_1_part,
  &soboninkun_djembe_1_part,
  &soboninkun_djembe_2_part,
  &tiriba_djembe_1_part,
  &tiriba_djembe_3_part,
  &zaouli_a_djembe_1_part,
  &zaouli_a_djembe_2_part,
  &zaouli_b_djembe_1_part,
  &zaouli_b_djembe_2_part,
  &abondan_djembe_1_part,
  &abondan_djembe_2_part,
  &sofa_djembe_1_part,
  &fankani_djembe_1_part,
  &denadon_djembe_1_part,
  &djagbewara_djembe_1_part,
  &marakadon_djembe_1_part,
  &marakadon_djembe_2_part,
  &sunun_djembe_1_part,
  &djansa_djembe_1_part,
  &djole_djembe_1_part,
  &djole_djembe_2_part,
  &wassolonka_djembe_1_part,
  &yankadi_djembe_1_part,
  &yankadi_djembe_2_part,
  &makru_djembe_1_part,
  &makru_djembe_2_part,
  &makru_djembe_3_part,
  &kuku_djembe_1_part,
  &kuku_djembe_2_part,
  &kuku_djembe_3_part,
  &kuku_djembe_4_part,
  &kono_djembe_1_part,
  &kono_djembe_2_part,
  &liberte_1_djembe_1_part,
  &liberte_1_djembe_2_part,
  &liberte_2_djembe_1_part,
  &liberte_2_djembe_2_part
};
const int rhythm_library_num_unique_parts = RHYTHM_LIBRARY_ARRAY_LENGTH(rhythm_library_unique_parts);












