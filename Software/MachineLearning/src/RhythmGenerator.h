#ifndef __MK_RHYTHM_GENERATOR__
#define __MK_RHYTHM_GENERATOR__


#if defined(__cplusplus)
extern "C"{
#endif   //(__cplusplus)

#include <stdint.h>

#include "Timestamp.h"

/*----------------------------------------------------------*/
typedef struct opaque_rhythm_generator_strcut RhythmGenerator;

typedef void (*rhythm_metronome_callback_t)(void* callback_arg);
typedef void (*rhythm_strike_callback_t)   (void* callback_arg, int stroke_category, double loudness);

/*----------------------------------------------------------*/
typedef enum rhythm_algorithm_enum
{
  RHYTHM_IMITATE_STROKE_ALGORITHM,
  RHYTHM_DELAY_ALGORITHM,
  RHYTHM_LEARN_ALGORITHM,
  RHYTHM_NUM_ALGORITHMS
}rhythm_algorithm_t;

/*----------------------------------------------------------*/
RhythmGenerator*  rhythm_new           (unsigned subdivisions_per_beat, 
                                        unsigned num_stroke_categories, 
                                        rhythm_metronome_callback_t metronome_callback,
                                        void* metronome_callback_arg,
                                        rhythm_strike_callback_t strike_callback,
                                        void* strike_callback_arg,
                                        timestamp_microsecs_t prediction_duration);
RhythmGenerator*   rhythm_destroy      (RhythmGenerator* self);
void               rhythm_strike       (RhythmGenerator* self, timestamp_microsecs_t onset_timestamp, int stroke_category, double loudness);
void               rhythm_set_algorithm(RhythmGenerator* self, rhythm_algorithm_t algo);
rhythm_algorithm_t rhythm_get_algorithm(RhythmGenerator* self);

void               rhythm_set_learning_rate(RhythmGenerator* self, double rate);
double             rhythm_get_learning_rate(RhythmGenerator* self);
void               rhythm_forget_everything(RhythmGenerator* self);
void               rhythm_train_zeros(RhythmGenerator* self, double tolerance);

//these run tests in chapter 6 of dissertation.
void               rhythm_train_keita_one_at_a_time(RhythmGenerator* self, double tolerance);
void               rhythm_train_keita_all_at_once(RhythmGenerator* self, double tolerance);
void               rhythm_train_signal(RhythmGenerator* self, double tolerance);
void               rhythm_train_density(RhythmGenerator* self);
void               rhythm_train_meter(RhythmGenerator* self);
void               rhythm_train_syncopation(RhythmGenerator* self);

#if defined(__cplusplus)
}
#endif   //(__cplusplus)

#endif //__MK_RHYTHM_GENERATOR__