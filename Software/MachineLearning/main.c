  //gcc *.c Kiki_Communication_Framework/*.c -framework AudioToolbox -framework CoreMidi -framework Carbon

#include <pthread.h>
#include "src/OSC.h"
#include "src/Wifi.h"
#include "src/Timestamp.h"
#include "src/Microphone.h"
#include "src/StrokeClassifier.h"
#include "src/RhythmGenerator.h"
#include "src/RhythmLibrary.h"
#include "src/SimulatedAnnealing.h"
#include "src/Util.h"
#include "../Kiki_Communication_Framework/Kiki_Communication.h"

#define NUM_INPUT_AUDIO_CHANNELS 2
#define KIKI_DELAY_TIME_USEC 600000
#define RHYTHM_SUBDIVISIONS_PER_BEAT /*12*/ 4
#define OUTPUT_PORT 1234
#define INPUT_PORT  4321
#define HUMAN_AUDIO_CHANNEL 0
#define ROBOT_AUDIO_CHANNEL 1

#define ARM_FASTEST_REPETITION_RATE 300000 //usec for repeated strokes
#define ARM_FASTEST_PLAYING_RATE    400000 //usec for different strokes

#define NUM_ANNEALING_REJECTIONS_BEFORE_RESET 10

enum annealing_features_enum
{
  ARM_X_INDEX = 0,
  ARM_Y_INDEX,
  ARM_A_INDEX,
  ARM_SPEED_INDEX,
  NUM_ANNEALING_FEATURES,
};

/*----------------------------------------------------------*/
typedef struct main_program_struct
{
  Audio*                audio_input;
  BOOL                  should_generate_rhythm;
  RhythmGenerator*      generator;
  Kiki*                 kiki;
  
  //these should be states;
  BOOL                  listening_for_annealing_target;
  BOOL                  awaiting_kikis_response;
  BOOL                  awaiting_annealing_stroke;
  ClassifierExample     annealing_target, annealing_candidate;
  double                annealing_features[NUM_ANNEALING_FEATURES];
  double                best_annealed_features[NUM_ANNEALING_FEATURES];
  int                   annealing_arm;
  int                   annealing_stroke;
  
  int                   prev_arm_category;
  timestamp_microsecs_t prev_arm_time;
  int                   prev_solenoid;
}Main;


void  kiki_recieve_callback(void* self, char* message, kiki_arg_t args[], int num_args);

void  human_onset_detected_callback(void* SELF, ClassifierExample* e, timestamp_microsecs_t onset_timestamp);
void  robot_onset_detected_callback(void* SELF, ClassifierExample* e, timestamp_microsecs_t onset_timestamp);
void  metronome_callback(void* SELF);
void  strike_callback(void* SELF, int stroke_category, double loudness);

void  rhythm_generator_subroutine(RhythmGenerator* rhythm);
void  stroke_classifier_tuner_subroutine(StrokeClassifier* classifier, const char* classifier_name);
void  simulated_annealing_subroutine(Main* self);
void* simulated_annealing_thread(void* SELF);
void  annealing_make_neighbour_callback(void* SELF, double* features, double T);
double annealing_cost_callback(void* SELF, double features[]);

void iHateCanonicalInputProcessingIReallyReallyDo(void);
void makeStdinCannonicalAgain();

/*----------------------------------------------------------*/
Main* main_destroy(Main* self)
{
  if(self != NULL)
    {
      auSubclassDestroy(self->audio_input);
      rhythm_destroy(self->generator);
      kiki_destroy(self->kiki);
      free(self);
    }
  return (Main*)NULL;
}

/*----------------------------------------------------------*/
Main* main_new(int argc, char* argv[])
{
  Main* self = calloc(1, sizeof(*self));
  if(self != NULL)
    {
      StrokeClassifier* classifier;
      
      self->audio_input     = NULL;

      self->kiki = kiki_new(kiki_recieve_callback, self);
      if(self->kiki == NULL) return main_destroy(self); 

      self->generator = rhythm_new(RHYTHM_SUBDIVISIONS_PER_BEAT, 3, metronome_callback, self, strike_callback, self, KIKI_DELAY_TIME_USEC);
      if(self->generator == NULL) return main_destroy(self);

      if(argc > 1)
        {
          self->audio_input = (Audio*) micSimulatorNew(argv[1]);
          if(self->audio_input == NULL) fprintf(stderr, "unable to open %s, using mic instead\r\n", argv[1]);
        }   
      if(self->audio_input == NULL)
        {
          self->audio_input = (Audio*) micNew(NUM_INPUT_AUDIO_CHANNELS);
          if(self->audio_input == NULL) return main_destroy(self);
        }
      if(self->audio_input == NULL) return main_destroy(self);
      
      //human's classifier
      classifier = micGetClassifier((Microphone*)self->audio_input, HUMAN_AUDIO_CHANNEL);
      if(classifier == NULL) return main_destroy(self);
      classifier_set_onset_callback(classifier, human_onset_detected_callback, self);
      classifier_set_onset_threshold(classifier, 0.5);      
      
      //robot's classifier
      classifier = micGetClassifier((Microphone*)self->audio_input, ROBOT_AUDIO_CHANNEL);
      if(classifier != NULL)
        {
          classifier_set_onset_callback(classifier, robot_onset_detected_callback, self);
          classifier_set_onset_threshold(classifier, 0.5);
        }

      auPlay(self->audio_input);
    }
  return self;
}

/*----------------------------------------------------------*/
int main(int argc, char* argv[])
{
  iHateCanonicalInputProcessingIReallyReallyDo();
  wiSetup(INPUT_PORT);
  Main* m = main_new(argc, argv);
  
  if(m == NULL) {fprintf(stderr, "unable to make a main object... aborting\r\n"); return -1;}

  const char* main_help =
    "main menu input options:\r\n\ta - tune the human's stroke classifier\r\n\tb - tune the robot's stroke classifier\r\n\t"
    "g - start generating rhythms\r\n\ts - run simulated annealing\r\n\th - print this help\r\n\tq - save and quit\r\n";
  fprintf(stderr, "%s", main_help);
        
  for(;;)
    {
      char c = getchar();
      switch(c)
        {
          case 'a':
            stroke_classifier_tuner_subroutine(micGetClassifier((Microphone*)m->audio_input, HUMAN_AUDIO_CHANNEL), "HUMAN'S");
            fprintf(stderr, "%s", main_help);
            break;
          case 'b':
            stroke_classifier_tuner_subroutine(micGetClassifier((Microphone*)m->audio_input, ROBOT_AUDIO_CHANNEL), "ROBOT'S");
            fprintf(stderr, "%s", main_help);
            break;
          case 'g':
            m->should_generate_rhythm = 1;
            rhythm_generator_subroutine(m->generator);
            m->should_generate_rhythm = 0;
            fprintf(stderr, "%s", main_help);
            break;
          case 's':
            simulated_annealing_subroutine(m);
            fprintf(stderr, "%s", main_help);
            break;
          case 'h':
            fprintf(stderr, "%s", main_help);
            break;          
          case 'q':
            goto out;
            
          default: break;
        }
    }

 out:
  main_destroy(m);
  makeStdinCannonicalAgain();
  return 0;
}

/*----------------------------------------------------------*/
void rhythm_generator_subroutine(RhythmGenerator* rhythm)
{
  const char* rhythm_generator_help = 
    "input options:\r\n\tL - increment learning rate\r\n\tl - decrement learning rate\r\n\t"
    "g - cycle through rhythm algorithms\r\n\tf - forget training\r\n\t0 - train zeros\r\n\t\r\n\t"
    "1 - train Keita one at a time\r\n\t2 - train Keita as corpus\r\n\t3 - train yankadi-makru-signal\r\n\t"
    "4 - train rhythmic density\r\n\t5 - train simple / compound meter\r\n\t6 - train syncopicity\r\n\t\r\n\t"
    "h - print this help\r\n\tq - return to main menu\r\n";

  fprintf(stderr, "\r\nGENERATING RHYTHMS USING LEARNING ALGORITHM\r\n");
  fprintf(stderr, "%s", rhythm_generator_help);
  
  for(;;)
    {
      char c = getchar();
      switch(c)
        {
          case '0':
            rhythm_train_zeros(rhythm, 0.05);
            fprintf(stderr, "done training zeros\r\n");
            break;
          case '1':
            rhythm_train_keita_one_at_a_time(rhythm, 0.05);
            fprintf(stderr, "done training %i rhythms from keita, one at a time\r\n", rhythm_library_num_unique_parts);
            break;
          case '2':
            rhythm_train_keita_all_at_once(rhythm, 0.05);
            fprintf(stderr, "done training %i rhythms from keita all at once\r\n", rhythm_library_num_unique_parts);
            break;
          case '3':
            rhythm_train_signal(rhythm, 0.6);
            fprintf(stderr, "done training yankadi and macru\r\n");
            break;
          case '4':
            rhythm_train_density(rhythm);
            fprintf(stderr, "done training density\r\n");
            break;
          case '5':
            rhythm_train_meter(rhythm);
            fprintf(stderr, "done training meter\r\n");
            break;
          case '6':
            rhythm_train_syncopation(rhythm);
            fprintf(stderr, "done training syncopation\r\n");
            break;
          case 'f':
            rhythm_forget_everything(rhythm);
            fprintf(stderr, "all has been lost and forgotten\r\n");
            break;
          case 'L':
            rhythm_set_learning_rate(rhythm, rhythm_get_learning_rate(rhythm)+0.01);
            fprintf(stderr, "learning rate = %lf\r\n", (double)rhythm_get_learning_rate(rhythm));
            break;
          case 'l':
            rhythm_set_learning_rate(rhythm, rhythm_get_learning_rate(rhythm)-0.01);
            fprintf(stderr, "learning rate = %lf\r\n", (double)rhythm_get_learning_rate(rhythm));
            break;
          case 'g':{
            rhythm_algorithm_t alg = (rhythm_get_algorithm(rhythm)+1) % RHYTHM_NUM_ALGORITHMS;
            rhythm_set_algorithm(rhythm, alg);
            switch(alg)
              {
                case RHYTHM_IMITATE_STROKE_ALGORITHM: fprintf(stderr, "using stroke-immitation algorithm\r\n"); break;
                case RHYTHM_DELAY_ALGORITHM         : fprintf(stderr, "using delay algorithm\r\n")            ; break;
                case RHYTHM_LEARN_ALGORITHM         : fprintf(stderr, "using learning algorithm\r\n")         ; break;
                default: break;
              }
            break;
            }
          case 'h':
            fprintf(stderr, "%s", rhythm_generator_help);
            break;
          case 'q':
            goto out;

          default: break;
        }
    }

 out:
  fprintf(stderr, "DONE GENERATING RHYTHMS\r\n\n");
  return;
}

/*----------------------------------------------------------*/
void stroke_classifier_tuner_subroutine(StrokeClassifier* classifier, const char* classifier_name)
{
  const char* classifier_tuner_help =
    "input options:\r\n\t0~9 - start training that category\r\n\tSHIFT + 0~9 - toggle feature mask for that feature\r\n\t"
    "C - delete all training examples\r\n\tc - delete last training example\r\n\tk - decrement k\r\n\t"
    "K - increment k\r\n\tp - print normalized features\r\n\tt - decrement onset threshold\r\n\t"
    "T - increment onset threshold\r\n\tv - cross-validate training examples\r\n\t"
    "h - print this help\r\n\tq - save or return to main menu\r\n";
    
  if(classifier == NULL)
    {
      fprintf(stderr, "\r\nUNABLE TO TUNE THE %s STROKE CLASSIFIER\r\n\n", classifier_name);
      return;
    }
  else
    fprintf(stderr, "\r\nTUNING THE %s STROKE CLASSIFIER\r\n%s", classifier_name, classifier_tuner_help);
  
  for(;;)
    {
      char c = getchar();
      switch(c)
        {
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':
            classifier_enter_training_mode (classifier, 1, c - '0');
            fprintf(stderr, "training category %c\r\n", c);
            break;
          case '!':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_CENTROID, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_CENTROID));
            fprintf(stderr, "feature mask for CLASSIFIER_SPECTRAL_CENTROID is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_CENTROID));
            break;
          case '@':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SPREAD, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SPREAD));
            fprintf(stderr, "feature mask for CLASSIFIER_SPECTRAL_SPREAD is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SPREAD));
            break;
          case '#':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SKEWNESS, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SKEWNESS));
            fprintf(stderr, "feature mask for CLASSIFIER_SPECTRAL_SKEWNESS is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SKEWNESS));
            break;
          case '$':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_KURTOSIS, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_KURTOSIS));
            fprintf(stderr, "feature mask for CLASSIFIER_SPECTRAL_KURTOSIS is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_KURTOSIS));
            break;
          case '%':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_RMS_AMPLITUDE, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_RMS_AMPLITUDE));
            fprintf(stderr, "feature mask for CLASSIFIER_RMS_AMPLITUDE is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_RMS_AMPLITUDE));
            break;
          case '^':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_ZERO_CROSSING_RATE, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_ZERO_CROSSING_RATE));
            fprintf(stderr, "feature mask for CLASSIFIER_ZERO_CROSSING_RATE is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_ZERO_CROSSING_RATE));
            break;
          case 'C':
            classifier_delete_all_training_examples(classifier);
            fprintf(stderr, "all examples deleted\r\n");
            break;
          case 'c':
            classifier_delete_last_training_example(classifier);
            fprintf(stderr, "last example deleted\r\n");
            break;
          case 'h':
            fprintf(stderr, "Tuning the %s stroke classifier\r\n%s", classifier_name, classifier_tuner_help);
            break;
          case 'K':
            classifier_set_k(classifier, classifier_get_k(classifier)+1);
            fprintf(stderr, "classifier k = %i\r\n", (int)classifier_get_k(classifier));
            break;
          case 'k':
            classifier_set_k(classifier, classifier_get_k(classifier)-1);
            fprintf(stderr, "classifier k = %i\r\n", (int)classifier_get_k(classifier));
            break;
          case 'T':
            classifier_set_onset_threshold(classifier, classifier_get_onset_threshold(classifier)+0.05);
            fprintf(stderr, "onset threshold = %lf\r\n", (double)classifier_get_onset_threshold(classifier));
            break;
          case 't':
            classifier_set_onset_threshold(classifier, classifier_get_onset_threshold(classifier)-0.05);
            fprintf(stderr, "onset threshold  = %lf\r\n", (double)classifier_get_onset_threshold(classifier));
            break;
          case 'p':
            classifier_print_normalized_examples(classifier);
            break;
          case 'v':{
            unsigned n; double accuracy;
            accuracy = classifier_validate_training_examples(classifier, &n);
            fprintf(stderr, "accuracy = %lf%% across %u samples\r\n", accuracy, n);
            break;
            }
          case 'q':
            if(classifier_get_is_training(classifier))
              {
                classifier_enter_training_mode (classifier, 0, -1);
                fprintf(stderr, "training examples saved\r\n");
              }
            else goto out;
            
          default: break;
        }
    }

 out:
  fprintf(stderr, "DONE TUNING THE %s STROKE CLASSIFIER\r\n\n", classifier_name);
  return;
}

/*----------------------------------------------------------*/
void simulated_annealing_subroutine(Main* self)
{
  char c;
  pthread_t t;
  int error;
  self->annealing_arm = 0;
  self->annealing_stroke = ARM_LOCATION_BASS;

  fprintf(stderr, "\r\nLEARNING STROKES WITH SIMULATED ANNEALING\r\n");
  
  fprintf(stderr, "STEP 0: USE THE KIKI APP TO MAKE SURE THE 'BASS' STROKE MAKES SOUND WHEN EXECUTED\r\n");
  
  fprintf(stderr, "STEP 1: SELECT AN ARM TO TUNE (0 or 1)\r\n");
  if(getchar() == '1') self->annealing_arm = 1;

  fprintf(stderr, "STEP 1: SELECT A STROKE TO TUNE (0 (bass) 1 (tone) or 2 (slap))\r\n");
  c = getchar();
  if(c == '1') self->annealing_stroke = ARM_LOCATION_TONE;
  else if(c == '2') self->annealing_stroke = ARM_LOCATION_SLAP;
  
  fprintf(stderr, "STEP 2: USE THE STROKE CLASSIFIER TO TRAIN MANY ARBITRARY STROKES IN ANY CATEGORY SO THE ROBOT CAN LEARN ITS TIMBRE SPACE\r\n");
  stroke_classifier_tuner_subroutine(micGetClassifier((Microphone*)self->audio_input, ROBOT_AUDIO_CHANNEL), "ROBOT'S");
  
  fprintf(stderr, "STEP 3: PLAY AN IDEAL STROKE THAT YOU WANT THE ROBOT TO LEARN. PRESS ANY KEY WHEN DONE\r\n");
  self->listening_for_annealing_target = YES;
  getchar();
  self->listening_for_annealing_target = NO;
  
  double* f = self->annealing_target.normalized_features;
  fprintf(stderr, "target: %lf %lf %lf %lf %lf %lf\r\n", f[0], f[1], f[2], f[3], f[4], f[5]);
  
  fprintf(stderr, "STEP 3: WAIT OR PRESS ANY KEY TO TERMINATE \r\n");
  error = pthread_create(&t, NULL, simulated_annealing_thread, self);
  if(error) {fprintf(stderr, "unable to create a new thread for annealing\r\n"); return;}
  
  getchar();
  pthread_cancel(t);
  
  pthread_join(t, NULL);
  
  //kiki_send_message(self->kiki, kiki_cmd_arm_set_predefined_location, self->annealing_arm, (int)self->best_annealed_features[0], (int)self->best_annealed_features[1], (int)self->best_annealed_features[2], self->annealing_stroke);
  //do some stuff here

  //kiki_send_message       (self->kiki, kiki_cmd_arm_strike, self->best_annealed_features[0], (int)round(self->best_annealed_features[0]), (int)round(self->best_annealed_features[1]), (int)round(self->best_annealed_features[2]), (int)round(self->best_annealed_features[3]));
  //kiki_send_message       (self->kiki, kiki_cmd_arm_set_predefined_location, self->best_annealed_features[0], (int)round(self->best_annealed_features[0]), (int)round(self->best_annealed_features[1]), (int)round(self->best_annealed_features[2]), self->annealing_stroke);
  
}

/*----------------------------------------------------------*/
void* simulated_annealing_thread(void* SELF)
{
  Main* self = (Main*)SELF;
  Annealing* sa = NULL;
  
  int timeout; //millisecs
  int success;
  
  self->awaiting_kikis_response = YES;
  kiki_send_message(self->kiki, kiki_cmd_arm_get_predefined_location, self->annealing_arm, ARM_LOCATION_TONE);
  
  timeout = 100; 
  while((self->awaiting_kikis_response) && (timeout-- > 0)) usleep(1000);
  if(timeout <= 0) {fprintf(stderr, "kiki is not responding -- aborting annealing\r\n"); return NULL;}
  
  self->annealing_features[ARM_SPEED_INDEX] = 0.85;
  sa = sa_new(NUM_ANNEALING_FEATURES);
  if(sa == NULL){fprintf(stderr, "cannot make annealing object -- aborting annealing\r\n"); return NULL;}
  
  sa_set_alpha(sa, 0.99);
  success = sa_init_features (sa, self->annealing_features, self, annealing_cost_callback);
  if(!success) {fprintf(stderr, "cannot init simulated annealing features -- make sure bass stroke is valid\r\n"); goto out;}
  //usleep(800000);
  
  sa_simulate_annealing     (sa, self, annealing_cost_callback, annealing_make_neighbour_callback, 
                                        50,                 //max_iterations, 
                                         1, //0.0,                 //min_cost,
                                        -1,                        //min_temp
                                        NUM_ANNEALING_REJECTIONS_BEFORE_RESET,
                                       self->best_annealed_features
                                       );


  fprintf(stderr, "best features: %lf %lf %lf %lf\r\n", self->best_annealed_features[0], self->best_annealed_features[1], self->best_annealed_features[2], self->best_annealed_features[3]);
  fprintf(stderr, "best distance: %lf\r\n", sa_get_best_cost(sa));
  
 out:
  sa = sa_destroy(sa);
  return NULL;
}

/*----------------------------------------------------------*/
void annealing_make_neighbour_callback(void* SELF, double* features, double T)
{
  //Main* self = (Main*)SELF;
  int index = random() % NUM_ANNEALING_FEATURES;
  
  fprintf(stderr, "T: %lf \r\n", T);
  
  //T = 1;
  T = sqrt(T);
  
  //index = ARM_Y_INDEX;
  
  switch(index)
    {
      case ARM_X_INDEX:
        /* plus or minus 50 mm */
        features[index] += 50 * (2 * (random() / (double)RAND_MAX) - 1) * T;
        //fprintf(stderr, "X %lf\r\n", features[index]);
        break;
      case ARM_Y_INDEX:
        /* plus or minus 10 mm */
        features[index] += 10 * (2 * (random() / (double)RAND_MAX) - 1) * T;
        //fprintf(stderr, "Y %lf\r\n", features[index]);
        break;
      case ARM_A_INDEX:
        /* plus or minus 50 milliradians */
        features[index] += 50 * (2 * (random() / (double)RAND_MAX) - 1) * T;
        //fprintf(stderr, "A %lf\r\n", features[index]);
        break;
      case ARM_SPEED_INDEX:
        /* plus or minus 0.5 units */
        features[index] += 0.5 * (2 * (random() / (double)RAND_MAX) - 1) * T;
        if(features[index] > 1.0) features[index] = 1.0;
        if(features[index] < 0.0) features[index] = 0.0;
        //fprintf(stderr, "SPEED: %lf\r\n", features[index]);
        break;      
      default: break;
    }
}

/*----------------------------------------------------------*/
double annealing_cost_callback(void* SELF, double features[])
{
  Main* self = (Main*)SELF;
  int timeout; 
  double dist = -1;
  
  kiki_send_message(self->kiki, kiki_cmd_arm_strike, (int)self->annealing_arm,   (int)features[ARM_X_INDEX], 
                                         (int)features[ARM_Y_INDEX], (int)features[ARM_A_INDEX], 
                                         (float)features[ARM_SPEED_INDEX]);
  self->awaiting_annealing_stroke = YES;
  timeout = KIKI_DELAY_TIME_USEC * 1.5 / 1000.0; 
  while((self->awaiting_annealing_stroke) && (timeout-- > 0)) usleep(1000);
  
  if (timeout > 0) 
    dist = classifier_get_euclidian_distance(micGetClassifier((Microphone*)self->audio_input, ROBOT_AUDIO_CHANNEL), &self->annealing_target, &self->annealing_candidate);
    
  double* f = self->annealing_candidate.normalized_features;
  fprintf(stderr, "control: %i %i %i %f\r\n", (int)features[ARM_X_INDEX], (int)features[ARM_Y_INDEX], (int)features[ARM_A_INDEX], (float)features[ARM_SPEED_INDEX]);
  fprintf(stderr, "spectral: %lf %lf %lf %lf %lf %lf dist:%lf\r\n", f[0], f[1], f[2], f[3], f[4], f[5], dist);
  return dist;
}

/*----------------------------------------------------------*/
//this is called by the human's stroke classifier when the human strikes its drum
void human_onset_detected_callback(void* SELF, ClassifierExample* e, timestamp_microsecs_t onset_timestamp)
{
  Main* self = (Main*) SELF;
  float loudness = scalef(e->raw_features[CLASSIFIER_RMS_AMPLITUDE], 0.0, 0.8, 0.0, 1.0);
  CONSTRAIN(loudness, 0.0, 1.0);

  if(self->should_generate_rhythm)
    rhythm_strike(self->generator, onset_timestamp, e->category, loudness);
  else
    fprintf(stderr, "category: %i\t%f\r\n", (int)e->category, loudness);
}

/*----------------------------------------------------------*/
//this is called by the robots's stroke classifier when the robot strikes its drum (during annealing only)
void robot_onset_detected_callback(void* SELF, ClassifierExample* e, timestamp_microsecs_t onset_timestamp)
{  
  Main* self = (Main*) SELF;
  
  //fprintf(stderr, "robo ouch!\r\n");
  
  if(!self->awaiting_annealing_stroke)
    {
      double* f = e->normalized_features;
      fprintf(stderr, "%lf %lf %lf %lf %lf %lf\r\n", f[0], f[1], f[2], f[3], f[4], f[5]);
    }
    
  if(self->listening_for_annealing_target)
    classifier_example_copy_features(e, &self->annealing_target);
    
  else if(self->awaiting_annealing_stroke)
    {
      classifier_example_copy_features(e, &self->annealing_candidate);
      self->awaiting_annealing_stroke = NO;
    }
}

/*----------------------------------------------------------*/
//this is called by the rhythm generator to schedule a metronome click
void metronome_callback(void* SELF)
{
  Main* self = (Main*) SELF;
  kiki_send_message(self->kiki, kiki_cmd_thwack, 2, 1.0);
  
  const int buffer_size = 100;
  static char buffer[buffer_size];
  int num_bytes = oscConstruct(buffer, buffer_size, "/metro", "");
  wiWrite(buffer, buffer_size, "127.0.0.1", OUTPUT_PORT);
}

/*----------------------------------------------------------*/
//this is called by the rhythm generator to schedule kiki to play a note
void strike_callback(void* SELF, int stroke_category, double loudness)
{
  Main* self = (Main*) SELF;
  timestamp_microsecs_t now = timestamp_get_current_time();
  timestamp_microsecs_t duration = now - self->prev_arm_time;

  loudness = 0.3; //REMOVE LATER!!!!!!!!!!!!!

  if((duration < ARM_FASTEST_REPETITION_RATE) || ((duration < ARM_FASTEST_PLAYING_RATE) && (stroke_category != self->prev_arm_category)))
    {
      kiki_send_message(self->kiki, kiki_cmd_thwack, self->prev_solenoid, /*0.5*/ loudness);
      self->prev_solenoid++; self->prev_solenoid %= 2;
      //fprintf(stderr, "thwack: %lf\r\n", loudness);
    }
  else
    {
      uint8_t midi_message[3] = {MIDI_STATUS_NOTE_ON | MIDI_CHANNEL_DRUMS, 62, loudness * 127};
      switch(stroke_category)
        {
          case 0:  midi_message[1] = 60; break;
          case 2:  midi_message[1] = 64; break;
          default: break;
        }
      kiki_send_raw_midi(self->kiki, midi_message, 3);
      self->prev_arm_time = now;
      self->prev_arm_category = stroke_category;
      //fprintf(stderr, "strike: %i %lf\r\n", stroke_category, loudness);
    }

  //fprintf(stderr, "strike: %i %lf\r\n", stroke_category, loudness);
  const int buffer_size = 100;
  static char buffer[buffer_size];
  int num_bytes = oscConstruct(buffer, buffer_size, "/strike", "if", stroke_category, 0.5/*loudness*/);
  wiWrite(buffer, buffer_size, "127.0.0.1", OUTPUT_PORT);
}

/*----------------------------------------------------------*/
//this is called by kiki whenever she sends a message to this computer
void  kiki_recieve_callback(void* SELF, char* message, kiki_arg_t args[], int num_args)
{
  Main* self = (Main*)SELF;
  
  kiki_debug_print_message(message, args, num_args);

  if(!self->awaiting_kikis_response) return;
 
  switch(kiki_hash_message(message))
    {
      case kiki_hash_arm_predefined_location:
        if(num_args == 5)
          {
            //could check arm and loc;
            self->annealing_features[ARM_X_INDEX] = kiki_arg_to_float(&args[1]);
            self->annealing_features[ARM_Y_INDEX] = kiki_arg_to_float(&args[2]);
            self->annealing_features[ARM_A_INDEX] = kiki_arg_to_float(&args[3]);
          }
    }

  self->awaiting_kikis_response = NO;
}

/*---------------------------------------------------------------*/
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
struct termios oldTerminalAttributes;

void iHateCanonicalInputProcessingIReallyReallyDo(void)
{
	int error;
	struct termios newTerminalAttributes;
  
	int fd = fcntl(STDIN_FILENO,  F_DUPFD, 0);
  
	error = tcgetattr(fd, &(oldTerminalAttributes));
	if(error == -1) {  fprintf(stderr, "Error getting serial terminal attributes\r\n"); return;}
	
	newTerminalAttributes = oldTerminalAttributes; 
	
	cfmakeraw(&newTerminalAttributes);
	
	error = tcsetattr(fd, TCSANOW, &newTerminalAttributes);
	if(error == -1) {  fprintf(stderr,  "Error setting serial attributes\r\n"); return; }
}

/*------------------------------------------------------------------------------------*/
void makeStdinCannonicalAgain()
{
  int fd = fcntl(STDIN_FILENO,  F_DUPFD, 0);
  
	if (tcsetattr(fd, TCSANOW, &oldTerminalAttributes) == -1)
	  fprintf(stderr,  "Error setting serial attributes\r\n");
}
