//
//  MKPopUpButton.h
//  Kiki
//
//  Created by Michael Krzyzaniak on 7/29/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MKPopUpButton : NSPopUpButton
-(void)setFloatValue:(float)val;
-(void)setIntValue:(int)val;
-(IBAction)takeFloatValueFrom:(id)sender;
-(IBAction)takeIntValueFrom:(id)sender;
@end
