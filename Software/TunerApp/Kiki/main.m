//
//  main.m
//  Kiki
//
//  Created by Michael Krzyzaniak on 7/22/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
  return NSApplicationMain(argc, (const char **)argv);
}
