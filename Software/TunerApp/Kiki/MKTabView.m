//
//  MKTabView.m
//  Kiki
//
//  Created by Michael Krzyzaniak on 8/6/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import "MKTabView.h"

@implementation MKTabView


- (void)drawRect:(NSRect)dirtyRect
{
  [[NSColor whiteColor] setFill];
  [[NSBezierPath bezierPathWithRect: dirtyRect] fill];
  //[super drawRect:dirtyRect];
}

@end
