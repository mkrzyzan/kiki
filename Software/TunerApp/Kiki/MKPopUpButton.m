//
//  MKPopUpButton.m
//  Kiki
//
//  Created by Michael Krzyzaniak on 7/29/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import "MKPopUpButton.h"

@implementation MKPopUpButton

-(void)setFloatValue:(float)val
{
  [self selectItemAtIndex: (NSUInteger)val];
}
-(void)setIntValue:(int)val
{
  [self selectItemAtIndex: (NSUInteger)val];
}
-(IBAction)takeFloatValueFrom:(id)sender
{
  [self selectItemAtIndex: (NSUInteger)[sender floatValue]];
}
-(IBAction)takeIntValueFrom:(id)sender
{
  [self selectItemAtIndex: (NSUInteger)[sender intValue]];
}

@end
