//
//  MKTabView.h
//  Kiki
//
//  Created by Michael Krzyzaniak on 8/6/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MKTabView : NSTabView

@end
