//
//  MKArmView.m
//  Djembe_Kinematics_Solver
//
//  Created by Michael Krzyzaniak on 3/7/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import "MKArmView.h"

#define ARM_MIN_X  480
#define ARM_MAX_X  780
#define ARM_MIN_Y  -50
#define ARM_MAX_Y  199
#define ARM_MIN_A -600
#define ARM_MAX_A  600

/*----------------------------------------------*/
@implementation MKArmView

/*----------------------------------------------*/

#define ARM_HAND_LENGTH                      10
#define ARM_HAND_BASE_TO_PALM_LENGTH         10
#define ARM_PALM_TO_FINGERTIPS_LENGTH (\
        ARM_HAND_LENGTH -\
        ARM_HAND_BASE_TO_PALM_LENGTH)
#define ARM_SEGMENT_1_LENGTH                 197
#define ARM_SEGMENT_2_LENGTH                 197
#define ARM_SEGMENT_3_LENGTH_TO_HAND_BASE    318
#define ARM_SEGMENT_3_LENGTH_TO_PALM (\
        ARM_SEGMENT_3_LENGTH_TO_HAND_BASE +\
        ARM_HAND_BASE_TO_PALM_LENGTH)
#define ARM_SEGMENT_3_LENGTH_TO_FINGERTIPS (\
        ARM_SEGMENT_3_LENGTH_TO_HAND_BASE +\
        ARM_HAND_BASE_TO_PALM_LENGTH)
#define ARM_LENGTH (ARM_SEGMENT_1_LENGTH + ARM_SEGMENT_2_LENGTH + ARM_SEGMENT_3_LENGTH_TO_PALM)
        

/*----------------------------------------------*/
- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self != NULL) 
      {
        x = 600;
        y = 0;
        a = 0;
        table_top_position = 1/6.0;
        
        prev_x=x; prev_y=y; prev_a=a;
      }
    
    return self;
}

/*----------------------------------------------*/
-(BOOL)acceptsFirstResponder
{
  return YES;
}

/*----------------------------------------------*/
- (void)drawRect:(NSRect)rect
{
  //fix me!!!
  //recursive error handling could be bad if previous hand location was also incorrect
   
  //color background
  //[[NSColor colorWithCalibratedHue: 0.0 saturation: 0 brightness: 0.61 alpha: 1] setFill];
  //[[NSColor lightGrayColor] setFill];
  //[NSBezierPath fillRect: rect];
  
  
  float p1x , p2x , p3x ;             //servo positions
  float p1y , p2y , p3y , p4y;        //servo positions
  float a0  , a1  , a2  , a3  , a4  ; //angles (theta)
  float l0  , l1  , l2  , l3  , l4  ; //segment lengths
  float l0_2, l1_2, l2_2, l3_2, l4_2; //lengths squared
  float H;
  float lambda1 = 1, lambda2 = 1;
  
  float _x, _y;
  
  l0     = ARM_SEGMENT_1_LENGTH;
  l1     = ARM_SEGMENT_2_LENGTH;
  l2     = ARM_SEGMENT_3_LENGTH_TO_PALM;
  l0_2   = l0 * l0;
  l1_2   = l1 * l1;
  l2_2   = l2 * l2;
  p2x    = x - l2 * cos(a);
  p2y    = y + l2 * sin(a);
  l3_2   = (p2x*p2x) + (p2y*p2y);
  l3     = sqrt(l3_2); 
  if     (l3 > l0 + l1) {x=prev_x; y=prev_y; a=prev_a; [self drawRect:rect]; return;} //return /*FAILURE*/;
  if(l3 != 0) lambda1 = 0.5 + (l0_2 - l1_2) / (2 * l3_2);
  p3x    = p2x * lambda1;
  p3y    = p2y * lambda1;
  //a3     = acos( ((l1_2) + (l3_2) - (l0_2)) / (2 * l1 * l3) );
  //H      = l1 * sin(a3);
  a3     = ((l1_2) + (l3_2) - (l0_2)) / (2 * l1 * l3);
  H      = l1 * sqrt(1 - (a3*a3));
  //a4     = acos(p2x / l3);
  //p1y    = p3y + H * cos(a4);
  a4     = p2x / l3;
  p1y    = p3y + H * a4;
  if(p2y >= 0) H = -H;
  //p1x    = p3x + H * sin(a4);
  p1x    = p3x + H * sqrt(1-(a4*a4));
  _x     = x - p1x;
  _y     = y - p1y;
  l4     = sqrt(_x*_x + _y*_y);
  l4_2   = l4 * l4;
  a0     = asin(p1y / l0) + M_PI;
  a1     = acos( ((l0_2) + (l1_2) - (l3_2)) / (2 * l0 * l1) );
  a2     = (2 * M_PI) - acos( ((l1_2) + (l2_2) - (l4_2)) / (2 * l1 * l2) );
  if     (isnan(a0) || isnan(a1) || isnan(a2)) {x=prev_x; y=prev_y; a=prev_a; [self drawRect:rect]; return;}//return /*FAILURE*/;
  if(l4 != 0) lambda2 = 0.5 + (l1_2 - l2_2) / (2 * l4_2);
  p4y    = p1y + _y * lambda2;
  
  if(x > p1x)
    if(p2y > p4y)
      a2 = (M_PI * 2) - a2;
  
  if(x < p1x)
    if(p2y < p4y)
      a2 = (M_PI * 2) - a2;
  
  if(p1x < 0)
    a0 = M_PI * 3 - a0;

  a0 += stroke_distance;
  a1 += stroke_distance;
  a2 += stroke_distance;
  
  prev_x=x; prev_y=y; prev_a=a;


//------------------------------------------------------
  float angles[3] = {a0, a1, a2};
  //float angles[3] = {M_PI, M_PI, M_PI};
  //[self getServoAngles: angles];
  
  float view_midline = [self bounds].size.width * 0.5;
  float height       = [self bounds].size.height;
  float table_location = height * table_top_position;
  //float table_location = height * 1 / 2;
  height -= table_location;
  float scaling      = height / (double)ARM_LENGTH;
  NSRect oval = {0, 0 , 20, 20};
  
  NSPoint point;
  NSBezierPath* path = [NSBezierPath bezierPath];
  
  //draw table top
  /*
  point.x = 0;
  point.y = height;
  [path moveToPoint: point];
  point.x = view_midline * 2;
  [path lineToPoint: point];
  [path stroke];
  [path removeAllPoints];
  */
  [path setLineJoinStyle: NSRoundLineJoinStyle];
  [path setLineCapStyle: NSRoundLineCapStyle];
  [path setLineWidth:4];
  
  point.x = view_midline;
  point.y = height;
  [path moveToPoint: point];
  [self drawNumber: angles[0] atPoint: point];
  
  point.x = point.x - (ARM_SEGMENT_1_LENGTH * sin(angles[0]) * scaling);
  point.y = point.y + (ARM_SEGMENT_1_LENGTH * cos(angles[0]) * scaling);
  [path lineToPoint: point];
  [self drawNumber: angles[1] atPoint: point];

  angles[1] += angles[0] - M_PI;
  point.x = point.x - (ARM_SEGMENT_2_LENGTH * sin(angles[1]) * scaling);
  point.y = point.y + (ARM_SEGMENT_2_LENGTH * cos(angles[1]) * scaling);
  [path lineToPoint: point];
  [self drawNumber: angles[2] atPoint: point];
  
  angles[2] += angles[1] - M_PI;
  point.x = point.x - (ARM_SEGMENT_3_LENGTH_TO_PALM * sin(angles[2]) * scaling);
  point.y = point.y + (ARM_SEGMENT_3_LENGTH_TO_PALM * cos(angles[2]) * scaling);
  [path lineToPoint: point];
  
  NSColor* black = [NSColor blackColor];
  NSColor* red = [NSColor colorWithCalibratedHue: 0 saturation: 1 brightness: 0.5 alpha: 1];
  [black setStroke];
  //[red setFill];
  [path stroke];
  
  oval.origin.x = point.x - 10;
  oval.origin.y = point.y - 10;
  [black setFill];
  [[NSBezierPath bezierPathWithOvalInRect: oval] fill];
  
  
  //------------------------------------------------------
  oval.size.height = 10;
  oval.size.width = 10;
  oval.origin.x = view_midline + (p2y * scaling) - 5;
  oval.origin.y = height - (p2x * scaling) - 5;
  [red setFill];
  [[NSBezierPath bezierPathWithOvalInRect: oval] fill];
  oval.origin.x += 10;
  oval.origin.y += 10;
  handle_x = oval.origin.x;
  handle_y = oval.origin.y;
}

/*----------------------------------------------*/
-(void) drawNumber: (float) number atPoint: (NSPoint) point
{
  point.x += 10;
  NSString* string = [NSString stringWithFormat: @"%f", number];
  NSAttributedString* attributed = [[NSAttributedString alloc] initWithString: string];
  [attributed drawAtPoint: point];
  [attributed release];
}

/*----------------------------------------------*/
-(void) setX: (float)val restrict_range:(bool)restrictions
{
  if(restrictions)
    {
      if(val<ARM_MIN_X) val = ARM_MIN_X;
      if(val>ARM_MAX_X) val = ARM_MAX_X;
    }
    
  x = val;
  [self setNeedsDisplay: YES];
}

/*----------------------------------------------*/
-(void) setY: (float)val restrict_range:(bool)restrictions
{
  if(restrictions)
    {
      if(val<ARM_MIN_Y) val = ARM_MIN_Y;
      if(val>ARM_MAX_Y) val = ARM_MAX_Y;
    }
    
  y = val;
  [self setNeedsDisplay: YES];
}

/*----------------------------------------------*/
-(void) setA: (float)val restrict_range:(bool)restrictions
{
  if(restrictions)
    {
      if(val<ARM_MIN_A) val = ARM_MIN_A;
      if(val>ARM_MAX_A) val = ARM_MAX_A;
    }
    
  a = val*0.001;
  [self setNeedsDisplay: YES];
}

/*----------------------------------------------*/
-(IBAction) setX: (id) sender
{
  [self setX: [sender floatValue] restrict_range:YES];
}

/*----------------------------------------------*/
-(IBAction) setY: (id) sender
{
  [self setY: [sender floatValue] restrict_range:YES];
}

/*----------------------------------------------*/
-(IBAction) setA: (id) sender
{
  [self setA: [sender floatValue] restrict_range:YES];
}

/*----------------------------------------------*/
-(float)    getX
{
  return x;
}

/*----------------------------------------------*/
-(float)    getY
{
  return y;
}

/*----------------------------------------------*/
-(float)    getA
{
  return a*1000;
}

/*----------------------------------------------*/
-(IBAction) setStrokeDistance: (id) sender
{
  stroke_distance = [sender floatValue];
  [self setNeedsDisplay: YES];
}

/*----------------------------------------------*/
-(void)keyDown:(NSEvent *)event
{
  BOOL modifiers = NO;
  if([event modifierFlags] & NSShiftKeyMask)
    modifiers = YES;
  
  switch([event keyCode])
    {
      case 126: //NSUpArrowFunctionKey:
        [self setX:[self getX]-1 restrict_range: YES];
        [self performAction];
        break;
      case 125: //NSDownArrowFunctionKey:
        [self setX:[self getX]+1 restrict_range: YES];
        [self performAction];
        break;
      case 123: //NSLeftArrowFunctionKey:
        (modifiers) ? [self setA:[self getA]-5 restrict_range: YES]:
                      [self setY:[self getY]-1 restrict_range: YES];
        [self performAction];
        break;
        
      case 124: //NSRightArrowFunctionKey:
        (modifiers) ? [self setA:[self getA]+5 restrict_range: YES]:
                      [self setY:[self getY]+1 restrict_range: YES];
        [self performAction];
        break;
        
      default:break;
    }
}

/*----------------------------------------------*/
-(void)mouseDown:(NSEvent *)theEvent
{
  NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
  NSPoint p = {location.x, location.y}; 
  p.x -= handle_x;
  p.y -= handle_y;
  p.x *= p.x;
  p.y *= p.y;
  
  if(sqrt(p.x + p.y) <= 20)
    {
      mouse_has_handle = YES;
      if([NSEvent modifierFlags])
        is_adjusting_angle = YES;
    }
}

/*----------------------------------------------*/
-(void)mouseDragged:(NSEvent *)theEvent
{
  //NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
  float height       = [self bounds].size.height;
  float table_location = height * table_top_position;
  height -= table_location;
  float scaling      = ARM_LENGTH / height;

  
  if(mouse_has_handle)
    { 
      if(is_adjusting_angle)
        {
          NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
          location.x -= [self bounds].size.width / 2.0;
          location.y = [self bounds].size.height - location.y;
          location.y -= table_location;
          location.x *= scaling;
          location.y *= scaling;
          float theta = -atan2([self getY] - location.x, [self getX] - location.y);
          
          [self setA:theta*1000 restrict_range: YES];
        }
      else
        { 
          float x_inc, y_inc;
          y_inc = theEvent.deltaX * scaling;
          x_inc = theEvent.deltaY * scaling;
          [self setX:[self getX]+x_inc restrict_range: YES];
          [self setY:[self getY]+y_inc restrict_range: YES];
        }
      [self performAction];
    }
}

/*----------------------------------------------*/
-(void)mouseUp:(NSEvent *)theEvent
{
  mouse_has_handle = NO;
  is_adjusting_angle = NO;
}

/*----------------------------------------------*/

-(void)     setTarget:(id)target action:(SEL)action
{
  my_target = target;
  my_action = action;
}


-(void)     performAction
{
  if(my_target != nil)
    if([my_target respondsToSelector: my_action])
      [my_target performSelector:my_action withObject:self afterDelay:0];
}


@end
