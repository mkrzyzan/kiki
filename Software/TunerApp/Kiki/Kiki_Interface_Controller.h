//
//  Kiki_Interface_Controller.h
//  Kiki
//
//  Created by Michael Krzyzaniak on 7/24/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "Kiki_Communication.h"
#include "MKArmView.h"

@interface Kiki_Interface_Controller : NSObject
{
  //so they can be styled...
  //not currently used
  IBOutlet NSTabView* tab_view;
  IBOutlet NSWindow* main_window;
  
  IBOutlet id io_text;

  IBOutlet id servo_id;
  IBOutlet id servo_mode;
  IBOutlet id servo_superview;
  
  IBOutlet id mmap_arm;
  IBOutlet id mmap_superview; 
  
  IBOutlet id arm_superview;
  IBOutlet id arm_view;
  IBOutlet id arm_arm;
  IBOutlet id arm_speed; 
  IBOutlet id arm_location;
  BOOL        arm_should_continue_polling_location;
  
  IBOutlet id stroke_arm;
  IBOutlet id stroke_speed;
  IBOutlet id stroke_use_note_on;
  
  
  Kiki* kiki;
}

-(IBAction) kiki_io_clear_text:              (id) sender;

-(IBAction) kiki_servo_get_all_registers:    (id) sender;
-(IBAction) kiki_servo_get_register:         (id) sender;
-(IBAction) kiki_servo_do_prepared:          (id) sender;
-(IBAction) kiki_servo_factory_reset:        (id) sender;
-(IBAction) kiki_servo_ping:                 (id) sender;
-(IBAction) kiki_servo_set_register:         (id) sender; 

-(IBAction) kiki_mmap_get_register:          (id) sender;
-(IBAction) kiki_mmap_get_all_registers:     (id) sender;
-(IBAction) kiki_mmap_set_register:          (id) sender;
-(IBAction) kiki_mmap_factory_reset:         (id) sender;
-(IBAction) kiki_mmap_undo:                  (id) sender;
-(IBAction) kiki_mmap_save:                  (id) sender;

-(IBAction) kiki_arm_get_current_location:   (id) sender;
-(IBAction) kiki_arm_poll_current_location:  (id) sender;
-(IBAction) kiki_arm_set_current_location:   (id) sender;
-(IBAction) kiki_arm_get_predefined_location:(id) sender;
-(IBAction) kiki_arm_set_predefined_location:(id) sender;
-(IBAction) kiki_arm_strike:                 (id) sender;

-(IBAction) kiki_bass:                       (id)sender;
-(IBAction) kiki_tone:                       (id)sender;
-(IBAction) kiki_slap:                       (id)sender;
-(IBAction) kiki_thwack:                     (id)sender;
-(IBAction) kiki_cowbell:                    (id)sender;

@end
