//
//  AppDelegate.m
//  Kiki
//
//  Created by Michael Krzyzaniak on 7/22/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)dealloc
{
    [super dealloc];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
  // Insert code here to initialize your application
}

@end
