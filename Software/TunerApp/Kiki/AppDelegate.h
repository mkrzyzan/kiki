//
//  AppDelegate.h
//  Kiki
//
//  Created by Michael Krzyzaniak on 7/22/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
