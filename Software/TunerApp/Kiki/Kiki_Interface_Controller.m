//
//  Kiki_Interface_Controller.m
//  Kiki
//
//  Created by Michael Krzyzaniak on 7/24/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import "Kiki_Interface_Controller.h"


@interface Kiki_Interface_Controller (private)

-(void) message_recd: (char*)message with_args: (kiki_arg_t*)args num_args: (int)num_args;
-(void) kiki_io_print_string:(NSMutableString*)str;

@end

/*------------------------------------------------------------------------*/
void message_recd(void* self, char* message, kiki_arg_t* args, int num_args)
{
  [(Kiki_Interface_Controller*) self message_recd: message with_args: args num_args: num_args];
}

@implementation Kiki_Interface_Controller

-(id) init
{
  self = [super init];
  if(self != NULL)
    {
      kiki = kiki_new(message_recd, self);
      if(kiki == NULL)
        {
          [super dealloc];
          return NULL;
        }
    }
  return self;
}

-(void) awakeFromNib
{
  int i;
  for(i=0; i<254; i++)
    [servo_id addItemWithTitle:[NSString stringWithFormat: @"%i", i]];
  [servo_id addItemWithTitle:@"broadcast"];
  

  //NSColor* bg_color = [NSColor colorWithCalibratedHue:0.0 saturation:1.0 brightness:0.5/*0.685*/ alpha:1.0];
  //NSColor* bg_color = [NSColor lightGrayColor];
  //[main_window setBackgroundColor: bg_color];
  
  //[io_window    setBackgroundColor: bg_color];
  //[servo_window setBackgroundColor: bg_color];
  //[mmap_window  setBackgroundColor: bg_color];
  //[arm_window   setBackgroundColor: bg_color];

  //can't be done in IB with custom view even though it inherits from NSResponder
  [arm_view  setTarget: self action: @selector(kiki_arm_set_current_location:)];
}

/*------------------------------------------------------------------------*/
-(IBAction) kiki_servo_do_prepared: (id) sender
{
  long s = [servo_id indexOfSelectedItem];
  kiki_send_message(kiki, kiki_cmd_servo_do_prepared, s);
  [self print_sent: kiki_cmd_servo_do_prepared, s];
}

-(IBAction) kiki_servo_factory_reset: (id) sender
{
  long s = [servo_id indexOfSelectedItem];
  kiki_send_message(kiki, kiki_cmd_servo_factory_reset, s);
  [self print_sent: kiki_cmd_servo_factory_reset, s];
}

-(IBAction) kiki_servo_ping: (id) sender;
{
  long s = [servo_id indexOfSelectedItem];
  kiki_send_message(kiki, kiki_cmd_servo_ping, s);
  [self print_sent: kiki_cmd_servo_ping, s];
}

-(IBAction) kiki_servo_get_all_registers: (id) sender
{
  NSArray* regs = [servo_superview subviews];
  for (id reg in regs)
    {
      if([reg action] == @selector(kiki_servo_get_register:))
        {
          [self kiki_servo_get_register: reg]; 
          usleep(10000);
        } 
    }
}

-(IBAction) kiki_servo_get_register: (id) sender
{
  long s   = [servo_id indexOfSelectedItem];
  long reg = [sender tag];
  kiki_send_message(kiki, kiki_cmd_servo_get, s, reg);
  [self print_sent: kiki_cmd_servo_get, s, reg];
}

-(IBAction) kiki_servo_set_register: (id) sender
{
  long s   = [servo_id indexOfSelectedItem];
  long reg = [sender tag];
  float val = [sender floatValue];
  BOOL do_now = ![servo_mode indexOfSelectedItem];
  char* command = (do_now) ? kiki_cmd_servo_set : kiki_cmd_servo_prepare;
  
  if(![[sender stringValue] isEqualTo:@""])
    {
      kiki_send_message(kiki, command, s, reg, val);
      [self print_sent: command, s, reg, val];
      
      if(do_now)
        [self kiki_servo_get_register:sender];
    }
}

/*------------------------------------------------------------------------*/
#define KIKI_MMAP_ARM_OBJECT_SIZE   (MMAP_ADDR_L_ARM_OBJECT - MMAP_ADDR_R_ARM_OBJECT)

-(IBAction) kiki_mmap_get_register: (id) sender
{
  int addr = (int)[sender tag];
  if((addr >= MMAP_ADDR_R_ARM_OBJECT) && (addr < MMAP_ADDR_COWBELL_SOLENOID_OBJECT))
    addr += KIKI_MMAP_ARM_OBJECT_SIZE * [mmap_arm indexOfSelectedItem];
    
  kiki_send_message(kiki, kiki_cmd_mmap_get, addr);
  [self print_sent: kiki_cmd_mmap_get, addr];
}

-(IBAction) kiki_mmap_get_all_registers: (id) sender
{
  NSArray* addrs = [mmap_superview subviews];
  for (id addr in addrs)
    {
      if([addr action] == @selector(kiki_mmap_get_register:))
        [self kiki_mmap_get_register: addr];
    }  
}

-(IBAction) kiki_mmap_set_register: (id) sender
{
  int addr = (int)[sender tag];
  if((addr >= MMAP_ADDR_R_ARM_OBJECT) && (addr < MMAP_ADDR_COWBELL_SOLENOID_OBJECT))
    addr += KIKI_MMAP_ARM_OBJECT_SIZE * [mmap_arm indexOfSelectedItem];
  int val = [sender intValue];
  
  if(![[sender stringValue] isEqualTo:@""])
    {
      kiki_send_message(kiki, kiki_cmd_mmap_set, addr, val);
      [self print_sent: kiki_cmd_mmap_set, addr, val];
      [self kiki_mmap_get_register: sender];
    }
}

-(IBAction) kiki_mmap_factory_reset: (id) sender
{
  kiki_send_message(kiki, kiki_cmd_mmap_factory_reset);
  [self print_sent: kiki_cmd_mmap_factory_reset];
}

-(IBAction) kiki_mmap_undo: (id) sender
{
  kiki_send_message(kiki, kiki_cmd_mmap_undo);
  [self print_sent: kiki_cmd_mmap_undo];
}

-(IBAction) kiki_mmap_save: (id) sender
{
  kiki_send_message(kiki, kiki_cmd_mmap_save);
  [self print_sent: kiki_cmd_mmap_save];
}

/*------------------------------------------------------------------------*/
-(IBAction) kiki_arm_get_current_location:  (id) sender
{
  int   arm   = (int)[arm_arm indexOfSelectedItem];
  kiki_send_message(kiki, kiki_cmd_arm_get_current_location, (int)arm);
  [self print_sent:  kiki_cmd_arm_get_current_location, (int)arm]; 
}

-(IBAction) kiki_arm_poll_current_location:  (id) sender
{
  kiki_send_message(kiki, kiki_cmd_servo_set, 254/*broadcast*/, 24/*TORQUE_ENABLE*/, [sender floatValue]);
  [self print_sent:  kiki_cmd_servo_set, 254/*broadcast*/, 24/*TORQUE_ENABLE*/, [sender floatValue]];
  
  if(![sender intValue])
    {
      arm_should_continue_polling_location = 1;
      [NSThread detachNewThreadSelector:@selector(arm_poll_run_loop:) toTarget:self withObject:nil];
    }
  else
    arm_should_continue_polling_location = 0;
}

-(void)arm_poll_run_loop: (id) sender
{
  NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
  while (arm_should_continue_polling_location)
    {
      [self performSelectorOnMainThread: @selector(kiki_arm_get_current_location:) withObject:nil waitUntilDone:NO];
      //[self kiki_arm_get_current_location: sender];
      [NSThread sleepForTimeInterval: 0.1];
    }
  [pool drain];
}

-(IBAction) kiki_arm_set_current_location:  (id) sender
{
  int   arm   =  (int)[arm_arm indexOfSelectedItem];
  float speed = [arm_speed floatValue];
  int   x     = [arm_view getX];
  int   y     = [arm_view getY];
  int   a     = [arm_view getA];
  kiki_send_message(kiki, kiki_cmd_arm_set_current_location, arm, x, y, a, speed);
  [self print_sent:  kiki_cmd_arm_set_current_location, arm, x, y, a, speed];
  //it wont be there yet...
  //[self arm_get_current_location: sender];
}

-(IBAction) kiki_arm_get_predefined_location:(id) sender
{
  int   arm   =  (int)[arm_arm indexOfSelectedItem];
  int   loc   =  (int)[arm_location indexOfSelectedItem];
  kiki_send_message(kiki, kiki_cmd_arm_get_predefined_location, arm, loc);
  [self print_sent:  kiki_cmd_arm_get_predefined_location, arm, loc];
  //will set the position of the robot's arm when the response arrives;
}

-(IBAction) kiki_arm_set_predefined_location:(id) sender
{
  int   arm   =  (int)[arm_arm indexOfSelectedItem];
  int   x     = [arm_view getX];
  int   y     = [arm_view getY];
  int   a     = [arm_view getA];
  int   loc   =  (int)[arm_location indexOfSelectedItem];
  
  kiki_send_message(kiki, kiki_cmd_arm_set_predefined_location, arm, x, y, a, loc);
  [self  print_sent:kiki_cmd_arm_set_predefined_location, arm, x, y, a, loc];
}

-(IBAction) kiki_arm_strike:                 (id) sender
{
  int   arm   =  (int)[arm_arm indexOfSelectedItem];
  int   x     = [arm_view getX];
  int   y     = [arm_view getY];
  int   a     = [arm_view getA];
  float speed = [arm_speed floatValue];
  
  kiki_send_message(kiki, kiki_cmd_arm_strike, arm, x, y, a, speed);
  [self  print_sent:kiki_cmd_arm_strike, arm, x, y, a, speed];
}

/*------------------------------------------------------------------------*/
-(void) print_sent:(char*)fmt, ...
{
  va_list args;
  va_start(args, fmt);
  NSString* format = [NSString stringWithCString: fmt encoding: NSASCIIStringEncoding];
  NSMutableString* str = [[NSMutableString alloc] initWithFormat:format arguments:args];
  [str appendString: @"\n"];
  [str insertString: @"sent: " atIndex: 0];
  [self kiki_io_print_string:str];
  [str release];
}

-(NSMutableString*) kiki_args_to_str:(char*)message args: (kiki_arg_t*) args num_args: (int) num_args
{
  int i;
  NSMutableString* str = [NSMutableString stringWithFormat: @"recd: %s(", message];
  
  for(i=0; i<num_args; i++)
    { 
      if(i != 0)
        [str appendString: @" "];
      
      [str appendFormat: @"<%c>", args[i].type];
      switch(args[i].type)
        {
          case 'f':
            [str appendFormat: @"%f", args[i].value.f]; break;
          case 'i':
            [str appendFormat: @"%i", args[i].value.i]; break;
          case 's':
            [str appendFormat: @"%s", args[i].value.s]; break;
        }
    }
  [str appendString: @");\n"];
  return str;
}

-(void) kiki_io_print_string:(NSMutableString*)str
{
  [io_text insertText:str];
  [io_text setNeedsDisplay: YES];
}

-(IBAction) kiki_io_clear_text:              (id) sender
{
  [io_text setString: @""];
}

/*------------------------------------------------------------------------*/
-(IBAction) kiki_bass:                       (id)sender
{
  float spd = [stroke_speed floatValue];
  int   arm = (int)[stroke_arm indexOfSelectedItem];
  
  if([stroke_use_note_on indexOfSelectedItem] == 0)
    {
      kiki_send_message(kiki, kiki_cmd_bass, arm, spd);
      [self print_sent: kiki_cmd_bass, arm, spd];
    }
  else
    {
      uint8_t msg[] = {MIDI_STATUS_NOTE_ON | MIDI_CHANNEL_10, MIDI_PITCH_C_4 + arm * 12, spd * 127};
      kiki_send_raw_midi(kiki, msg, 3);
    }
}

-(IBAction) kiki_tone:                       (id)sender
{
  float spd = [stroke_speed floatValue];
  int   arm = (int)[stroke_arm indexOfSelectedItem];
  
  if([stroke_use_note_on indexOfSelectedItem] == 0)
    {
      kiki_send_message(kiki, kiki_cmd_tone, arm, spd);
      [self print_sent: kiki_cmd_tone, arm, spd];
    }
  else
    {
      uint8_t msg[] = {MIDI_STATUS_NOTE_ON | MIDI_CHANNEL_10, MIDI_PITCH_D_4 + arm * 12, spd * 127};
      kiki_send_raw_midi(kiki, msg, 3);
    }
}

-(IBAction) kiki_slap:                       (id)sender
{
  float spd   = [stroke_speed floatValue];
  int   arm = (int)[stroke_arm indexOfSelectedItem];
  
  if([stroke_use_note_on indexOfSelectedItem] == 0)
    {
      kiki_send_message(kiki, kiki_cmd_slap, arm, spd);
      [self print_sent: kiki_cmd_slap, arm, spd];
    }
  else
    {
      uint8_t msg[] = {MIDI_STATUS_NOTE_ON | MIDI_CHANNEL_10, MIDI_PITCH_E_4 + arm * 12, spd * 127};
      kiki_send_raw_midi(kiki, msg, 3);
    }
}

-(IBAction) kiki_thwack:                     (id)sender
{
  float spd = [stroke_speed floatValue];
  int   arm = (int)[stroke_arm indexOfSelectedItem];
  
  if([stroke_use_note_on indexOfSelectedItem] == 0)
    {
      kiki_send_message(kiki, kiki_cmd_thwack, arm, spd);
      [self print_sent: kiki_cmd_thwack, arm, spd];
    }
  else
    {
      uint8_t msg[] = {MIDI_STATUS_NOTE_ON | MIDI_CHANNEL_10, MIDI_PITCH_F_4 + (arm * 12), spd * 127};
      kiki_send_raw_midi(kiki, msg, 3);
    }
}

-(IBAction) kiki_cowbell:                     (id)sender
{
  float spd = [stroke_speed floatValue];
  
  if([stroke_use_note_on indexOfSelectedItem] == 0)
    {
      kiki_send_message(kiki, kiki_cmd_thwack, 2, spd);
      [self print_sent: kiki_cmd_thwack, 2, spd];
    }
  else
    {
      uint8_t msg[] = {MIDI_STATUS_NOTE_ON | MIDI_CHANNEL_10, MIDI_PITCH_F_6, spd * 127};
      kiki_send_raw_midi(kiki, msg, 3);
    }
}

/*------------------------------------------------------------------------*/
-(void) message_recd: (char*)message with_args: (kiki_arg_t*)args num_args: (int)num_args
{
  //kiki_debug_print_message(message, args, num_args);
  //[self print_recd: message args: args num_args: num_args];
  NSMutableString* str = [self kiki_args_to_str: message args: args num_args: num_args];
  [self performSelectorOnMainThread: @selector(kiki_io_print_string:) withObject: str waitUntilDone:NO];
  
  int h = kiki_hash_message(message);
  switch(h)
    {
      case kiki_hash_servo_register:
        if(num_args == 2)
          {
            int   reg = kiki_arg_to_int  (&args[0]);
            float val = kiki_arg_to_float(&args[1]);
            
            NSArray* controls = [servo_superview subviews];
            for (id control in controls)
              if(([control action] == @selector(kiki_servo_set_register:)) 
              && ([control tag]    == reg))
                [control setFloatValue:val];   
          }
        break;
      
      case kiki_hash_mmap_addr:
        if(num_args == 2)
          {
            int reg = kiki_arg_to_int(&args[0]);
            int val = kiki_arg_to_int(&args[1]);

            if((reg >= MMAP_ADDR_L_ARM_OBJECT) && (reg < MMAP_ADDR_COWBELL_SOLENOID_OBJECT))
            //while(reg >= (KIKI_MMAP_ARM_OBJECT_SIZE + MMAP_ADDR_R_ARM_OBJECT))
              reg -= KIKI_MMAP_ARM_OBJECT_SIZE;
            
            NSArray* controls = [mmap_superview subviews];
            for (id control in controls)
              if(([control action] == @selector(kiki_mmap_set_register:)) 
              && ([control tag]    == reg))
                [control setIntValue:val];   
          }
        break;

    case kiki_hash_arm_predefined_location:
      if(num_args == 5)
        {
          int arm = kiki_arg_to_int(&args[0]);
          int x   = kiki_arg_to_int(&args[1]);
          int y   = kiki_arg_to_int(&args[2]);
          int a   = kiki_arg_to_int(&args[3]);
          int loc = kiki_arg_to_int(&args[4]);
          [arm_location selectItemAtIndex:loc];
          [arm_arm  selectItemAtIndex: arm];
          [arm_view setX: x restrict_range: NO];
          [arm_view setY: y restrict_range: NO];
          [arm_view setA: a restrict_range: NO];
          [self performSelectorOnMainThread: @selector(kiki_arm_set_current_location:) withObject:nil waitUntilDone:NO];
        }
      break;
    
    case kiki_hash_arm_current_location:
      if(num_args == 4)
        {
          int arm = kiki_arg_to_int(&args[0]);
          int x   = kiki_arg_to_int(&args[1]);
          int y   = kiki_arg_to_int(&args[2]);
          int a   = kiki_arg_to_int(&args[3]);
          [arm_arm  selectItemAtIndex: arm];
          [arm_view setX: x restrict_range: NO];
          [arm_view setY: y restrict_range: NO];
          [arm_view setA: a restrict_range: NO];
        }
      break;
        
    case kiki_hash_arm_milliseconds:
    case kiki_hash_aok:
    case kiki_hash_error:
    default:
      //kiki_debug_print_message(message, args, num_args);
      break;
    }
}


@end
