//
//  MKArmView.h
//  Djembe_Kinematics_Solver
//
//  Created by Michael Krzyzaniak on 3/7/14.
//  Copyright (c) 2014 ArizonaStateUniversity. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MKArmView : NSView
{
  float x, y, a;
  float prev_x, prev_y, prev_a;
  float stroke_distance;
  float table_top_position;
  
  float handle_x, handle_y;
  bool  mouse_has_handle;
  bool  is_adjusting_angle;
  
  /*easier than subclassing NSControl*/
  id    my_target;
  SEL   my_action;
}

-(void)      setTarget:(id)target action:(SEL)action;
-(IBAction) setX: (id) sender;
-(IBAction) setY: (id) sender;
-(IBAction) setA: (id) sender;

-(void)     setX: (float)val restrict_range:(bool)restrictions;
-(void)     setY: (float)val restrict_range:(bool)restrictions;
-(void)     setA: (float)val restrict_range:(bool)restrictions;

-(float)    getX;
-(float)    getY;
-(float)    getA;

//-(IBAction) setStrokeDistance: (id) sender;

@end
