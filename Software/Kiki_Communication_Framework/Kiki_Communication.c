#include "Kiki_Communication.h"
#include "MIDI_Parser.h"
#include <stdarg.h>

/*--------------------------------------------------------*/
//should be instance var, not class var
kiki_message_received_callback kiki_recv_callback = NULL;
void* kiki_callback_self;
void  kiki_sysex_handler(midi_sysex_mfr_t mfr, char* message);

/*--------------------------------------------------------*/
#define KIKI_SYSEX_BUFFER_SIZE 100
#define KIKI_MIDI_MFR MIDI_MFR_NON_COMMERCIAL

#ifdef __APPLE__

#include <pthread.h>
#include <CoreMIDI/MIDIServices.h>
#include <Carbon/Carbon.h>

const   CFStringRef KIKI_DEVICE_NAME = CFSTR("Kiki");

/*--------------------------------------------------------*/
struct opaque_kiki_struct
{
  MIDIDeviceRef   device;
  MIDIEndpointRef source;
  MIDIEndpointRef destination;
  
  MIDIClientRef   client;
  MIDIPortRef     out_port, in_port;
  
  pthread_t       midi_client_thread;
};

/*--------------------------------------------------------*/
void    kiki_connect(Kiki* self);
Boolean kiki_setup_midi_client(Kiki* self);
void*   kiki_setup_midi_client_run_loop(void* SELF);
void    kiki_midi_data_recd_callback(const MIDIPacketList *pktlist, void* SELF, void *SELF2);
void    kiki_midi_state_changed_callback(const MIDINotification  *message, void* SELF);
Boolean kiki_check_midi_object_name(Kiki* self, MIDIObjectRef midi_object);

/*--------------------------------------------------------*/
void kiki_disconnect(Kiki* self)
{
  self->source = 0;
  MIDIFlushOutput(self->out_port);
  //fprintf(stderr, "Disconnected\n");
}

/*--------------------------------------------------------*/
Kiki* kiki_destroy(Kiki* self)
{
  if(self != NULL)
    {
      kiki_disconnect(self);
      pthread_kill(self->midi_client_thread, SIGUSR1);
      free(self);
    }
  return (Kiki*)NULL;
}

/*--------------------------------------------------------*/
Kiki* kiki_new(kiki_message_received_callback callback, void* callback_self)
{
  Kiki* self = calloc(1, sizeof(*self));
  if(self != NULL)
    {
      //aarg, these should be instance vars, not class vars...
      kiki_recv_callback = callback;
      kiki_callback_self = callback_self;
      
      midi_sysex_event_handler = kiki_sysex_handler;
      
      if(!kiki_setup_midi_client(self))
        self = kiki_destroy(self);
    }
    
  return self;
}

#endif //__APPLE__
/*--------------------------------------------------------*/
void kiki_init(kiki_message_received_callback callback, void* callback_self)
{
  kiki_recv_callback = callback;
  kiki_callback_self = callback_self;
  midi_sysex_event_handler = kiki_sysex_handler;
}

#ifdef __APPLE__
/*--------------------------------------------------------*/
void kiki_connect(Kiki* self)
{
  ItemCount num_sources      = MIDIGetNumberOfSources();
  MIDIEndpointRef source;
  int i;
  
  if(self->client == 0)
    {
      kiki_setup_midi_client(self);
      return;
    }
  
  //find source named "Kiki"
  for(i=0; i<num_sources; i++)
    {
      if((source = MIDIGetSource(i)) != 0)
        {
          if(kiki_check_midi_object_name(self, source))
            {
              self->source = source;
              //fprintf(stderr, "connected\n");
              
              MIDIPortConnectSource(self->in_port, self->source, self);  
              break;
            }
        }
    }

  //get the source's parent device and destination
  if(self->source != 0)
    {
      OSStatus error = MIDIEntityGetDevice(self->source, &self->device);
      if(self->device != 0)
        self->destination = MIDIEntityGetDestination(self->device, 0);
    }
}

/*--------------------------------------------------------*/
//true on success
Boolean kiki_setup_midi_client(Kiki* self)
{
  return !pthread_create(&self->midi_client_thread, NULL, kiki_setup_midi_client_run_loop, self);
}

/*--------------------------------------------------------*/
void* kiki_setup_midi_client_run_loop(void* SELF)
{
  Kiki* self = (Kiki*)SELF;
  
  OSStatus error = noErr;
  error = MIDIClientCreate(KIKI_DEVICE_NAME, kiki_midi_state_changed_callback, self, &self->client);
  
  if(error) fprintf(stderr, "Error creating MIDI client: %d\n", error);
  
  else
    {
      error = MIDIOutputPortCreate (self->client, CFSTR("Kiki Client Output Port"), &self->out_port);
      if(error) fprintf(stderr, "Error creating MIDI client output port: %d\n", error);
      else 
        {
          error = MIDIInputPortCreate(self->client, CFSTR("Kiki Client Input Port"), kiki_midi_data_recd_callback, self, &self->in_port);    
          if(error)
            { 
              fprintf(stderr, "Error creating MIDI client input port: %d\n", error);
              MIDIEndpointDispose(self->out_port);
            }
        }
        
      if(error)
        {
          MIDIClientDispose(self->client);
          self->client = 0;
        }
    }
  
  if(error == noErr)
    {
      kiki_connect(self);
      CFRunLoopRun();
    }
  
  return NULL;
}

/*--------------------------------------------------------*/
Boolean kiki_check_midi_object_name(Kiki* self, MIDIObjectRef midi_object)
{
  CFStringRef str;
  MIDIObjectGetStringProperty(midi_object, kMIDIPropertyName, &str);
  CFComparisonResult comparison = CFStringCompare (KIKI_DEVICE_NAME, str, kCFCompareCaseInsensitive | kCFCompareWidthInsensitive);
  CFRelease(str);
  
  return comparison == kCFCompareEqualTo;
}

/*--------------------------------------------------------*/
void kiki_midi_data_recd_callback(const MIDIPacketList *packet_list, void* SELF, void *SELF2)
{
  //Kiki* self = (Kiki*)SELF;
  const MIDIPacket *packet = &(packet_list->packet[0]);
  int i, j;
  for(i=0; i<packet_list->numPackets; i++)
    {
      for(j=0; j<packet->length; j++)
        midi_parse(packet->data[j]);
      packet = MIDIPacketNext (packet);
    }
}

/*--------------------------------------------------------*/
void kiki_midi_state_changed_callback(const MIDINotification *message, void* SELF)
{
  Kiki* self = (Kiki*)SELF;
  
  switch(message->messageID)
    {
      case kMIDIMsgObjectAdded:
        if((self->source == 0) && (((MIDIObjectAddRemoveNotification*)message)->childType == kMIDIObjectType_Source))
          kiki_connect(self);
        break;
        
      case kMIDIMsgObjectRemoved:
        if((self->source != 0) && (((MIDIObjectAddRemoveNotification*)message)->childType == kMIDIObjectType_Source))
          if(kiki_check_midi_object_name(self, ((MIDIObjectAddRemoveNotification*)message)->child))
            kiki_disconnect(self);
        break;
        
      case kMIDIMsgSetupChanged:
        break;
      case kMIDIMsgPropertyChanged :
        break;
      case kMIDIMsgThruConnectionsChanged:
        break;
      case kMIDIMsgSerialPortOwnerChanged:
        break;
      case kMIDIMsgIOError:
        break;
      default: break;
    }
}

/*--------------------------------------------------------*/
void kiki_send_raw_midi(Kiki* self, uint8_t* midi_bytes, int num_bytes)
{
	MIDIPacketList packet_list;
	MIDIPacket* current_packet;
	current_packet = MIDIPacketListInit(&packet_list);	
	current_packet = MIDIPacketListAdd(&packet_list, sizeof(packet_list), current_packet, 0 /*mach_absolute_time()*/, num_bytes, midi_bytes);
  MIDISend(self->out_port, self->destination, &packet_list);
}

/*--------------------------------------------------------*/
void kiki_send_message(Kiki* self, const char *fmt, ...)
{
  if(self->source != 0)
    {
#else //!__APPLE__
void kiki_send_message(const char *fmt, ...)
{
#endif //__APPLE__

      uint8_t  buffer[KIKI_SYSEX_BUFFER_SIZE];
      uint8_t* b = buffer;

      va_list args;
      va_start(args, fmt);

      *b++ = MIDI_STATUS_SYSTEM_EXCLUSIVE;
      *b++ = MIDI_MFR_NON_COMMERCIAL;

      b += vsnprintf((char*)b, KIKI_SYSEX_BUFFER_SIZE-2, fmt, args);

      if(b-buffer >= KIKI_SYSEX_BUFFER_SIZE)
        b = buffer + KIKI_SYSEX_BUFFER_SIZE - 1;

      //replace '\0' with EOX
      *b++ = MIDI_STATUS_END_OF_EXCLUSIVE;
      
#ifdef __APPLE__
      kiki_send_raw_midi(self, buffer, b - buffer);
    }
#else
      usb_midi_send_sysex(buffer, b-buffer);
#endif
}

/*---------------------------------------------------*/
void kiki_sysex_handler(midi_sysex_mfr_t mfr, char* message)
{
  //fprintf(stderr, "SYSEX\tmfr:%i\tmessage:%s\t\n", mfr, message); 
  int MAX_NUM_ARGS = 8;
  kiki_arg_t args[MAX_NUM_ARGS], *a = args;
  
  if(mfr != KIKI_MIDI_MFR) return;
  
  while(1)
    {
      if((a-args) <= MAX_NUM_ARGS)
        {
          int contains_period = 0;
          int should_break    = 0;
          char* end;
          
          //strip whitespace chars
          while((*message <= ' ') && (*message != '\0'))
            message++;
                    
          if(*message == '\0')
            break;
          
           end = message;
           
          //find next space and replace with null
          while((*end != '\0') && (*end != ' '))
            if(*end++ == '.')
              contains_period = 1;

          if(*end == '\0') should_break = 1;
          else *end = '\0';
          
          //next argrument is a number
          if(((*message >= '0') && (*message <= '9')) || (*message == '+') || (*message == '-'))
            {            
              if(contains_period) //it is a float
                {
                  a->type = 'f';
                  a->value.f = (float)atof(message);
                  a++;
                }
              else // it is an int
                {
                  a->type = 'i';
                  a->value.i = (int32_t)atol(message);
                  a++;
                }
            }

          //next argument is a string
          else if((*message >= '!') && (*message <= '~'))
            {
              a->type = 's';
              a->value.s = message;
              a++;          
            }

          if(should_break) break;
          else(message = end+1);
        }
      else //fmt string is full
        break;
    }
    
    if(args[0].type == 's')
      kiki_recv_callback(kiki_callback_self, args[0].value.s, args+1, a-args-1);
}

/*-----------------------------------------------------*/
float kiki_arg_to_float(kiki_arg_t* arg)
{
  float result = 0;
  switch(arg->type)
    {
      case 'f': 
        result = arg->value.f;
        break;
      case 'i':
        result = arg->value.i;
        break;
      case 's':
        result = (float)atof(arg->value.s);
        break;
      default: 
        break;
    }
    return result;
}

/*-----------------------------------------------------*/
int32_t kiki_arg_to_int(kiki_arg_t* arg)
{
  int32_t result = 0;
  switch(arg->type)
    {
      case 'f': 
        result = arg->value.f;
        break;
      case 'i':
        result = arg->value.i;
        break;
      case 's':
        result = (int32_t)atol(arg->value.s);
        break;
      default: 
        break;
    }
  return result;  
}

/*-----------------------------------------------------*/
uint32_t kiki_hash_message(char* message)
{
  uint32_t hash = 5381;
  while (*message != '\0')
    hash = hash * 33 ^ (int)*message++;
  return hash;
}

/*-----------------------------------------------------*/
void kiki_debug_print_message(char* message, kiki_arg_t args[], int num_args)
{
#ifdef __APPLE__
  int i;
  fprintf(stderr, "recd: %s(", message);
  
  for(i=0; i<num_args; i++)
    { 
      if(i != 0)
        putchar(' ');
      printf("<%c>", args[i].type);
      switch(args[i].type)
        {
          case 'f':
            printf("%f", args[i].value.f); break;
          case 'i':
            printf("%i", args[i].value.i); break;
          case 's':
            printf("%s", args[i].value.s); break;
        }
    }
  printf(");\r\n");
#endif// __APPLE__
}
