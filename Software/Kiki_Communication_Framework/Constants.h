#ifndef __KIKI_COMMUNICATION_CONSTANTS__
#define __KIKI_COMMUNICATION_CONSTANTS__ 1 


#include "Mmap_Constants.h"

/*----------------------------------------------------*/
typedef enum arm_predefined_location_enum
{
  ARM_LOCATION_OFFSET = 0,
  ARM_LOCATION_REST,
  ARM_LOCATION_BASS,
  ARM_LOCATION_TONE,
  ARM_LOCATION_SLAP,
  ARM_NUM_LOCATIONS,
}arm_predefined_location_t;

/*----------------------------------------------------*/
typedef enum arm_index_enum
{
  ARM_INDEX_LEFT,
  ARM_INDEX_RIGHT,
}arm_index_t;


#endif //__KIKI_COMMUNICATION_CONSTANTS__

