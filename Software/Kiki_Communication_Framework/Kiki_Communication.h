#ifndef KIKI_COMMUNICATION
#define KIKI_COMMUNICATION 1

#if defined(__cplusplus)
extern "C"{
#endif   //(__cplusplus)

#include <stdint.h>
#include "MIDI_Parser.h"
#include "Constants.h"

/*---------------------------------------------------*/
typedef struct kiki_arg_struct
{
  union kiki_arg_value_union
    {
      float   f;
      int32_t i;
      char*   s;
    }value;

  char type;
}kiki_arg_t;

/*---------------------------------------------------*/
typedef enum kiki_message_hash_enum
{
  kiki_hash_servo_get                   = 2729253246,
  kiki_hash_servo_set                   = 2729266538,
  kiki_hash_servo_prepare               = 4197311433,
  kiki_hash_servo_do_prepared           =  574166457,
  kiki_hash_servo_ping                  = 4166258424,
  kiki_hash_servo_factory_reset         = 1682338742,
  
  kiki_hash_mmap_get                    = 1899342642,
  kiki_hash_mmap_set                    = 1899364134,
  kiki_hash_mmap_undo                   = 2549421140,
  kiki_hash_mmap_save                   = 2549478341,
  kiki_hash_mmap_factory_reset          =  761174138,
  
  kiki_hash_bass                        =  100943785,
  kiki_hash_tone                        =  101583962,
  kiki_hash_slap                        =  101481156,
  kiki_hash_thwack                      = 3253059912,
  
  kiki_hash_arm_set_current_location    = 4280917853,
  kiki_hash_arm_get_current_location    = 1773171401,
  kiki_hash_arm_get_predefined_location =  500541798,
  kiki_hash_arm_set_predefined_location =  554142578,
  kiki_hash_arm_strike                  = 1774053049,
  
  kiki_hash_servo_register              = 3436190337,
  kiki_hash_mmap_addr                   = 2548966359,
  kiki_hash_arm_milliseconds            = 2529009253,
  kiki_hash_arm_current_location        = 3791209664,
  kiki_hash_arm_predefined_location     =  934587791,
  kiki_hash_aok                         = 2085472399,
  kiki_hash_error                       = 3342388946,
    
}kiki_message_hash_t;

/*--------------------------------------------------------*/
#define kiki_cmd_servo_get                   "/servo_get %i %i"         //id, reg
#define kiki_cmd_servo_set                   "/servo_set %i %i %f"      //id, reg, val
#define kiki_cmd_servo_prepare               "/servo_prepare %i %i %f"  //id, reg, val
#define kiki_cmd_servo_do_prepared           "/servo_do_prepared %i"    //id
#define kiki_cmd_servo_ping                  "/servo_ping %i"           //id
#define kiki_cmd_servo_factory_reset         "/servo_factory_reset %i"  //id

#define kiki_cmd_mmap_get                    "/mmap_get %i"              //addr
#define kiki_cmd_mmap_set                    "/mmap_set %i %i"           //addr, val
#define kiki_cmd_mmap_undo                   "/mmap_undo"                //none
#define kiki_cmd_mmap_save                   "/mmap_save"                //none
#define kiki_cmd_mmap_factory_reset          "/mmap_factory_reset"       //none
      
#define kiki_cmd_bass                        "/bass   %i %f"               //arm, loudness
#define kiki_cmd_tone                        "/tone   %i %f"               //arm, loudness
#define kiki_cmd_slap                        "/slap   %i %f"               //arm, loudness
#define kiki_cmd_thwack                      "/thwack %i %f"               //solenoid, loudness
//#define kiki_cmd_cowbell is treated as kiki_cmd_thwack with solenoid = 2;

#define kiki_cmd_arm_get_current_location    "/arm_get_current_location %i"                 //arm 
#define kiki_cmd_arm_set_current_location    "/arm_set_current_location %i %i %i %i %f"     //arm, x, y, a, speed
#define kiki_cmd_arm_get_predefined_location "/arm_get_predefined_location %i %i"           //arm, loc
#define kiki_cmd_arm_set_predefined_location "/arm_set_predefined_location %i %i %i %i %i"  //arm, x, y, a, loc
#define kiki_cmd_arm_strike                  "/arm_strike  %i %i %i %i %f"                  //arm, x, y, a, speed

#define kiki_reply_servo_register            "/servo_register %i %f"
#define kiki_reply_mmap_addr                 "/mmap_addr %i %i"
#define kiki_reply_arm_milliseconds          "/arm_milliseconds %f"
#define kiki_reply_arm_current_location      "/arm_current_location %i %i %i %i"        //arm, x, y, a
#define kiki_reply_arm_predefined_location   "/arm_predefined_location %i %i %i %i %i"  //arm, x, y, a, loc
#define kiki_reply_aok                       "/aok"
#define kiki_reply_error                     "/error %s"

/*---------------------------------------------------*/
typedef void  (*kiki_message_received_callback)(void* self, char* message, kiki_arg_t args[], int num_args);

/*--------------------------------------------------------*/
#ifdef  __APPLE__

typedef  struct opaque_kiki_struct Kiki;
Kiki*    kiki_new                (kiki_message_received_callback callback, void* callback_self);
Kiki*    kiki_destroy            (Kiki* self);
void     kiki_send_message       (Kiki* self, const char *message, /*args*/...);
void     kiki_send_raw_midi      (Kiki* self, uint8_t* midi_bytes, int num_bytes);


#else //!__APPLE__, ie this code is running on the robot

void     kiki_send_message       (const char *message, /*args*/...);

#endif// __APPLE__

void     kiki_init               (kiki_message_received_callback callback, void* callback_self);
float    kiki_arg_to_float       (kiki_arg_t* arg);
int32_t  kiki_arg_to_int         (kiki_arg_t* arg);

uint32_t kiki_hash_message       (char* message);
void     kiki_debug_print_message(char* message, kiki_arg_t args[], int num_args);

#if defined(__cplusplus)
}
#endif   //(__cplusplus)

#endif //KIKI_COMMUNICATION
