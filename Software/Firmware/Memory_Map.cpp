#include <EEPROM.h>
#include "Memory_Map.h"
#include <Arduino.h>
#include "Arm_Controller.h"

/*---------------------------------------------------*/
// changing this will factory reset the eeprom on boot
#define MMAP_MAGIC_NUMBER         69

/*---------------------------------------------------*/
uint8_t mmap_cache[MMAP_SIZE];
const unsigned MMAP_ADDR_CACHE_START = (unsigned)&mmap_cache;

/*---------------------------------------------------*/
typedef enum mmap_size_enum
{
  MMAP_SIZE_INT_8  = 1,
  MMAP_SIZE_INT_16 = 2,
  MMAP_SIZE_INT_32 = 4,
}mmap_size_t;

/*---------------------------------------------------*/
void mmap_fill_cache();

/*---------------------------------------------------*/
mmap_size_t mmap_get_size(mmap_address_t addr)
{
  mmap_size_t result = MMAP_SIZE_INT_16;
  
  switch(addr)
    {
      case   MMAP_ADDR_MAGIC_NUMBER:
      case   MMAP_ADDR_MIDI_CHANNEL:
        result = MMAP_SIZE_INT_8;
        break;
      case MMAP_ADDR_UART_BAUD:
      case MMAP_ADDR_STRIKE_DELAY_MILLIS:
        result = MMAP_SIZE_INT_32;
        break;
      default: break;
    }
  return result;
}

/*---------------------------------------------------*/
void mmap_init()
{
  uint8_t magic_number = EEPROM.read(MMAP_ADDR_MAGIC_NUMBER);
  
  if(magic_number == MMAP_MAGIC_NUMBER)
    mmap_fill_cache();
  else
    {
      mmap_factory_reset();
      mmap_save();
    }
}

/*---------------------------------------------------*/
void mmap_factory_reset()
{
  uint8_t* c = mmap_cache;
  int i = 0;
  for(i=0; i<MMAP_SIZE; i++)
    *c++ = 0;
  
  mmap_set(MMAP_ADDR_MAGIC_NUMBER, MMAP_MAGIC_NUMBER);
  mmap_set(MMAP_ADDR_MIDI_CHANNEL, 10    );
  mmap_set(MMAP_ADDR_UART_BAUD   , 57142 );
  mmap_set(MMAP_ADDR_STRIKE_DELAY_MILLIS, 500);
  mmap_set(MMAP_ADDR_R_ARM_OFFSET_X, ARM_RIGHT_LENGTH);
  mmap_set(MMAP_ADDR_R_ARM_REST_X  , ARM_RIGHT_LENGTH);
  mmap_set(MMAP_ADDR_R_ARM_BASS_X  , ARM_RIGHT_LENGTH);
  mmap_set(MMAP_ADDR_R_ARM_TONE_X  , ARM_RIGHT_LENGTH);
  mmap_set(MMAP_ADDR_R_ARM_SLAP_X  , ARM_RIGHT_LENGTH);
  mmap_set(MMAP_ADDR_L_ARM_OFFSET_X, ARM_LEFT_LENGTH);
  mmap_set(MMAP_ADDR_L_ARM_REST_X  , ARM_LEFT_LENGTH);
  mmap_set(MMAP_ADDR_L_ARM_BASS_X  , ARM_LEFT_LENGTH);
  mmap_set(MMAP_ADDR_L_ARM_TONE_X  , ARM_LEFT_LENGTH);
  mmap_set(MMAP_ADDR_L_ARM_SLAP_X  , ARM_LEFT_LENGTH);
  mmap_set(MMAP_ADDR_R_SOLENOID_PIN, 3);
  mmap_set(MMAP_ADDR_R_SOLENOID_MIN_EJECT_DURATION, 80 );
  mmap_set(MMAP_ADDR_R_SOLENOID_MAX_EJECT_DURATION, 120);
  mmap_set(MMAP_ADDR_L_SOLENOID_PIN, 4);
  mmap_set(MMAP_ADDR_L_SOLENOID_MIN_EJECT_DURATION, 80 );
  mmap_set(MMAP_ADDR_L_SOLENOID_MAX_EJECT_DURATION, 120);
  mmap_set(MMAP_ADDR_COWBELL_SOLENOID_PIN, 5);
  mmap_set(MMAP_ADDR_COWBELL_SOLENOID_MIN_EJECT_DURATION, 80 );
  mmap_set(MMAP_ADDR_COWBELL_SOLENOID_MAX_EJECT_DURATION, 120);
}

/*---------------------------------------------------*/
int32_t  mmap_get(mmap_address_t addr)
{
  int32_t result = 0;
  mmap_size_t data_size = mmap_get_size(addr);
  void* c = mmap_cache + addr;
  
  if((addr + (int)data_size) <= MMAP_SIZE)
    {
      switch(data_size)
        {
          case MMAP_SIZE_INT_8:
            result = *((int8_t*)c);
            break;
          case MMAP_SIZE_INT_16:
            result = *((int16_t*)c);
            break;          
          case MMAP_SIZE_INT_32:
            result = *((int32_t*)c);
            break;
          default: break;
        }
    }
  return result;
}

/*---------------------------------------------------*/
mmap_success_t  mmap_set(mmap_address_t addr, int32_t val)
{
  int32_t result = 0;
  mmap_size_t data_size = mmap_get_size(addr);
  void* c = mmap_cache + addr;
  
  if(((addr >= MMAP_ADDR_MAGIC_NUMBER)) && (addr + (int)data_size) <= MMAP_SIZE)
    {
      switch(data_size)
        {
          case MMAP_SIZE_INT_8:
            *((int8_t*)c) = val;
            break;
          case MMAP_SIZE_INT_16:
            *((int16_t*)c) = val;
            break;          
          case MMAP_SIZE_INT_32:
            *((int32_t*)c) = val;
            break;
          default: break;
        }
    }
  return true;
}

/*---------------------------------------------------*/
void mmap_undo()
{
  mmap_fill_cache();
}

/*---------------------------------------------------*/
void mmap_fill_cache()
{
  int i;
  uint8_t* c = mmap_cache;
  
  for(i=0; i<MMAP_SIZE; i++)
    *c++ =  EEPROM.read(i);
}

/*---------------------------------------------------*/
void mmap_save()
{
  int i;
  uint8_t* c = mmap_cache;
  
  for(i=0; i<MMAP_SIZE; i++)
    EEPROM.write(i, *c++);
}

