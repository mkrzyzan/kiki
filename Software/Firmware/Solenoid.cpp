#include "Solenoid.h"
#include "Memory_Map.h"
#include "Arduino.h"

#define SOLENOID_QUEUE_SIZE 10

/*----------------------------------------------------*/
typedef uint32_t solenoid_ctrl_time_t;

/*----------------------------------------------------*/
typedef enum solenoid_ctrl_queued_event_state_enum
{
  SOLENOID_STATE_INACTIVE,
  SOLENOID_STATE_DELAYING,
  SOLENOID_STATE_STRIKING,
}solenoid_ctrl_queueud_event_state_t;

/*----------------------------------------------------*/
typedef struct solenoid_ctrl_queued_event_struct
{
  solenoid_ctrl_time_t start_time;
  solenoid_ctrl_time_t impact_time;
  int pwm_level;
}solenoid_ctrl_queued_event_t;

/*----------------------------------------------------*/
typedef struct solenoid_ctrl_queue_struct
{
  int num_entries;
  solenoid_ctrl_queued_event_t* head;
  solenoid_ctrl_queued_event_t* tail;
  solenoid_ctrl_queued_event_t  buffer[SOLENOID_QUEUE_SIZE];
}solenoid_ctrl_queue_t;

/*----------------------------------------------------*/
typedef struct opaque_solenoid_mmap_struct
{
  uint16_t pin;
  uint16_t min_duration;
  uint16_t max_duration;
}solenoid_mmap_t;

/*----------------------------------------------------*/
struct opaque_solenoid_struct
{
  solenoid_mmap_t* mmap;
  solenoid_ctrl_queue_t queue;
  solenoid_ctrl_queueud_event_state_t state;
};

/*----------------------------------------------------*/
Solenoid _solenoid_right_solenoid;
Solenoid _solenoid_left_solenoid;
Solenoid _solenoid_cowbell_solenoid;
Solenoid* solenoid_right_solenoid = &_solenoid_right_solenoid;
Solenoid* solenoid_left_solenoid  = &_solenoid_left_solenoid;
Solenoid* solenoid_cowbell_solenoid = &_solenoid_cowbell_solenoid;
Solenoid* solenoids[SOLENOID_NUM_SOLENOIDS];

/*----------------------------------------------------*/
void solenoid_init()
{
  int i;
  
  solenoid_right_solenoid->mmap = (solenoid_mmap_t*)(MMAP_ADDR_CACHE_START + MMAP_ADDR_R_SOLENOID_OBJECT);
  solenoid_left_solenoid->mmap  = (solenoid_mmap_t*)(MMAP_ADDR_CACHE_START + MMAP_ADDR_L_SOLENOID_OBJECT);
  solenoid_cowbell_solenoid->mmap  = (solenoid_mmap_t*)(MMAP_ADDR_CACHE_START + MMAP_ADDR_COWBELL_SOLENOID_OBJECT);
  solenoids[0] = solenoid_right_solenoid;
  solenoids[1] = solenoid_left_solenoid;
  solenoids[2] = solenoid_cowbell_solenoid;
  
  for(i=0; i<SOLENOID_NUM_SOLENOIDS; i++)
    {
      solenoids[i]->state = SOLENOID_STATE_INACTIVE;
      solenoids[i]->queue.head = solenoids[i]->queue.tail = solenoids[i]->queue.buffer;
      solenoids[i]->queue.num_entries = 0;
    }
}

/*----------------------------------------------------*/
void solenoid_strike(Solenoid* self, float loudness)
{
  if(loudness > 1) loudness = 1;
  if(loudness < 0) loudness = 0;
  
  if(self->queue.num_entries < SOLENOID_QUEUE_SIZE)
    {
      solenoid_ctrl_queued_event_t *e = self->queue.head++;
      if((self->queue.head - self->queue.buffer) >= SOLENOID_QUEUE_SIZE)
        self->queue.head = self->queue.buffer;
     
      e->impact_time = *((int32_t*)(MMAP_ADDR_CACHE_START + MMAP_ADDR_STRIKE_DELAY_MILLIS)) + millis();
      e->start_time  = e->impact_time;
      e->start_time -= (1-loudness) * (self->mmap->max_duration - self->mmap->min_duration) + self->mmap->min_duration; 
      e->pwm_level   = loudness * 178 + 77;
      
      self->queue.num_entries++;
    }
}


/*----------------------------------------------------*/
void solenoid_ctrl_run_loop()
{
  int i;
  uint32_t now = millis();
  
  for(i=0; i<SOLENOID_NUM_SOLENOIDS; i++)
    {
      Solenoid* self = solenoids[i];
      if(self->queue.num_entries > 0)
        {
          solenoid_ctrl_queued_event_t *e = self->queue.tail;
          
          if(self->state == SOLENOID_STATE_INACTIVE)
            if(now >= e->start_time)
              {
                analogWrite(self->mmap->pin, e->pwm_level);
                self->state = SOLENOID_STATE_STRIKING;
              }
          
          if(self->state == SOLENOID_STATE_STRIKING)
            if(now >= e->impact_time)
              {
                analogWrite(self->mmap->pin, 0);
                self->queue.tail++;
                if((self->queue.tail - self->queue.buffer) >= SOLENOID_QUEUE_SIZE)
                  self->queue.tail = self->queue.buffer;
                self->queue.num_entries--;
                  
                self->state = SOLENOID_STATE_INACTIVE;
              }
        }
    }
}

