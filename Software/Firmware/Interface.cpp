//#include <HardwareSerial.h>

#include <Arduino.h>
#include "Interface.h"
#include "Dynamixel_Servo.h"
#include "Kiki_Communication.h"
#include "Memory_Map.h"
#include "Arm_Controller.h"
#include "Solenoid.h"

void interface_dispatch          (void* self, char* message, kiki_arg_t args[], int num_args);
void interface_note_on_callback  (midi_channel_t chan, midi_pitch_t pitch, midi_velocity_t vel);
void interface_note_off_callback (midi_channel_t chan, midi_pitch_t pitch, midi_velocity_t vel);
void interface_send_aok          ();
void interface_send_error        (const char* error);
void interface_send_servo_error  (servo_error_t error);

HardwareSerial* interface_serial = NULL;

/*---------------------------------------------------*/
void interface_init()
{
  midi_note_on_event_handler  = interface_note_on_callback;
  midi_note_off_event_handler = interface_note_off_callback;
  kiki_init(interface_dispatch, NULL);
}

/*---------------------------------------------------*/
void interface_run_loop()
{
  /* kiki_communication depends on this too */
  while(usbMIDI.available())
    midi_parse(usbMIDI.read_raw());
}

/*---------------------------------------------------*/
void interface_send_aok()
{ 
  kiki_send_message(kiki_reply_aok); 
}

/*---------------------------------------------------*/
void interface_send_error(const char* error)
{ 
  kiki_send_message(kiki_reply_error, error); 
}

/*---------------------------------------------------*/
void interface_send_servo_error(servo_error_t error)
{ 
  if(error == SERVO_NO_ERROR)
    interface_send_aok();
  else
    interface_send_error(servo_errors_to_string(error));
}

/*---------------------------------------------------*/
void interface_note_on_callback(midi_channel_t chan, midi_pitch_t pitch, midi_velocity_t vel)
{
  //kiki_send_message("here:%i %i %i", chan, pitch, vel);
  //uint8_t* midi_channel = (uint8_t*) (MMAP_ADDR_CACHE_START + MMAP_ADDR_MIDI_CHANNEL);
  
  //if(chan == (*midi_channel) - 1)
    {
      float spd = vel / 127.0;
      
      switch(pitch)
        {
          case INTERFACE_BASS_MIDI_NOTE:                                   arm_strike_predefined(arms[0], ARM_LOCATION_BASS, spd); break;
          case INTERFACE_TONE_MIDI_NOTE:                                   arm_strike_predefined(arms[0], ARM_LOCATION_TONE, spd); break;
          case INTERFACE_SLAP_MIDI_NOTE:                                   arm_strike_predefined(arms[0], ARM_LOCATION_SLAP, spd); break;
          case INTERFACE_BASS_MIDI_NOTE + INTERFACE_ARM_INDEX_MIDI_OFFSET: arm_strike_predefined(arms[1], ARM_LOCATION_BASS, spd); break;
          case INTERFACE_TONE_MIDI_NOTE + INTERFACE_ARM_INDEX_MIDI_OFFSET: arm_strike_predefined(arms[1], ARM_LOCATION_TONE, spd); break;
          case INTERFACE_SLAP_MIDI_NOTE + INTERFACE_ARM_INDEX_MIDI_OFFSET: arm_strike_predefined(arms[1], ARM_LOCATION_SLAP, spd); break;

          case INTERFACE_SOLENOID_MIDI_NOTE:                                   solenoid_strike      (solenoids[0], spd); break;
          case INTERFACE_SOLENOID_MIDI_NOTE + INTERFACE_ARM_INDEX_MIDI_OFFSET: solenoid_strike      (solenoids[1], spd); break;
          case INTERFACE_BELL_MIDI_NOTE:                                       solenoid_strike      (solenoids[2], spd); break;
          default: break;
        }
    }
}

/*---------------------------------------------------*/
void interface_note_off_callback(midi_channel_t chan, midi_pitch_t pitch, midi_velocity_t vel)
{

}

/*-----------------------------------------------------*/
void interface_dispatch(void* self, char* message, kiki_arg_t args[], int num_args)
{ 
  int INTERFACE_SERVO_TIMEOUT = 5; //ms
  servo_error_t error;
  
  switch(kiki_hash_message(message))
    {
      /*---------------------------------------------------*/
      case kiki_hash_servo_get:
        if(num_args == 2)
          {
            float result;
            int id  = kiki_arg_to_int  (&args[0]);
            int reg = kiki_arg_to_int  (&args[1]);
            error = servo_get(id,(servo_register_t)reg, &result, INTERFACE_SERVO_TIMEOUT);
            if(error == SERVO_NO_ERROR)
              kiki_send_message(kiki_reply_servo_register, reg, result);
            else
              interface_send_servo_error(error);
          }
        break;
        
      /*---------------------------------------------------*/  
      case kiki_hash_servo_set:
        if(num_args == 3)
          {
            int   id  = kiki_arg_to_int  (&args[0]);
            int   reg = kiki_arg_to_int  (&args[1]);
            float val = kiki_arg_to_float(&args[2]);
            error = servo_set(id, (servo_register_t)reg, val, INTERFACE_SERVO_TIMEOUT);
            interface_send_servo_error(error);
          }
        break;
        
      /*---------------------------------------------------*/
      case kiki_hash_servo_prepare:
        if(num_args == 3)
          {
            int   id  = kiki_arg_to_int  (&args[0]);
            int   reg = kiki_arg_to_int  (&args[1]);
            float val = kiki_arg_to_float(&args[2]);
            error = servo_prepare(id, (servo_register_t)reg, val, INTERFACE_SERVO_TIMEOUT);
            interface_send_servo_error(error);
          }
        break;
        
      /*---------------------------------------------------*/
      case kiki_hash_servo_do_prepared:
        if(num_args == 1)
          {
            int   id  = kiki_arg_to_int  (&args[0]);
            error =servo_do_prepared(id, INTERFACE_SERVO_TIMEOUT);
            interface_send_servo_error(error);
          }
        break;
        
      /*---------------------------------------------------*/  
      case kiki_hash_servo_ping:
        if(num_args == 1)
          {
            int   id  = kiki_arg_to_int  (&args[0]);
            unsigned long later, now;
            now = micros();
            error = servo_ping(id, INTERFACE_SERVO_TIMEOUT);
            later = micros();
            kiki_send_message("microseconds %i", (int)(later-now));
            interface_send_servo_error(error);
          }
        break;

      /*---------------------------------------------------*/
      case kiki_hash_servo_factory_reset:
        if(num_args == 1)
          {
            int   id  = kiki_arg_to_int  (&args[0]);
            error = servo_factory_reset(id, INTERFACE_SERVO_TIMEOUT);
            interface_send_servo_error(error);
          }
        break;

      /*---------------------------------------------------*/
      case kiki_hash_mmap_get:
        if(num_args == 1)
          {
            int addr = kiki_arg_to_int(&args[0]);
            int32_t val = mmap_get((mmap_address_t)addr);
            kiki_send_message(kiki_reply_mmap_addr, addr, val);
          }
        break;
        
      /*---------------------------------------------------*/
      case kiki_hash_mmap_set:
        if(num_args == 2)
          {
            int addr = kiki_arg_to_int(&args[0]);
            int val  = kiki_arg_to_int(&args[1]);
            mmap_success_t success = mmap_set((mmap_address_t)addr, val);
            success ? interface_send_aok() : interface_send_servo_error(SERVO_ERROR_RANGE);
          }
        break;

      /*---------------------------------------------------*/
      case kiki_hash_mmap_undo:
        if(num_args == 0)
          {
            mmap_undo();
            interface_send_aok();
          }
        break;

      /*---------------------------------------------------*/
      case kiki_hash_mmap_save:
        if(num_args == 0)
          {
            mmap_save();
            interface_send_aok();
          }
        break;
 
      /*---------------------------------------------------*/
      case kiki_hash_mmap_factory_reset:
        if(num_args == 0)
          {
            mmap_factory_reset();
            interface_send_aok();
          }
        break;
  
      /*---------------------------------------------------*/
      case kiki_hash_arm_get_current_location:
        if(num_args == 1)
          {
            int  i     = kiki_arg_to_int  (&args[0]) % ARM_NUM_ARMS;
            Arm* arm   = arms[i];
            arm_endpoint_location_t loc = {0, 0, 0};
            error = arm_get_current_location(arm, &loc);
            if   (error != SERVO_NO_ERROR) interface_send_servo_error(error);
            else kiki_send_message(kiki_reply_arm_current_location, i, loc.x, loc.y, loc.a);
          }
        break;
        
      /*---------------------------------------------------*/
      case kiki_hash_arm_set_current_location:
        if(num_args == 5)
          {
            int   i    = kiki_arg_to_int  (&args[0]);
            int   x    = kiki_arg_to_int  (&args[1]);
            int   y    = kiki_arg_to_int  (&args[2]);
            int   a    = kiki_arg_to_int  (&args[3]);
            float spd  = kiki_arg_to_float(&args[4]);
            Arm* arm   = arms[i % ARM_NUM_ARMS];
            arm_endpoint_location_t endpoint = {x, y, a};
            float milliseconds;
            
            unsigned long later, now;
            now = micros();
            
            error = arm_set_current_location(arm, &endpoint, spd, &milliseconds);
            
            later = micros();
            kiki_send_message("microseconds %i", (int)(later-now));
            
            if   (error != SERVO_NO_ERROR) interface_send_servo_error(error);
            else kiki_send_message(kiki_reply_arm_milliseconds, milliseconds);
          }
        break;
      
      /*---------------------------------------------------*/
      case kiki_hash_arm_get_predefined_location:
        if(num_args == 2)
          {
            int  i     = kiki_arg_to_int  (&args[0]);
            int  l     = kiki_arg_to_int  (&args[1]);
            Arm* arm   = arms[i % ARM_NUM_ARMS];
            arm_endpoint_location_t* loc;
            loc = arm_get_predefined_location(arm, (arm_predefined_location_t)l);
            if(loc == NULL) interface_send_servo_error(SERVO_ERROR_RANGE);
            else kiki_send_message(kiki_reply_arm_predefined_location, i, loc->x, loc->y, loc->a, l);
          }
        break;
        
      /*---------------------------------------------------*/ 
      case kiki_hash_arm_set_predefined_location:
        if(num_args == 5)
          {
            int   i    = kiki_arg_to_int  (&args[0]) % ARM_NUM_ARMS;
            int   x    = kiki_arg_to_int  (&args[1]);
            int   y    = kiki_arg_to_int  (&args[2]);
            int   a    = kiki_arg_to_int  (&args[3]);
            int   l    = kiki_arg_to_int  (&args[4]);
            Arm* arm   = arms[i];
            arm_endpoint_location_t* loc;
            loc = arm_get_predefined_location(arm, (arm_predefined_location_t)l);
            if(loc==NULL) interface_send_servo_error(SERVO_ERROR_RANGE);
            else
              {
                loc->x=x;
                loc->y=y;
                loc->a=a;
                interface_send_aok();
              }
          }
        break;
      
      /*---------------------------------------------------*/
      case kiki_hash_arm_strike:
        if(num_args == 5)
          {
            int   i    = kiki_arg_to_int  (&args[0]);
            int   x    = kiki_arg_to_int  (&args[1]);
            int   y    = kiki_arg_to_int  (&args[2]);
            int   a    = kiki_arg_to_int  (&args[3]);
            float spd  = kiki_arg_to_float(&args[4]);
            Arm* arm   = arms[i % ARM_NUM_ARMS];
            arm_endpoint_location_t loc = {x, y, a};
            error = arm_strike_anywhere(arm, &loc, spd);
            interface_send_servo_error(error);
          }
        break;
     
      /*---------------------------------------------------*/
      case kiki_hash_bass:
        if(num_args == 2)
          {
            int   i    = kiki_arg_to_int  (&args[0]);
            float spd  = kiki_arg_to_float(&args[1]);
            Arm* arm   = arms[i % ARM_NUM_ARMS];
            error = arm_strike_predefined(arm, ARM_LOCATION_BASS, spd);
            interface_send_servo_error(error);
          }
        break;

      /*---------------------------------------------------*/
      case kiki_hash_tone:
        if(num_args == 2)
          {
            int   i    = kiki_arg_to_int  (&args[0]);
            float spd  = kiki_arg_to_float(&args[1]);
            Arm* arm   = arms[i % ARM_NUM_ARMS];
            error = arm_strike_predefined(arm, ARM_LOCATION_TONE, spd);
            interface_send_servo_error(error);
          }
        break;
      
      /*---------------------------------------------------*/
      case kiki_hash_slap:
        if(num_args == 2)
          {
            int   i    = kiki_arg_to_int  (&args[0]);
            float spd  = kiki_arg_to_float(&args[1]);
            Arm* arm   = arms[i % ARM_NUM_ARMS];
            error = arm_strike_predefined(arm, ARM_LOCATION_SLAP , spd);
            interface_send_servo_error(error);
          }
        break;

      /*---------------------------------------------------*/
      case kiki_hash_thwack:
        if(num_args == 2)
          {
            int   i       = kiki_arg_to_int  (&args[0]);
            float spd     = kiki_arg_to_float(&args[1]);
            Solenoid* sol = solenoids[i % SOLENOID_NUM_SOLENOIDS];
            solenoid_strike(sol, spd);
            interface_send_aok();
          }
        break;
        
      /*---------------------------------------------------*/
      default: break;
    }
}

