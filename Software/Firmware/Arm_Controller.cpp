#include <math.h>
#include "Arm_Controller.h"
#include "Memory_Map.h"
#include "Arduino.h"

#include "Kiki_Communication.h" //for testing only

#define ARM_QUEUE_SIZE 10

/*----------------------------------------------------*/
#define constrain(x, min, max) ((x) > (max)) ? (max) : ((x) < (min)) ? (min) : (x)
//#define boundary_check_inclusive(x, min, max) ((x) > (max)) ? (FAILURE) : ((x) < (min)) ? (FAILURE) : (SUCCESS)

servo_error_t arm_endpoint_location_to_servo_angles(Arm* self, arm_endpoint_location_t* endpoint, float returned_angles[ARM_NUM_SERVOS_PER_ARM]);
void          arm_servo_angles_to_endpoint_location(Arm* self, float angles[ARM_NUM_SERVOS_PER_ARM], arm_endpoint_location_t* returned_endpoint);
servo_error_t arm_get_current_servo_angles(Arm* self, float returned_angles[ARM_NUM_SERVOS_PER_ARM]);
/* returns the number of milliseconds*/
float         arm_get_speeds_from_servo_angles(Arm* self, float start_angles   [ARM_NUM_SERVOS_PER_ARM], 
                                                          float end_angles     [ARM_NUM_SERVOS_PER_ARM], 
                                                          float returned_speeds[ARM_NUM_SERVOS_PER_ARM], 
                                                          float speed_coefficient);
/*this one uses the supplied speeds*/
servo_error_t arm_set_servo_angles_and_speeds(Arm* self, float target_servo_angles[ARM_NUM_SERVOS_PER_ARM], float servo_speeds[ARM_NUM_SERVOS_PER_ARM]);
/*this one calculates the speeds based on the current position*/
servo_error_t arm_set_servo_angles(Arm* self, float target_servo_angles[ARM_NUM_SERVOS_PER_ARM], float speed_coefficient, float* returned_milliseconds);

arm_endpoint_location_t* arm_get_predefined_location(Arm* self, arm_predefined_location_t endpoint);



//int           arm_fuzzy_is_equal(arm_endpoint_location_t* loc1, arm_endpoint_location_t* loc2, arm_float_t tolerance);

/*----------------------------------------------------*/
typedef uint32_t arm_ctrl_time_t;
/*----------------------------------------------------*/
typedef enum arm_ctrl_queued_event_state_enum
{
  ARM_STATE_INACTIVE,
  ARM_DELAYING_BEFORE_RECOIL,
  ARM_STATE_RECOILING,
  ARM_STATE_STRIKING,
}arm_ctrl_queueud_event_state_t;

/*----------------------------------------------------*/
typedef struct arm_ctrl_queued_event_struct
{
  arm_ctrl_time_t impact_time;
  float loudness;
  float strike_angles[ARM_NUM_SERVOS_PER_ARM];
}arm_ctrl_queued_event_t;

/*----------------------------------------------------*/
typedef struct arm_ctrl_queued_struct
{
  int num_entries;
  arm_ctrl_queued_event_t* head;
  arm_ctrl_queued_event_t* tail;
  arm_ctrl_queued_event_t  buffer[ARM_QUEUE_SIZE];
}arm_ctrl_queue_t;

/*----------------------------------------------------*/
#pragma pack(1)
struct opaque_arm_struct
{
  arm_endpoint_location_t* locations;//[ARM_NUM_LOCATIONS];
  uint8_t servo_ids[ARM_NUM_SERVOS_PER_ARM];
  arm_millimeters_t segment_lengths[ARM_NUM_SERVOS_PER_ARM];
  arm_ctrl_queueud_event_state_t state;
  
  arm_ctrl_queue_t queue;
  float recoil_angles[ARM_NUM_SERVOS_PER_ARM];
  float recoil_speeds[ARM_NUM_SERVOS_PER_ARM];
  float strike_speeds[ARM_NUM_SERVOS_PER_ARM];
  int   pause_duration, recoil_duration, strike_duration;
};
#pragma pack(pop)

/*----------------------------------------------------*/
Arm _arm_right_arm = {NULL, 
                     {ARM_RIGHT_ARM_PROXIMAL_SERVO_ID, ARM_RIGHT_ARM_MIDDLE_SERVO_ID, ARM_RIGHT_ARM_DISTAL_SERVO_ID},
                     {ARM_RIGHT_SEGMENT_1_LENGTH, ARM_RIGHT_SEGMENT_2_LENGTH, ARM_RIGHT_SEGMENT_3_LENGTH},
                     ARM_STATE_INACTIVE
                     };
Arm _arm_left_arm  = {NULL, 
                     {ARM_LEFT_ARM_PROXIMAL_SERVO_ID , ARM_LEFT_ARM_MIDDLE_SERVO_ID , ARM_LEFT_ARM_DISTAL_SERVO_ID},
                     {ARM_LEFT_SEGMENT_1_LENGTH , ARM_LEFT_SEGMENT_2_LENGTH , ARM_LEFT_SEGMENT_3_LENGTH},
                     ARM_STATE_INACTIVE
                     };

Arm* arm_right_arm = &_arm_right_arm;
Arm* arm_left_arm  = &_arm_left_arm;
Arm* arms[ARM_NUM_ARMS] = {arm_right_arm, arm_left_arm};

/*----------------------------------------------------*/
servo_error_t arm_endpoint_location_to_servo_angles(Arm* self, arm_endpoint_location_t* endpoint, float returned_angles[ARM_NUM_SERVOS_PER_ARM])
{
  arm_float_t p1x , p2x , p3x ;             //servo positions
  arm_float_t p1y , p2y , p3y , p4y;        //servo positions
  arm_float_t a0  , a1  , a2  , a3  , a4  ; //angles (theta)
  arm_float_t l0  , l1  , l2  , l3  , l4  ; //segment lengths
  arm_float_t l0_2, l1_2, l2_2, l3_2, l4_2; //lengths squared
  arm_float_t H;
  arm_float_t lambda1 = 1, lambda2 = 1;
  arm_float_t _x, _y, x, y, a;
  
  arm_endpoint_location_t* offset = &(self->locations[ARM_LOCATION_OFFSET]);
  
  x = endpoint->x + offset->x - ARM_RIGHT_LENGTH;
  y = endpoint->y + offset->y;
  a = (endpoint->a  + offset->a) / 1000.0;
  
  if(x < ARM_MIN_X) return SERVO_ERROR_RANGE;
  
  l0     = self->segment_lengths[0];
  l1     = self->segment_lengths[1];
  l2     = self->segment_lengths[2];;
  l0_2   = l0 * l0;
  l1_2   = l1 * l1;
  l2_2   = l2 * l2;
  p2x    = x - l2 * cos(a);
  p2y    = y + l2 * sin(a);
  l3_2   = (p2x*p2x) + (p2y*p2y);
  l3     = sqrt(l3_2); 
  if     (l3 > l0 + l1) return SERVO_ERROR_RANGE;
  //l3_2   = l3 * l3;
  if(l3 != 0) lambda1 = 0.5 + (l0_2 - l1_2) / (2 * l3_2);
  p3x    = p2x * lambda1;
  p3y    = p2y * lambda1;
  //a3     = acos( ((l1_2) + (l3_2) - (l0_2)) / (2 * l1 * l3) );
  //H      = l1 * sin(a3);
  a3     = ((l1_2) + (l3_2) - (l0_2)) / (2 * l1 * l3);
  H      = l1 * sqrt(1 - (a3*a3));
  //a4     = acos(p2x / l3);
  //p1y    = p3y + H * cos(a4);
  a4     = p2x / l3;
  p1y    = p3y + H * a4;
  
  if(p2y >= 0) H = -H;
  //p1x    = p3x + H * sin(a4);
  p1x    = p3x + H * sqrt(1-(a4*a4));
  _x     = x - p1x;
  _y     = y - p1y;
  l4     = sqrt(_x*_x + _y*_y);
  l4_2   = l4 * l4;
  a0     = asin(p1y / l0) + M_PI;
  a1     = acos( ((l0_2) + (l1_2) - (l3_2)) / (2 * l0 * l1) );
  a2     = (2 * M_PI) - acos( ((l1_2) + (l2_2) - (l4_2)) / (2 * l1 * l2) );
  if     (isnan(a0) || isnan(a1) || isnan(a2)) return SERVO_ERROR_RANGE;
  if(l4 != 0) lambda2 = 0.5 + (l1_2 - l2_2) / (2 * l4_2);
  p4y    = p1y + _y * lambda2;
  
  if(x > p1x)
    if(p2y > p4y)
      a2 = (M_PI * 2) - a2;
  
  if(x < p1x)
    if(p2y < p4y)
      a2 = (M_PI * 2) - a2;
  
  if(p1x < 0)
    a0 = M_PI * 3 - a0;
    
  returned_angles[0] = a0;
  returned_angles[1] = a1;
  returned_angles[2] = a2;
  
  return SERVO_NO_ERROR;
}

/*----------------------------------------------------*/
void arm_servo_angles_to_endpoint_location(Arm* self, float servo_angles[ARM_NUM_SERVOS_PER_ARM], arm_endpoint_location_t* returned_endpoint)
{ 
  float servo_angle; //servo_float_t, not arm_float_t
  float returned_angle = 0;
  int i;
  
  arm_endpoint_location_t* offset = &(self->locations[ARM_LOCATION_OFFSET]);
  returned_endpoint->x = -(offset->x) + ARM_RIGHT_LENGTH;
  returned_endpoint->y = -(offset->y);
  returned_endpoint->a = -(offset->a);
 
  for(i=0; i<ARM_NUM_SERVOS_PER_ARM; i++)
    {
      servo_angle = servo_angles[i] - M_PI;
      
      returned_endpoint->x += self->segment_lengths[i] * cos(servo_angle + returned_angle) + 0.5;
      returned_endpoint->y += self->segment_lengths[i] * sin(servo_angle + returned_angle) + 0.5;
      returned_angle += servo_angle;
    }
 
  while(returned_angle < -M_PI) returned_angle += (M_PI * 2);
  while(returned_angle >  M_PI) returned_angle -= (M_PI * 2);
  returned_endpoint->a += returned_angle * 1000 + 0.5;
  returned_endpoint->a *= -1;
}

/*----------------------------------------------------*/
servo_error_t arm_get_current_servo_angles(Arm* self, float returned_angles[ARM_NUM_SERVOS_PER_ARM])
{
  /*servo_error_t*/ uint16_t result = SERVO_NO_ERROR;
  int i;
  for(i=0; i<ARM_NUM_SERVOS_PER_ARM; i++)
    {
      result = servo_get(self->servo_ids[i], SERVO_REGISTER_PRESENT_ANGLE, returned_angles++, 10);
      if(result != SERVO_NO_ERROR)
        break;
    }
   
  return (servo_error_t)result;
}

/*----------------------------------------------------*/
/* returns num milliseconds */
float arm_get_speeds_from_servo_angles(Arm* self, float start_angles   [ARM_NUM_SERVOS_PER_ARM], 
                                                  float end_angles     [ARM_NUM_SERVOS_PER_ARM], 
                                                  float returned_speeds[ARM_NUM_SERVOS_PER_ARM], 
                                                  float speed_coefficient)
{
  //calculate distances
  int i;
  float milliseconds = 0, ms;
  float farthest_distance = -1;
  float  distances[ARM_NUM_SERVOS_PER_ARM];
  float* s = returned_speeds;
  float* d = distances;
  
  if(speed_coefficient > 1) speed_coefficient = 1;

  for(i=0; i<ARM_NUM_SERVOS_PER_ARM; i++)
    {
      *d = end_angles[i] - start_angles[i];
      *d = fabs(*d);
      if(*d > farthest_distance)
        farthest_distance = *d;
      d++;
    }
    
  d = distances;
  for(i=0; i<ARM_NUM_SERVOS_PER_ARM; i++)
    {
      if(speed_coefficient < 0)
        servo_get(self->servo_ids[i], SERVO_REGISTER_MOVING_SPEED, s, 10);
      else if(farthest_distance > 0)
        *s = *d * SERVO_MAXIMUM_MOVING_SPEED * speed_coefficient / farthest_distance;

      s++; d++;
    }

  if (farthest_distance > 0)
    {
      s = returned_speeds; d = distances;
      for(i=0; i<ARM_NUM_SERVOS_PER_ARM; i++)
        {
          if(*s > 0)
            {
              ms = *d / *s;
              if(ms > milliseconds)
                milliseconds = ms;
            }
          d++; s++;
        }
    }

  return milliseconds * 1000;
}

/*----------------------------------------------------*/
servo_error_t arm_set_servo_angles_and_speeds(Arm* self, float target_servo_angles[ARM_NUM_SERVOS_PER_ARM], float servo_speeds[ARM_NUM_SERVOS_PER_ARM])
{
  /*servo_error_t*/ uint16_t result = SERVO_NO_ERROR;
  
 
  int i;

/*
  for(i=0; i<ARM_NUM_SERVOS_PER_ARM; i++)
    result |= servo_set(self->servo_ids[i], SERVO_REGISTER_MOVING_SPEED, servo_speeds[i], 10);
  
  if(result == SERVO_NO_ERROR)
    for(i=0; i<ARM_NUM_SERVOS_PER_ARM; i++)
      result |= servo_prepare(self->servo_ids[i], SERVO_REGISTER_GOAL_ANGLE, target_servo_angles[i], 10);

  if(result == SERVO_NO_ERROR)
    result = servo_do_prepared(SERVO_BROADCAST_ID, 10);
 */   
  
   float values[6];
   values[0] = target_servo_angles[0];
   values[1] = servo_speeds       [0];
   values[2] = target_servo_angles[1];
   values[3] = servo_speeds       [1];
   values[4] = target_servo_angles[2];
   values[5] = servo_speeds       [2];
 
  result = servo_set_multiple(self->servo_ids, SERVO_REGISTER_GOAL_ANGLE, values, ARM_NUM_SERVOS_PER_ARM, 2);
 
  return (servo_error_t)result;  
}
/*----------------------------------------------------*/
servo_error_t arm_set_servo_angles(Arm* self, float target_servo_angles[ARM_NUM_SERVOS_PER_ARM], float speed_coefficient, float* returned_milliseconds)
{
  /*servo_error_t*/ uint16_t result;
  int i;
  float current_servo_angles[ARM_NUM_SERVOS_PER_ARM];
  float servo_speeds[ARM_NUM_SERVOS_PER_ARM];

  result = arm_get_current_servo_angles(self, current_servo_angles);
  
  if(result == SERVO_NO_ERROR)
    {
      *returned_milliseconds = arm_get_speeds_from_servo_angles(self, current_servo_angles, target_servo_angles, servo_speeds, speed_coefficient);
      result = arm_set_servo_angles_and_speeds(self, target_servo_angles, servo_speeds);
    }

  return (servo_error_t)result;  
}


/*----------------------------------------------------*/
servo_error_t arm_set_current_location(Arm* self, arm_endpoint_location_t* endpoint, float speed_coefficient, float* returned_milliseconds)
{
  /*servo_error_t*/ uint16_t result;
  float target_servo_angles[ARM_NUM_SERVOS_PER_ARM];
 
  result = arm_endpoint_location_to_servo_angles(self, endpoint, target_servo_angles);
  if(result == SERVO_NO_ERROR)
    result = arm_set_servo_angles(self, target_servo_angles, speed_coefficient, returned_milliseconds);
      
  return (servo_error_t)result;
}

/*----------------------------------------------------*/
servo_error_t arm_set_endpoint_location_predefined(Arm* self, arm_predefined_location_t endpoint,  float speed_coefficient, float* returned_milliseconds)
{ 
  servo_error_t result = SERVO_ERROR_RANGE;
  arm_endpoint_location_t* loc = arm_get_predefined_location(self, endpoint);
  if(loc != NULL)
    result = arm_set_current_location(self, loc, speed_coefficient, returned_milliseconds);
  return result;
}

/*----------------------------------------------------*/
servo_error_t arm_get_current_location(Arm* self, arm_endpoint_location_t  *endpoint)
{
  /*servo_error_t*/ uint16_t result = SERVO_NO_ERROR;
  
  float servo_angles[ARM_NUM_SERVOS_PER_ARM]; //servo_float_t, not arm_float_t
  
  result = arm_get_current_servo_angles(self, servo_angles);
  if(result == SERVO_NO_ERROR)
    arm_servo_angles_to_endpoint_location(self, servo_angles, endpoint);
 
  return (servo_error_t)result;
}

/*----------------------------------------------------*/
servo_error_t   arm_strike_anywhere (Arm* self, arm_endpoint_location_t*  strike_location, arm_float_t loudness)
{
  int   result = SERVO_NO_ERROR;
  
  if(self->queue.num_entries < ARM_QUEUE_SIZE)
    {
      float servo_strike_angles[ARM_NUM_SERVOS_PER_ARM];
      result = arm_endpoint_location_to_servo_angles(self, strike_location, servo_strike_angles);
      
      if(result == SERVO_NO_ERROR)
        {
          int i;
          arm_ctrl_queued_event_t *e = self->queue.head++;
          if((self->queue.head - self->queue.buffer) >= ARM_QUEUE_SIZE)
            self->queue.head = self->queue.buffer;
            
          e->loudness = loudness;
          for(i=0; i<ARM_NUM_SERVOS_PER_ARM; i++)
            e->strike_angles[i] = servo_strike_angles[i];
            
          e->impact_time = *((int32_t*)(MMAP_ADDR_CACHE_START + MMAP_ADDR_STRIKE_DELAY_MILLIS)) + millis();
          self->queue.num_entries++;
        }
    }
    
  return (servo_error_t)result;
}

/*----------------------------------------------------*/
/*
servo_error_t   _arm_strike (Arm* self, arm_endpoint_location_t*  strike_location, arm_float_t loudness)
{
  servo_error_t result;
  
  int TOTAL_STRIKE_TIME = 500; //milliseconds
  
  loudness = constrain(loudness, 0.0, 1.0);

  float recoiled_servo_angles[ARM_NUM_SERVOS_PER_ARM];
  float strike_servo_angles[ARM_NUM_SERVOS_PER_ARM];
  float servo_speeds[ARM_NUM_SERVOS_PER_ARM];
  float recoil_millisecs, pause_millisecs, strike_millisecs;
  float speed_coefficient =0.25+(loudness*0.75);
  
  arm_endpoint_location_t recoiled_location = 
    {
      strike_location->x,
      strike_location->y,
      strike_location->a,
    };
  
  if(result == SERVO_NO_ERROR)
  result = arm_endpoint_location_to_servo_angles(self, &recoiled_location, recoiled_servo_angles);
  
  if(result == SERVO_NO_ERROR)
    result = arm_endpoint_location_to_servo_angles(self, strike_location, strike_servo_angles  );
  
  if(result == SERVO_NO_ERROR)
    {
      float recoil_increment = 0.06 + (loudness * 0.06);
      recoiled_servo_angles[0] += recoil_increment;
      recoiled_servo_angles[1] += recoil_increment;
      recoiled_servo_angles[2] += recoil_increment;
      result = arm_set_servo_angles(self, recoiled_servo_angles, 1, &recoil_millisecs);
    }
    
  if(result == SERVO_NO_ERROR)
    {
      _delay_ms(recoil_millisecs + 0.5);
      strike_millisecs = arm_get_speeds_from_servo_angles(self, recoiled_servo_angles, strike_servo_angles, servo_speeds, speed_coefficient);
      pause_millisecs = TOTAL_STRIKE_TIME - strike_millisecs - recoil_millisecs;
      if(pause_millisecs > 0)
        _delay_ms(pause_millisecs + 0.5);
      result = arm_set_servo_angles_and_speeds(self, strike_servo_angles, servo_speeds);
    }
  
  if(result == SERVO_NO_ERROR)
    {
      //_delay_ms(strike_millisecs + 0.5);
      //servo_set(self->servo_ids[2], SERVO_REGISTER_TORQUE_ENABLE, 0, 10);
      
      arm_endpoint_location_t current_location;
      while(1)
        {
          result = arm_endpoint_location(self, &current_location);
          if(result != SERVO_NO_ERROR) 
            break;
          current_location.a -= strike_location->a;
          current_location.a = abs(current_location.a);
          if(current_location.a < 30)
            {
              servo_set(self->servo_ids[2], SERVO_REGISTER_TORQUE_ENABLE, 0, 10);
              break;
            }
        }
      
    }
  return (servo_error_t)result;
}
*/
/*----------------------------------------------------*/
servo_error_t   arm_strike_predefined       (Arm* self, arm_predefined_location_t predefined_index, arm_float_t loudness)
{
  servo_error_t result = SERVO_ERROR_RANGE;
  arm_endpoint_location_t* loc =  arm_get_predefined_location(self, predefined_index);
  if(loc != NULL)
    result = arm_strike_anywhere(self, loc, loudness);

  return result;  
}

/*----------------------------------------------------*/
arm_endpoint_location_t* arm_get_predefined_location     (Arm* self, arm_predefined_location_t predefined_index)
{
  arm_endpoint_location_t* loc = NULL;
  if((predefined_index < ARM_NUM_LOCATIONS) && (predefined_index >= 0))
     loc = self->locations + predefined_index;
    //loc = &(self->locations[predefined_index]);
    
  return loc;
}

/*----------------------------------------------------*/
//control thread below here
/*----------------------------------------------------*/
#define ARM_CTRL_QUEUE_RATE 1
#define SET_BIT(reg, name) reg |=  (1<<name)
#define CLR_BIT(reg, name) reg &= ~(1<<name)
  
void arm_init()
{
  int i;
  for(i=0; i<ARM_NUM_ARMS; i++)
    {
      arms[i]->queue.head = arms[i]->queue.tail = arms[i]->queue.buffer;
      arms[i]->queue.num_entries = 0;
    }
  arm_left_arm->locations  = (arm_endpoint_location_t*)(MMAP_ADDR_CACHE_START + MMAP_ADDR_L_ARM_OBJECT);
  arm_right_arm->locations = (arm_endpoint_location_t*)(MMAP_ADDR_CACHE_START + MMAP_ADDR_R_ARM_OBJECT);
/*
  CLR_BIT( TCCR1A, WGM10  ); // CTC mode with TOP = OCR2A
  CLR_BIT( TCCR1A, WGM11  ); //
  SET_BIT( TCCR1B, WGM12  ); //
  CLR_BIT( TCCR1B, WGM13  ); //
  SET_BIT( TCCR1B, CS10   ); // no prescaler
  CLR_BIT( TCCR1B, CS11   ); // 
  CLR_BIT( TCCR1B, CS12   ); // 
  SET_BIT( TIMSK1, OCIE1A ); // output compare match interrupt

  OCR1A = (F_CPU / 1000) * ARM_CTRL_QUEUE_RATE; //timer TOP
*/
}


/*----------------------------------------------------*/

//ISR(TIMER1_COMPA_vect)
/*
void arm_ctrl_run_loop()
{
  uint32_t now;
  int i;
  
  for(i=0; i<ARM_NUM_ARMS; i++)
    {
      Arm* self = arms[i];
      if(self->queue.num_entries > 0)
        {
          arm_ctrl_queued_event_t *e = self->queue.tail;
          float current_angles[ARM_NUM_SERVOS_PER_ARM];    //this is not needed in every execution path
          float speed_coefficient = 0.25 + (e->loudness * 0.75); //nor is this
          
          if(self->state == ARM_STATE_INACTIVE)
            {
              float recoil_increment = 0.06 + (e->loudness * 0.6);
              int j;
              for(j=0; j<ARM_NUM_SERVOS_PER_ARM; j++)
                self->recoil_angles[j] = e->strike_angles[j];
                
              self->recoil_angles[2] += recoil_increment;
              
              if(arm_get_current_servo_angles(self, current_angles) != SERVO_NO_ERROR) return;
              
              self->recoil_duration = arm_get_speeds_from_servo_angles(self, current_angles, self->recoil_angles   , self->recoil_speeds, 1);
              self->strike_duration = arm_get_speeds_from_servo_angles(self, self->recoil_angles , e->strike_angles, self->strike_speeds, speed_coefficient);
              self->pause_duration  = ARM_STRIKE_DELAY - (self->recoil_duration + self->strike_duration);
              self->state = ARM_DELAYING_BEFORE_RECOIL;
            }
          
          now = millis();
          
          switch(self->state)
            {
              case ARM_DELAYING_BEFORE_RECOIL:
                if((now + self->recoil_duration + self->strike_duration) >= (e->impact_time))
                  {
                    arm_set_servo_angles_and_speeds(self, self->recoil_angles, self->recoil_speeds);
                    self->state = ARM_STATE_RECOILING;
                  }
                break;
                
              case ARM_STATE_RECOILING:
                if(arm_get_current_servo_angles(self, current_angles) != SERVO_NO_ERROR) return;  
                self->strike_duration = arm_get_speeds_from_servo_angles(self, current_angles , e->strike_angles, self->strike_speeds, speed_coefficient);
                
                Serial.println((int32_t)(e->impact_time - now));
                
                if((self->strike_duration + now) >= e->impact_time)
                  {
                    arm_set_servo_angles_and_speeds(self, e->strike_angles, self->strike_speeds);
                    self->state = ARM_STATE_STRIKING;
                  }
                break;              
              
              case ARM_STATE_STRIKING:
                {
                //float angle;
                //servo_set(self->servo_ids[2], SERVO_REGISTER_TORQUE_ENABLE, 0, 10);
                
                if((e->impact_time+100) < now)
                  {
                    servo_set(self->servo_ids[2], SERVO_REGISTER_TORQUE_ENABLE, 0, 10);
                    //servo_set(SERVO_BROADCAST_ID, SERVO_REGISTER_TORQUE_ENABLE, 0, 10);
                    
                    self->queue.tail++;
                    if((self->queue.tail - self->queue.buffer) >= ARM_QUEUE_SIZE)
                      self->queue.tail = self->queue.buffer;
                    self->queue.num_entries--;
                    
                    self->state = ARM_STATE_INACTIVE;
                  }
                break;
                
              default: break;
            }
          }
        }
    }
}
*/


void arm_ctrl_run_loop()
{
  int i;
  
  for(i=0; i<ARM_NUM_ARMS; i++)
    {
      Arm* self = arms[i];
      if(self->queue.num_entries > 0)
        {
          arm_ctrl_queued_event_t *e = self->queue.tail;
          float current_angles[ARM_NUM_SERVOS_PER_ARM];    //this is not needed in every execution path
          float speed_coefficient = 1.0;//0.25 + (e->loudness * 0.75); //nor is this
          
          if(self->state == ARM_STATE_INACTIVE)
            {
              float recoil_increment = 0.36 + (e->loudness * 0.3);
              int j;
              for(j=0; j<ARM_NUM_SERVOS_PER_ARM; j++)
                self->recoil_angles[j] = e->strike_angles[j];
                
              self->recoil_angles[2] += recoil_increment;
              
              if(arm_get_current_servo_angles(self, current_angles) != SERVO_NO_ERROR) continue;
              
              self->recoil_duration = arm_get_speeds_from_servo_angles(self, current_angles, self->recoil_angles, self->recoil_speeds, 1);
              arm_set_servo_angles_and_speeds(self, self->recoil_angles, self->recoil_speeds);
              
              //is the following line necessary?
              self->strike_duration = arm_get_speeds_from_servo_angles(self, self->recoil_angles , e->strike_angles, self->strike_speeds, speed_coefficient);
              //self->pause_duration  = ARM_STRIKE_DELAY - (self->recoil_duration + self->strike_duration);
              //self->state = ARM_DELAYING_BEFORE_RECOIL;
              self->state = ARM_STATE_RECOILING;
              
              //kiki_send_message("impact_time %i" , e->impact_time);
              //kiki_send_message("recoiling %i"    , millis());
            }
          
          
          if(self->state == ARM_STATE_RECOILING)
            {
              if(arm_get_current_servo_angles(self, current_angles) != SERVO_NO_ERROR) continue;  
              self->strike_duration = arm_get_speeds_from_servo_angles(self, current_angles , e->strike_angles, self->strike_speeds, speed_coefficient);
                
                //kiki_send_message("loop %i", (int)(e->impact_time - now));
                
              if((self->strike_duration + millis()) >= (e->impact_time - 100))
                {
                  arm_set_servo_angles_and_speeds(self, e->strike_angles, self->strike_speeds);
                  self->state = ARM_STATE_STRIKING;
                  //kiki_send_message("striking %i", millis());
                }            
            }
          
          if(self->state == ARM_STATE_STRIKING)
            {    
                //float angle;
                //servo_set(self->servo_ids[2], SERVO_REGISTER_TORQUE_ENABLE, 0, 10);
                
                if((e->impact_time) < millis())
                  {
                    servo_set(self->servo_ids[2], SERVO_REGISTER_TORQUE_ENABLE, 0, 10);
                    //servo_set(SERVO_BROADCAST_ID, SERVO_REGISTER_TORQUE_ENABLE, 0, 10);
                    
                    self->queue.tail++;
                    if((self->queue.tail - self->queue.buffer) >= ARM_QUEUE_SIZE)
                      self->queue.tail = self->queue.buffer;
                    self->queue.num_entries--;
                    
                    //kiki_send_message("impact %i", millis());
                    
                    self->state = ARM_STATE_INACTIVE;
                  }
            }
        }
    }
}
