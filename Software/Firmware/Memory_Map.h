#ifndef MEMORY_MAP
#define MEMORY_MAP

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "Constants.h"

extern const unsigned MMAP_ADDR_CACHE_START;

/*---------------------------------------------------*/
typedef int mmap_success_t;

/*---------------------------------------------------*/

void           mmap_init();
int32_t        mmap_get (mmap_address_t addr);
mmap_success_t mmap_set (mmap_address_t addr, int32_t val);
void           mmap_undo();
void           mmap_save();
void           mmap_factory_reset();


#ifdef __cplusplus
}
#endif

#endif //MEMORY_MAP
