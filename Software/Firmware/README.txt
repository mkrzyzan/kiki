This code is intended to run on Arduino Teensy 3.1, compiled with the Arduino IDE.

In order to compile this code, some items in the Teensy library need to be modified. This is accomplished by moving the files in 

./Teensy_Replacements/

into 

/Applications/Arduino.app/Contents/Resources/Java/hardware/teensy/cores/teensy3/

thereby replacing the files there.

Note that some of the files in this folder are symbolic links to files in 

../Kiki_Communication_Framework/

