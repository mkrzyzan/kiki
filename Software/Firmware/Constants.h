#ifndef __KIKI_COMMUNICATION_CONSTANTS__
#define __KIKI_COMMUNICATION_CONSTANTS__ 1 


#include "Mmap_Constants.h"

/*----------------------------------------------------*/
#define INTERFACE_BASS_MIDI_NOTE          MIDI_PITCH_C_4
#define INTERFACE_TONE_MIDI_NOTE          MIDI_PITCH_D_4
#define INTERFACE_SLAP_MIDI_NOTE          MIDI_PITCH_E_4
#define INTERFACE_SOLENOID_MIDI_NOTE      MIDI_PITCH_F_4
#define INTERFACE_BELL_MIDI_NOTE          MIDI_PITCH_F_6
#define INTERFACE_ARM_INDEX_MIDI_OFFSET   12 //the next higher octave for each arm, i.e. MIDI_PITCH_C_5, etc.

/*----------------------------------------------------*/
typedef enum arm_predefined_location_enum
{
  ARM_LOCATION_OFFSET = 0,
  ARM_LOCATION_REST,
  ARM_LOCATION_BASS,
  ARM_LOCATION_TONE,
  ARM_LOCATION_SLAP,
  ARM_NUM_LOCATIONS,
}arm_predefined_location_t;

/*----------------------------------------------------*/
typedef enum arm_index_enum
{
  ARM_INDEX_LEFT,
  ARM_INDEX_RIGHT,
}arm_index_t;


#endif //__KIKI_COMMUNICATION_CONSTANTS__

