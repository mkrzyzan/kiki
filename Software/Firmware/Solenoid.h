#define SOLENOID_NUM_SOLENOIDS         3

typedef struct opaque_solenoid_struct Solenoid;

extern Solenoid* solenoid_right_solenoid;
extern Solenoid* solenoid_left_solenoid ;
extern Solenoid* solenoid_cowbell ;
extern Solenoid* solenoids[SOLENOID_NUM_SOLENOIDS];

void solenoid_init();
void solenoid_strike(Solenoid* self, float loudness);
void solenoid_ctrl_run_loop();
