#include "MIDI_Parser.h"
#include "Interface.h"
#include "Dynamixel_Servo.h"
#include "Memory_Map.h"
#include "Arm_Controller.h"
#include "Solenoid.h"
#include "EEPROM.h"
#include "Kiki_Communication.h"

//unused port on board
//#define HALF_DUPLEX_PIN 6?
//#define servo_serial Serial3

#define HALF_DUPLEX_PIN 11
#define servo_serial Serial2

//#define FSR_INTERRUPT_PIN 20
//uint32_t fsr_interrupt_time = 0;
//bool     fsr_interrupt_recd = false;

/*---------------------------------------------------*/
void setup(void)
{  
  mmap_init();
  interface_init();
  
  //pinMode(13, OUTPUT);

  servo_init(&servo_serial, HALF_DUPLEX_PIN, mmap_get(MMAP_ADDR_UART_BAUD));
  solenoid_init();
  arm_init();
  
  //pinMode(FSR_INTERRUPT_PIN, INPUT);
  //attachInterrupt(FSR_INTERRUPT_PIN, fsr_triggered, RISING);
}

/*---------------------------------------------------*/
void loop(void)
{
  interface_run_loop();
  arm_ctrl_run_loop();
  solenoid_ctrl_run_loop();
  /*
  if(fsr_interrupt_recd)
    {
      kiki_send_message("fsr %i\n", (int)fsr_interrupt_time);
      fsr_interrupt_recd = false;   
    }
    */
}

/*---------------------------------------------------*/
/*
void fsr_triggered()
{
  fsr_interrupt_time = millis();
  fsr_interrupt_recd = true;
}
*/
