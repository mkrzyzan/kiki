#include "Dynamixel_Servo.h"
#include "Constants.h"

#define ARM_NUM_SERVOS_PER_ARM 3
#define ARM_STRIKE_DELAY       1000 //ms

#define ARM_MIN_X                      500

#define ARM_RIGHT_ARM_PROXIMAL_SERVO_ID 10
#define ARM_RIGHT_ARM_MIDDLE_SERVO_ID   11
#define ARM_RIGHT_ARM_DISTAL_SERVO_ID   12

#define ARM_LEFT_ARM_PROXIMAL_SERVO_ID  20
#define ARM_LEFT_ARM_MIDDLE_SERVO_ID    21
#define ARM_LEFT_ARM_DISTAL_SERVO_ID    22

//all lengths in millimeters and angles in milliradians
#define ARM_RIGHT_HAND_LENGTH                       10
#define ARM_RIGHT_HAND_BASE_TO_PALM_LENGTH          10
#define ARM_RIGHT_PALM_TO_FINGERTIPS_LENGTH (\
        ARM_RIGHT_HAND_LENGTH -\
        ARM_RIGHT_HAND_BASE_TO_PALM_LENGTH)
#define ARM_RIGHT_SEGMENT_1_LENGTH                 197
#define ARM_RIGHT_SEGMENT_2_LENGTH                 197
#define ARM_RIGHT_SEGMENT_3_LENGTH_TO_HAND_BASE    318
#define ARM_RIGHT_SEGMENT_3_LENGTH_TO_PALM (\
        ARM_RIGHT_SEGMENT_3_LENGTH_TO_HAND_BASE +\
        ARM_RIGHT_HAND_BASE_TO_PALM_LENGTH)
#define ARM_RIGHT_SEGMENT_3_LENGTH_TO_FINGERTIPS (\
        ARM_RIGHT_SEGMENT_3_LENGTH_TO_HAND_BASE +\
        ARM_RIGHT_HAND_BASE_TO_PALM_LENGTH)
#define ARM_RIGHT_SEGMENT_3_LENGTH ARM_RIGHT_SEGMENT_3_LENGTH_TO_PALM

#define ARM_RIGHT_LENGTH (ARM_RIGHT_SEGMENT_1_LENGTH + ARM_RIGHT_SEGMENT_2_LENGTH + ARM_RIGHT_SEGMENT_3_LENGTH)


#define ARM_LEFT_HAND_LENGTH                        10
#define ARM_LEFT_HAND_BASE_TO_PALM_LENGTH           10
#define ARM_LEFT_PALM_TO_FINGERTIPS_LENGTH (\
        ARM_LEFT_HAND_LENGTH -\
        ARM_LEFT_HAND_BASE_TO_PALM_LENGTH)
#define ARM_LEFT_SEGMENT_1_LENGTH                  197
#define ARM_LEFT_SEGMENT_2_LENGTH                  197
#define ARM_LEFT_SEGMENT_3_LENGTH_TO_HAND_BASE     318
#define ARM_LEFT_SEGMENT_3_LENGTH_TO_PALM (\
        ARM_LEFT_SEGMENT_3_LENGTH_TO_HAND_BASE +\ 
        ARM_LEFT_HAND_BASE_TO_PALM_LENGTH)
#define ARM_LEFT_SEGMENT_3_LENGTH_TO_FINGERTIPS (\
        ARM_LEFT_SEGMENT_3_LENGTH_TO_HAND_BASE +\
        ARM_LEFT_HAND_BASE_TO_PALM_LENGTH)
#define ARM_LEFT_SEGMENT_3_LENGTH ARM_LEFT_SEGMENT_3_LENGTH_TO_PALM
#define ARM_LEFT_LENGTH (ARM_LEFT_SEGMENT_1_LENGTH + ARM_LEFT_SEGMENT_2_LENGTH + ARM_LEFT_SEGMENT_3_LENGTH)


/*----------------------------------------------------*/
typedef int16_t  arm_millimeters_t;
typedef int16_t  arm_milliradians_t;
typedef double   arm_float_t;

/*----------------------------------------------------*/
#pragma pack(1)
typedef struct arm_endpoint_location_struct
{
  arm_millimeters_t  x;
  arm_millimeters_t  y;
  arm_milliradians_t a;
}arm_endpoint_location_t;
#pragma pack(pop)

/*----------------------------------------------------*/
typedef struct opaque_arm_struct Arm;

/*----------------------------------------------------*/
#define ARM_NUM_ARMS 2

extern Arm* arm_right_arm;
extern Arm* arm_left_arm ;
extern Arm* arms[ARM_NUM_ARMS];


/*----------------------------------------------------*/
void            arm_init                           ();
void            arm_ctrl_run_loop                  ();
servo_error_t   arm_set_current_location           (Arm* self, arm_endpoint_location_t*  endpoint, float speed_coefficient, float* returned_milliseconds);
servo_error_t   arm_get_current_location           (Arm* self, arm_endpoint_location_t*  returned_endpoint);

servo_error_t   arm_strike_anywhere                (Arm* self, arm_endpoint_location_t*  endpoint, arm_float_t loudness);
servo_error_t   arm_strike_predefined              (Arm* self, arm_predefined_location_t endpoint, arm_float_t loudness);
servo_error_t   arm_strike_solenoid                (int which, arm_float_t loudness);


/* if you modify the location, use the mmap_save() or mmap_undo(), if desired */
arm_endpoint_location_t* arm_get_predefined_location (Arm* self, arm_predefined_location_t predefined_index);

