#include <math.h>
#include <stdio.h>
#include <string.h> //memset
#include "../../MachineLearning/src/List.h"

#define NUM_FEATURES 6


/*-------------------------------------------------*/
typedef struct comparison_struct
{
  long double human[NUM_FEATURES];
  long double a    [NUM_FEATURES];
  long double b    [NUM_FEATURES];
  long double a_dist, b_dist, a_minus_b;
  int subject_response;
}Comparison;

/*-------------------------------------------------*/
typedef struct r_struct
{
  long double r, m, b, t;
  unsigned mask;
}R;

/*-------------------------------------------------*/
typedef struct trial_struct
{
  List* comparisons;
  List* rs;
}Trial;

Comparison* compare_new();
Comparison* compare_destroy(Comparison* self);
R* r_new();
R* r_destroy(R* self);
Trial* trial_new();
Trial* trial_destroy(Trial* self);

/*-------------------------------------------------*/
/*-------------------------------------------------*/
Comparison* compare_new()
{
  Comparison* self = calloc(1, sizeof(*self));
  return self;
}

/*-------------------------------------------------*/
Comparison* compare_destroy(Comparison* self)
{
  if(self != NULL)
    free(self);
  return (Comparison*) NULL;
}

/*-------------------------------------------------*/
R* r_new()
{
  R* self = calloc(1, sizeof(*self));
  return self;
}

/*-------------------------------------------------*/
R* r_destroy(R* self)
{
  if(self != NULL)
    free(self);
  return (R*) NULL;
}

/*-------------------------------------------------*/
Trial* trial_new()
{
  Trial* self = calloc(1, sizeof(*self));
  if(self != NULL)
    {
      self->comparisons = listNew();
      if(self->comparisons == NULL)
        return trial_destroy(self);
        
      self->rs = listNew();
      if(self->rs == NULL)
        return trial_destroy(self);
    }
  return self;  
}

/*-------------------------------------------------*/
Trial* trial_destroy(Trial* self)
{
  if(self != NULL)
    {
      listDestroy(self->comparisons, YES);
      listDestroy(self->rs, YES);
      free(self);
    }
  return (Trial*) NULL;
}

/*-------------------------------------------------*/
/*-------------------------------------------------*/
//Gets features only. No distances calculated.
void trial_read_file(Trial* self, const char* filename)
{
  FILE* f = fopen(filename, "r");
  Comparison* c = NULL;
  int ignore;
  
  if(f != NULL)
    {
      int n;
      for(;;)
        {
          c = compare_new();
          if(c == NULL) break;
          
          n = fscanf(f, "human: %Lf %Lf %Lf %Lf %Lf %Lf\r\n", c->human+0, c->human+1, c->human+2, c->human+3, c->human+4, c->human+5);
          if(n != 6) break;
          
          n = fscanf(f, "A: %Lf %Lf %Lf %Lf %Lf %Lf\r\n", c->a+0, c->a+1, c->a+2, c->a+3, c->a+4, c->a+5);
          if(n != 6) break;

          n = fscanf(f, "B: %Lf %Lf %Lf %Lf %Lf %Lf\r\n", c->b+0, c->b+1, c->b+2, c->b+3, c->b+4, c->b+5);
          if(n != 6) break;

          n = fscanf(f, "response %i: %i\r\n", &ignore, &c->subject_response);
          if(n != 2) break;
          
          //if(c->subject_response >  1) c->subject_response =  1;
          //if(c->subject_response < -1) c->subject_response = -1;

          listAppendData(self->comparisons, c, (listDataDeallocator_t)compare_destroy);
          
          c = NULL;
        }
      compare_destroy(c);
      fclose(f);
    }
}

/*-------------------------------------------------*/
//use mask to control which dimensions are included in distance calculation
//LSB of mask toggles p[0]
long double euclidian_dist(long double* p, long double* q, int n, unsigned mask)
{
  long double temp, result = 0;
  int i;
  for(i=0; i<n; i++)
    {
      if(mask & 0x01)
        {
          temp = q[i] - p[i];
          temp *= temp;
          result += temp;
        }
      mask >>= 1;
    }
  return sqrtl (result);
}

/*-------------------------------------------------*/
void trial_calculate_distances(Trial* self, unsigned mask)
{
  List* l = self->comparisons;
  if(listResetIterator(l))
    {
      Comparison* c;
      do{
          c = listCurrentData(l);
          c->a_dist = euclidian_dist(c->human, c->a, NUM_FEATURES, mask);
          c->b_dist = euclidian_dist(c->human, c->b, NUM_FEATURES, mask);
          c->a_minus_b = c->a_dist - c->b_dist;         
        }while(listAdvanceIterator(l));
    }
}

/*-------------------------------------------------*/
R* trial_calculate_r(Trial* self)
{
  R* result = r_new();
  
  if(result != NULL)
    {
      List* l = self->comparisons;
      
      int DIMENSION = 2;
      long double avg     [DIMENSION];
      long double variance[DIMENSION];
      long double std_dev [DIMENSION];
      long double temp;
      long double product     = 1;
      long double numerator   = 0;
      long double r           = 1;
      long double m, b, t;
      int d;
      int N = listCount(l);
      int ret;

      memset(avg,      0, DIMENSION * sizeof(*avg     ));
      memset(variance, 0, DIMENSION * sizeof(*variance));
      memset(std_dev,  0, DIMENSION * sizeof(*std_dev ));
      
      //average
      if(listResetIterator(l))
        {
          Comparison* c;
          do{
              c = listCurrentData(l);
              avg[0] += c->a_minus_b;
              avg[1] += c->subject_response;
            }while(listAdvanceIterator(l));
        }
      
      avg[0] /= N;
      avg[1] /= N;
      
      if(listResetIterator(l))
        {
          Comparison* c;
          do{
              c = listCurrentData(l);
              temp = c->a_minus_b - avg[0];
              variance[0] += temp * temp;
              product = temp;
              temp = c->subject_response - avg[1];
              variance[1] += temp * temp;
              product *= temp;
              numerator += product;
            }while(listAdvanceIterator(l));
        }

      for(d=0; d<DIMENSION; d++)
        {
          r         *= sqrtl(variance[d]);
          variance[d] /= N;
          std_dev [d] = sqrtl(variance[d]);
        }
        
      r = numerator / r;
      m = r * (std_dev[1] / std_dev[0]); //how to  calculate for multivariate data?
      b = avg[1] - m * avg[0];
      t = sqrtl((1-r*r) / (N-2));
      t = r / t;
      
      result->r = r;
      result->m = m;
      result->b = b;
      result->t = t;
    }
  return result;
}

/*-------------------------------------------------*/
void trial_r_for_all_feature_combinations(Trial* self)
{
  unsigned mask;
  unsigned mask_max = 1 << NUM_FEATURES;
  
  for(mask=1; mask<mask_max; mask++)
    {
      trial_calculate_distances(self, mask);
      R* r = trial_calculate_r(self);
      if(r != NULL)
        {
          r->mask = mask;
          listAppendData(self->rs, r, (listDataDeallocator_t)r_destroy);
        }
      else
        { 
          fprintf(stderr, "unable to make R object\r\n");
          break;
        }
    }
}

/*-------------------------------------------------*/
void trial_print_distances(Trial* self)
{
  List* l = self->comparisons;
  
  if(listResetIterator(l))
   {
     Comparison* c;
     long double min;
     do{
         c = listCurrentData(l);
         min = (c->a_dist > c->b_dist) ? c->b_dist : c->a_dist;
         fprintf(stderr, "%Lf %Lf %Lf\r\n", (long double)c->a_minus_b, (long double)c->subject_response, min);
       }while(listAdvanceIterator(l));
   }   
}

/*-------------------------------------------------*/
BOOL trial_sort_by_r_callback(R* r1, R* r2)
{
  return r1->r < r2->r;
}

/*-------------------------------------------------*/
void trial_print_rs(Trial* self)
{
  List* l = self->rs;
  int N = listCount(self->comparisons);
  
  if(listResetIterator(l))
   {
     R* r;
     do{
         r = listCurrentData(l);
         fprintf(stderr, "N: %i\tr: %Lf\tslope: %Lf\ty-intercept: %Lf\tt stat: %Lf w/%i DOF\tmask:%u%u%u%u%u%u\r\n", N, (long double)r->r, (long double)r->m, r->b, r->t, N-2, (((r->mask)>>0)&1), (((r->mask)>>1)&1), (((r->mask)>>2)&1), (((r->mask)>>3)&1), (((r->mask)>>4)&1), (((r->mask)>>5)&1));
       }while(listAdvanceIterator(l));
   } 
}

/*-------------------------------------------------*/
//note that masks are handled internally in bit-reversed order but will be printed in conceptual-order
//i.e. leftmost for centroid (represented internally as 0x01);
int main(int argc, const char* argv[])
{
  if(argc < 2)
   {fprintf(stderr, "Which file would you like to run chapter 5 tests on?\r\n"); return -1;}

  while(--argc > 0)
    {
      ++argv;
      
      Trial* t = trial_new();
      if(t == NULL) {fprintf(stderr, "unable to make trial object\r\n"); return -1;}
      
      trial_read_file(t, *argv);
      if(listCount(t->comparisons) == 0){fprintf(stderr, "unable to read %s\r\n", *argv); return -1;}
      
      trial_r_for_all_feature_combinations(t);
      listSort(t->rs, (listSortCallback_t)trial_sort_by_r_callback);
      trial_print_rs(t);
      
      
      
      //int mask = 15;//47; //all but RMS
      //trial_calculate_distances(t, mask);
      //trial_print_distances(t);
      
      /*
      R*  r = trial_calculate_r(t);
      if(r != NULL)
        {
          r->mask = mask;
          listAppendData(t->rs, r, NULL);
          trial_print_rs(t);
        }
      */
      trial_destroy(t);
    }
  return 1;
}













