~/kiki/Software/Chapter_5: ./ch5 Test2.aif
main menu input options:
	b - tune the robot's stroke classifier
	<space> - play a random stroke
	p - play/pause audio
	0 - reset stroke count
	[] - increment or decrement stroke count
	h - print this help
	q - save and quit

TUNING THE ROBOT'S STROKE CLASSIFIER
input options:
	0~9 - start training that category
	SHIFT + 0~9 - toggle feature mask for that feature
	C - delete all training examples
	c - delete last training example
	k - decrement k
	K - increment k
	p - print normalized features
	t - decrement onset threshold
	T - increment onset threshold
	v - cross-validate training examples
	h - print this help
	q - save or return to main menu
feature mask for CLASSIFIER_ZERO_CROSSING_RATE is 0
feature mask for CLASSIFIER_SPECTRAL_KURTOSIS is 0
feature mask for CLASSIFIER_SPECTRAL_SKEWNESS is 0
feature mask for CLASSIFIER_SPECTRAL_SPREAD is 0
feature mask for CLASSIFIER_SPECTRAL_CENTROID is 0
feature mask for CLASSIFIER_SPECTRAL_CENTROID is 1
DONE TUNING THE ROBOT'S STROKE CLASSIFIER

main menu input options:
	b - tune the robot's stroke classifier
	<space> - play a random stroke
	p - play/pause audio
	0 - reset stroke count
	[] - increment or decrement stroke count
	h - print this help
	q - save and quit
play
stroke count: 0
0.475761 0.365604 -0.272106 -1.142414 -0.438448
stroke count: 1
1.341110 -0.896824 -0.945709 1.030801 2.416563
stroke count: 2
1.287139 -1.088565 -1.285345 1.178705 1.659111
a: 0.865349:	b: 0.811378	b-a: -0.053971, human: +0
stroke count: 0
1.316089 -1.031210 -1.176864 1.729054 1.484314
stroke count: 1
0.941667 -0.886490 -1.258914 0.450291 2.066970
stroke count: 2
1.087782 -0.916084 -1.238446 0.892112 2.066970
a: 0.374423:	b: 0.228307	b-a: -0.146115, human: +0
stroke count: 0
1.661017 -1.253303 -1.796728 1.349484 2.241766
stroke count: 1
1.151445 -0.902453 -1.417851 1.069256 1.717377
stroke count: 2
1.154072 -0.923432 -0.980091 1.221292 1.717377
a: 0.509572:	b: 0.506945	b-a: -0.002627, human: -1
stroke count: 0
0.597133 0.511174 -0.213624 -1.241878 -0.554979
stroke count: 1
0.972626 -0.843710 -1.000739 0.857649 2.416563
stroke count: 2
0.147908 -0.565169 -0.565830 -0.126559 0.493800
a: 0.375493:	b: 0.449225	b-a: +0.073732, human: -2
stroke count: 0
1.832982 -0.794334 -1.078595 1.140528 2.649625
stroke count: 1
0.133808 -0.340360 -0.465567 -0.579771 -0.147120
stroke count: 2
1.072352 -1.153682 -1.301602 1.258511 2.125235
a: 1.699174:	b: 0.760631	b-a: -0.938544, human: +0
stroke count: 0
2.616119 -0.624351 -1.076503 0.898843 2.533094
stroke count: 1
1.184904 -1.120015 -1.444351 1.354241 1.833908
stroke count: 2
1.240523 -0.931956 -0.998602 1.196731 1.484314
a: 1.431215:	b: 1.375596	b-a: -0.055618, human: +1
stroke count: 0
0.973210 -0.148797 -0.456121 -0.174198 -0.205386
stroke count: 1
0.895070 -0.940924 -0.926578 0.872251 1.659111
stroke count: 2
1.444001 -1.167244 -1.316056 1.677117 1.484314
a: 0.078140:	b: 0.470790	b-a: +0.392650, human: +0
stroke count: 0
1.563790 -0.922685 -1.114577 1.185081 1.600846
stroke count: 1
1.171617 -1.148679 -1.290384 1.566413 2.066970
stroke count: 2
1.008815 -0.990988 -0.911548 1.422242 1.018190
a: 0.392172:	b: 0.554975	b-a: +0.162803, human: +1
stroke count: 0
2.110712 -0.980785 -1.429810 1.402512 2.241766
stroke count: 1
0.754979 -0.630583 -0.567926 1.083367 0.260738
stroke count: 2
1.042237 -1.022409 -1.111567 1.380408 0.959925
a: 1.355732:	b: 1.068475	b-a: -0.287258, human: +1
stroke count: 0
0.683617 -0.309452 -0.658515 -0.204015 -0.321917
stroke count: 1
0.746468 -0.978386 -1.184170 1.123204 1.309518
stroke count: 2
1.164058 -1.068128 -1.349277 1.300793 2.533094
a: 0.062851:	b: 0.480440	b-a: +0.417590, human: -2
stroke count: 0
1.878724 -0.776635 -0.985119 1.109782 2.416563
stroke count: 1
1.040060 -0.973961 -1.159767 0.835659 2.183501
stroke count: 2
0.958241 -1.052363 -1.104331 1.186282 1.076456
a: 0.838664:	b: 0.920483	b-a: +0.081819, human: +0
stroke count: 0
2.172300 -1.108347 -1.661562 1.542318 1.950439
stroke count: 1
1.256829 -0.992384 -1.116691 1.406230 1.950439
stroke count: 2
1.049164 -1.070015 -1.292380 1.143544 1.018190
a: 0.915471:	b: 1.123136	b-a: +0.207665, human: -1
stroke count: 0
0.859903 -0.161482 -0.636823 -0.292838 -0.438448
stroke count: 1
0.953599 -1.041197 -1.289182 1.071110 2.125235
stroke count: 2
0.934340 -1.070327 -1.055310 1.395619 1.367783
a: 0.093697:	b: 0.074437	b-a: -0.019260, human: +0
stroke count: 0
1.409893 -1.166782 -1.390506 1.567717 2.707891
stroke count: 1
0.978630 -1.013824 -1.067380 1.059873 2.066970
stroke count: 2
1.059075 -1.024573 -1.117347 1.189090 1.717377
a: 0.431263:	b: 0.350819	b-a: -0.080444, human: +1
stroke count: 0
2.560134 -0.761826 -1.335894 1.008049 3.348812
stroke count: 1
0.909702 -1.064313 -1.106992 1.219106 0.202473
stroke count: 2
1.235411 -0.747945 -0.671344 1.278771 1.134721
a: 1.650431:	b: 1.324723	b-a: -0.325709, human: -1
stroke count: 0
0.782671 0.641932 -0.254940 -1.319413 -1.137635
stroke count: 1
0.347745 -0.584102 -0.555430 0.496470 -0.380183
stroke count: 2
0.802884 -0.516092 -1.108851 0.007328 1.134721
a: 0.434926:	b: 0.020214	b-a: -0.414712, human: +1
stroke count: 0
1.964947 -0.812121 -1.105398 1.213627 2.300032
stroke count: 1
1.065892 -0.981350 -1.422059 0.640000 2.066970
stroke count: 2
0.327617 -0.585192 -0.469751 0.427530 -0.088855
a: 0.899055:	b: 1.637330	b-a: +0.738275, human: -1
stroke count: 0
2.133628 -0.898302 -1.244152 1.461736 3.523608
stroke count: 1
0.828144 -0.828313 -1.078551 0.711671 1.192987
stroke count: 2
0.084258 -0.525323 -0.514397 0.224081 -0.321917
a: 1.305484:	b: 2.049369	b-a: +0.743885, human: -2
stroke count: 0
0.982723 -0.216272 -0.489027 0.294387 -1.137635
stroke count: 1
0.974540 -1.102730 -1.215451 1.191624 2.300032
stroke count: 2
1.057458 -1.000914 -0.970111 1.252697 0.901659
a: 0.008183:	b: 0.074735	b-a: +0.066552, human: +1
stroke count: 0
1.572485 -1.051426 -1.292075 1.230416 2.125235
stroke count: 1
1.114118 -0.883921 -0.864519 1.222921 0.610331
stroke count: 2
1.456710 -1.015668 -1.221924 1.331564 2.183501
a: 0.458367:	b: 0.115775	b-a: -0.342592, human: +1
stroke count: 0
2.572214 -0.832148 -1.453740 1.077007 3.523608
stroke count: 1
1.177911 -0.889378 -0.966968 1.040376 1.484314
stroke count: 2
0.660672 -0.876292 -1.057276 0.679487 1.134721
a: 1.394303:	b: 1.911542	b-a: +0.517239, human: -2
User defined signal 1: 30
~/kiki/Software/Chapter_5: 
