//dissertation chapter 5 stuff

#include "RandomStrokes.h"
#include "../../Kiki_Communication_Framework/Kiki_Communication.h"
#include "../../MachineLearning/src/StrokeClassifier.h"
#include "../../MachineLearning/src/Microphone.h"

void makeStdinCannonicalAgain();
void iHateCanonicalInputProcessingIReallyReallyDo(void);

#define ROBOT_AUDIO_CHANNEL 1
#define NUM_INPUT_AUDIO_CHANNELS 2
/*----------------------------------------------------------*/
typedef struct main_program_struct
{
  Audio*                audio_input;
  Kiki*                 kiki;
  ClassifierExample     strokes[3];
  int                   stroke_count;
  int                   response_count;
  int                   human_response;
  
  //should mutex;
  int                   coroutine_yield_to_main;
}Main;

/*----------------------------------------------------------*/
//this is called by the robots's stroke classifier when the robot strikes its drum (during annealing only)
void robot_onset_detected_callback(void* SELF, ClassifierExample* e, timestamp_microsecs_t onset_timestamp)
{  
  Main* self = (Main*) SELF;
  
  //fprintf(stderr, "stroke count: %i\r\n", self->stroke_count);
  double* f = e->normalized_features;
  //fprintf(stderr, "%lf %lf %lf %lf %lf\r\n", f[0], f[1], f[2], f[3], f[5]);
  
  classifier_example_copy_features(e, &self->strokes[self->stroke_count]);
  ++self->stroke_count; self->stroke_count %= 3;
  
  if(self->stroke_count == 1)
    {
      f = self->strokes[0].normalized_features;
      fprintf(stderr, "human: %lf %lf %lf %lf %lf %lf\r\n", f[0], f[1], f[2], f[3], f[4], f[5]);    
    }
  else if (self->stroke_count == 2)
    {
      f = self->strokes[1].normalized_features;
      fprintf(stderr, "A: %lf %lf %lf %lf %lf %lf\r\n", f[0], f[1], f[2], f[3], f[4], f[5]);    
    }
  if(self->stroke_count == 0)
    {
      //StrokeClassifier* classifier = micGetClassifier((Microphone*)self->audio_input, ROBOT_AUDIO_CHANNEL);
      //double dist_a = classifier_get_euclidian_distance(classifier, &self->strokes[0], &self->strokes[1]);
      //double dist_b = classifier_get_euclidian_distance(classifier, &self->strokes[0], &self->strokes[2]);
      //fprintf(stderr, "a: %lf:\tb: %lf\ta-b: %+lf, human: %+i\r\n", dist_a, dist_b, dist_a - dist_b, self->human_response);
      ++self->response_count;
      f = self->strokes[2].normalized_features;
      fprintf(stderr, "B: %lf %lf %lf %lf %lf %lf\r\n", f[0], f[1], f[2], f[3], f[4], f[5]);
      fprintf(stderr, "response %i: %i\r\n", self->response_count, self->human_response);
    }
}

/*----------------------------------------------------------*/
//this is called by kiki whenever she sends a message to this computer
void  kiki_recieve_callback(void* SELF, char* message, kiki_arg_t args[], int num_args)
{
  Main* self = (Main*)SELF;
  
  kiki_debug_print_message(message, args, num_args);
}


/*----------------------------------------------------------*/
Main* main_destroy(Main* self)
{
  if(self != NULL)
    {
      auSubclassDestroy(self->audio_input);
      kiki_destroy(self->kiki);
      free(self);
    }
  return (Main*)NULL;
}

/*----------------------------------------------------------*/
Main* main_new(int argc, char* argv[])
{
  Main* self = calloc(1, sizeof(*self));
  if(self != NULL)
    {
      StrokeClassifier* classifier;
      
      self->audio_input     = NULL;

      self->kiki = kiki_new(kiki_recieve_callback, self);
      if(self->kiki == NULL) return main_destroy(self); 

      if(argc > 1)
        {
          self->audio_input = (Audio*) micSimulatorNew(argv[1]);
          if(self->audio_input == NULL) fprintf(stderr, "unable to open %s, using mic instead\r\n", argv[1]);
        }   
      if(self->audio_input == NULL)
        {
          self->audio_input = (Audio*) micNew(NUM_INPUT_AUDIO_CHANNELS);
          if(self->audio_input == NULL) return main_destroy(self);
        }
      if(self->audio_input == NULL) return main_destroy(self);
      
      //robot's classifier
      classifier = micGetClassifier((Microphone*)self->audio_input, ROBOT_AUDIO_CHANNEL);
      if(classifier != NULL)
        {
          classifier_set_onset_callback(classifier, robot_onset_detected_callback, self);
          classifier_set_onset_threshold(classifier, 0.8);
          classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_RMS_AMPLITUDE, 0);
        }
      //auPlay(self->audio_input);
    }
  return self;
}

/*----------------------------------------------------------*/
void stroke_classifier_tuner_subroutine(StrokeClassifier* classifier, const char* classifier_name)
{
  const char* classifier_tuner_help =
    "input options:\r\n\t0~9 - start training that category\r\n\tSHIFT + 0~9 - toggle feature mask for that feature\r\n\t"
    "C - delete all training examples\r\n\tc - delete last training example\r\n\tk - decrement k\r\n\t"
    "K - increment k\r\n\tp - print normalized features\r\n\tt - decrement onset threshold\r\n\t"
    "T - increment onset threshold\r\n\tv - cross-validate training examples\r\n\t"
    "h - print this help\r\n\tq - save or return to main menu\r\n";
    
  if(classifier == NULL)
    {
      fprintf(stderr, "\r\nUNABLE TO TUNE THE %s STROKE CLASSIFIER\r\n\n", classifier_name);
      return;
    }
  else
    fprintf(stderr, "\r\nTUNING THE %s STROKE CLASSIFIER\r\n%s", classifier_name, classifier_tuner_help);
  
  for(;;)
    {
      char c = getchar();
      switch(c)
        {
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':
            classifier_enter_training_mode (classifier, 1, c - '0');
            fprintf(stderr, "training category %c\r\n", c);
            break;
          case '!':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_CENTROID, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_CENTROID));
            fprintf(stderr, "feature mask for CLASSIFIER_SPECTRAL_CENTROID is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_CENTROID));
            break;
          case '@':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SPREAD, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SPREAD));
            fprintf(stderr, "feature mask for CLASSIFIER_SPECTRAL_SPREAD is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SPREAD));
            break;
          case '#':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SKEWNESS, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SKEWNESS));
            fprintf(stderr, "feature mask for CLASSIFIER_SPECTRAL_SKEWNESS is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_SKEWNESS));
            break;
          case '$':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_KURTOSIS, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_KURTOSIS));
            fprintf(stderr, "feature mask for CLASSIFIER_SPECTRAL_KURTOSIS is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_SPECTRAL_KURTOSIS));
            break;
          case '%':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_RMS_AMPLITUDE, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_RMS_AMPLITUDE));
            fprintf(stderr, "feature mask for CLASSIFIER_RMS_AMPLITUDE is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_RMS_AMPLITUDE));
            break;
          case '^':
             classifier_set_feature_mask_for_feature(classifier, CLASSIFIER_ZERO_CROSSING_RATE, 
            !classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_ZERO_CROSSING_RATE));
            fprintf(stderr, "feature mask for CLASSIFIER_ZERO_CROSSING_RATE is %i\r\n", 
             classifier_get_feature_mask_for_feature(classifier, CLASSIFIER_ZERO_CROSSING_RATE));
            break;
          case 'C':
            classifier_delete_all_training_examples(classifier);
            fprintf(stderr, "all examples deleted\r\n");
            break;
          case 'c':
            classifier_delete_last_training_example(classifier);
            fprintf(stderr, "last example deleted\r\n");
            break;
          case 'h':
            fprintf(stderr, "Tuning the %s stroke classifier\r\n%s", classifier_name, classifier_tuner_help);
            break;
          case 'K':
            classifier_set_k(classifier, classifier_get_k(classifier)+1);
            fprintf(stderr, "classifier k = %i\r\n", (int)classifier_get_k(classifier));
            break;
          case 'k':
            classifier_set_k(classifier, classifier_get_k(classifier)-1);
            fprintf(stderr, "classifier k = %i\r\n", (int)classifier_get_k(classifier));
            break;
          case 'T':
            classifier_set_onset_threshold(classifier, classifier_get_onset_threshold(classifier)+0.05);
            fprintf(stderr, "onset threshold = %lf\r\n", (double)classifier_get_onset_threshold(classifier));
            break;
          case 't':
            classifier_set_onset_threshold(classifier, classifier_get_onset_threshold(classifier)-0.05);
            fprintf(stderr, "onset threshold  = %lf\r\n", (double)classifier_get_onset_threshold(classifier));
            break;
          case 'p':
            classifier_print_normalized_examples(classifier);
            break;
          case 'v':{
            unsigned n; double accuracy;
            accuracy = classifier_validate_training_examples(classifier, &n);
            fprintf(stderr, "accuracy = %lf%% across %u samples\r\n", accuracy, n);
            break;
            }
          case 'q':
            if(classifier_get_is_training(classifier))
              {
                classifier_enter_training_mode (classifier, 0, -1);
                fprintf(stderr, "training examples saved\r\n");
              }
            else goto out;
            
          default: break;
        }
    }

 out:
  fprintf(stderr, "DONE TUNING THE %s STROKE CLASSIFIER\r\n\n", classifier_name);
  return;
}

/*----------------------------------------------------------*/
void play_random_stroke(Main* self)
{
  int     i = random() % num_random_strokes;
  double* s = random_strokes[i];
  kiki_send_message(self->kiki, kiki_cmd_arm_strike, /*arm*/0, (int)s[0], (int)s[1], (int)s[2], /*spd*/ (float)s[3]);
}


/*----------------------------------------------------------*/
int main(int argc, char* argv[])
{
  iHateCanonicalInputProcessingIReallyReallyDo();
  Main* m = main_new(argc, argv);
  
  if(m == NULL) {fprintf(stderr, "unable to make a main object... aborting\r\n"); return -1;}

  const char* main_help =
    "main menu input options:\r\n\tb - tune the robot's stroke classifier\r\n\t"
    "<space> - play a random stroke\r\n\tp - play/pause audio\r\n\t0 - reset stroke count\r\n\t"
    "[] - increment or decrement stroke count\r\n\t* - mark output invalid\r\n\t"
    "h - print this help\r\n\tq - save and quit\r\n";
  fprintf(stderr, "%s", main_help);
        
  for(;;)
    {
      char c = getchar();
      switch(c)
        {
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
            m->human_response = (c - '0') - 3;
            m->coroutine_yield_to_main = NO;
            break;
          case '*':
          case '8':
            //to mark bad printout
            fprintf(stderr, "*\r\n");
            break;
          case 'b':
            stroke_classifier_tuner_subroutine(micGetClassifier((Microphone*)m->audio_input, ROBOT_AUDIO_CHANNEL), "ROBOT'S");
            fprintf(stderr, "%s", main_help);
            break;
          case ' ':
            play_random_stroke(m);
            break;
          case 'p':
            auTogglePlayPause(m->audio_input);
            auIsPlaying(m->audio_input) ? fprintf(stderr, "play\r\n") : fprintf(stderr, "pause\r\n");
            break;
          case '[':
            m->stroke_count--;
            m->stroke_count %= 3;
            break;
          case ']':
            m->stroke_count++;
            m->stroke_count %= 3;
            break;
          case 'h':
            fprintf(stderr, "%s", main_help);
            break;          
          case 'q':
            goto out;
            auPlay(m->audio_input);
          default: break;
        }
    }

 out:
  main_destroy(m);
  makeStdinCannonicalAgain();
  return 0;
}

/*---------------------------------------------------------------*/
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
struct termios oldTerminalAttributes;

void iHateCanonicalInputProcessingIReallyReallyDo(void)
{
	int error;
	struct termios newTerminalAttributes;
  
	int fd = fcntl(STDIN_FILENO,  F_DUPFD, 0);
  
	error = tcgetattr(fd, &(oldTerminalAttributes));
	if(error == -1) {  fprintf(stderr, "Error getting serial terminal attributes\r\n"); return;}
	
	newTerminalAttributes = oldTerminalAttributes; 
	
	cfmakeraw(&newTerminalAttributes);
	
	error = tcsetattr(fd, TCSANOW, &newTerminalAttributes);
	if(error == -1) {  fprintf(stderr,  "Error setting serial attributes\r\n"); return; }
}

/*------------------------------------------------------------------------------------*/
void makeStdinCannonicalAgain()
{
  int fd = fcntl(STDIN_FILENO,  F_DUPFD, 0);
  
	if (tcsetattr(fd, TCSANOW, &oldTerminalAttributes) == -1)
	  fprintf(stderr,  "Error setting serial attributes\r\n");
}